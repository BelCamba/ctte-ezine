---
title: Septiembre 2019
---

# Cambá CTTE Ezine Septiembre 2019

## Letra de canción

```
Nos rendimos a nuestras pasiones pero nunca quedamos satisfechos
Y toda la presión de otros, nos hizo vivir solo una mentira
Le dimos el poder a personas que no deseaban lo mejor para nosotros
¿Cuánto vamos a ceder hasta que seamos lo que detestamos?
Así que tengo que decir: el tiempo pasa
Digo, tomemos el éxito en nuestras manos
Comencemos a tomar el control
Todo nuestro placer a corto plazo es nuestra fuerza impulsora en la vida
lo que nos lleva a más sufrimiento, tragedia y vicio
Todos somos víctimas, solo algunos pocos están seguros
Entonces debemos tomar el control antes de convertirnos en esclavos
Empecemos a tomar el liderazgo de nuestras vidas
Estamos viviendo esta vida como la vemos en televisión, solo miramos
No puedo dejar que este mundo reaccione sobre mí, no sobre mí
Aprovecha la oportunidad a cualquier costo
```

Traducción del tema  [Time's ticking away](https://www.youtube.com/watch?v=MbAGQoAQyOI), de la banda [Shelter](https://en.wikipedia.org/wiki/Shelter_(band)), del disco ["Beyond Planet Earth"](https://www.youtube.com/watch?v=b1d_mbczmOc)


## Herramientas de software

- [Hasura GraphQl Engine](https://github.com/hasura/graphql-engine) - Graphql api over postgres
- [DeepFaceLab_Linux](https://github.com/lbfs/DeepFaceLab_Linux) - Para creación de deepfakes
- [Funkwhale](https://funkwhale.audio) - Proyecto comunitario que permite escuchar y compartir música y audios dentro de una red descentralizada y abierta (con software libre).

## Informes/Encuestas

- Lanzmiento de [Nuxt.press](https://nuxt.press/)
- Nueva versión de [GIT 2.23.0](https://github.blog/2019-08-16-highlights-from-git-2-23/)
- Se hizo la [Hackmeeting 2019](https://es.hackmeeting.org/hm/index.php?title=Hackmeeting2019) - desde 20 al 22 de septiembre en Errekaleor - Vitoria-Gasteiz - País Vasco. [Algunas charlas](https://es.hackmeeting.org/hm/index.php?title=Nodos2019)
- Se hizo el campamento hacker del [CCC](https://www.ccc.de/) en Zehdenick, Germany (El más grande del mundo) - [Más data](https://events.ccc.de/camp/2019/wiki/Main_Page), [Proyectos](https://events.ccc.de/camp/2019/wiki/Static:Projects) -> (Pensando Agitar versión local en lo de @neto)
- [Mariano Lambir](https://github.com/mlambir), asociado a [Fiqus](https://fiqus.com/), presento [Nano-wirebot](https://js13kgames.com/entries/nano-wirebot) en la compentencia de juegos en 13kb

### Encuesta de Sueldos 2019.02 - Julio/Agosto

> [Encuesta](https://openqube.io/encuesta-sueldos-2019.02) que lleva a cabo [openqube.io](https://openqube.io), que es una plataforma colaborativa en la que empleados y ex empleados pueden calificar y escribir reseñas anónimas para brindar información de calidad sobre las empresas

- [Contribuye al open source](https://openqube.io/encuesta-sueldos-2019.02#Perfil-de-participantes-Roles-Cuanta-gente-contribuye-al-Open-Source): 17%
- [Programa por hobbie](https://openqube.io/encuesta-sueldos-2019.02#Perfil-de-participantes-Roles-Cuanta-gente-programa-por-hobbie): 54%
- [Salarios](https://openqube.io/encuesta-sueldos-2019.02#Salarios) - Mediana salarial en Argentina $61914, U$S 1112 (Al 15/08/2019)
  - **Developer** Junior: 40k - Semisenior: 60k - Senior: 72k
  - **Project manager** Junior: 46k - Semisenior: 48k - Senior: 78k
  - **Sysadmin** Junior: 38k - Semisenior: 60k - Senior: 70k
  - **Lider Técnico** Junior: 70k - Semisenior: 85k - Senior: 88k
  - **Arquitecto** Junior: 105k - Semisenior: 110k - Senior: 109.5k
- La brecha salarial general es grande: 14.55% y según los datos recabados se acentúa en las personas con +8 años de experiencia (45% de la población censada (8, 55] = 22,87% + 17,23% + 4,66% + 0,24%),
- La población que participa en la encuesta sigue siendo 85%+ hombres.
- [Tecnologías más populares](https://openqube.io/encuesta-sueldos-2019.02#Tecnologia-Tecnologias-mas-populares): Linux, Javascript, Bootstrap, Mysql, Postman, Visual Studio Code

### Encuesta de HackerRank 2018 sobre estudiantes

> [Encuesta](https://research.hackerrank.com/student-developer/2018) que lleva a cabo [hackerrank](https://www.hackerrank.com/) sobre más de 10k de estudiantes que programan.

Algunas conclusiones:

- Un titulo universitario no es suficiente para aprender a programar
- Los estudiantes confian mas en YouTube que les profesionales
- Globalmente la demanda por Javascript supera la experiencias de les estudiantes
- Ruby, Python y JavaScript están primeros en la lista to-do de les estudiantes
- Los frameworks son generalmente aprendidos en el trabajo
- Las oportunidades de crecimiento atraen 5 veces más que beneficios (ej: frutas, sillas, etc)
- En Estados unidos, en balance entre la vida laboral y personal supera a las oportunidades de crecimiento
- Se destacan las habilidades de les estudiantes, no se usan los Curriculums

## Noticias, Enlaces y Textos

### Más sociales/filosóficos

- [Hola Mundo en C](CTTE-texto-holamundo) - Extracto de "La Clase Peligrosa", escrito por Juan Grabois
- Video [Trouble #8 - hack the system](https://open.tube/videos/watch/47e373c0-9d8a-433a-9e9d-d2ba62bfaba4) con subtitulos en español, o [versión original](https://sub.media/video/trouble-8-hack-the-system/) - Internet como campo de batalla
- Charla [Presentación del libro "La Potencia Feminista"](https://www.youtube.com/watch?v=4D_26KYUn1M) escrito por [Verónica Gago](http://tintalimon.com.ar/libro/La-potencia-feminista)
- Charla [Presentación del libro "Esferas de la insurrección"](https://www.youtube.com/watch?v=kW_n-kF-QCg) escrito por [Suely Rolnik](http://tintalimon.com.ar/libro/Esferas-de-la-insurreccin)

### Más técnicos

- [Correr WebAssembly desde otros lenaguajes](https://hacks.mozilla.org/2019/08/webassembly-interface-types/)
- [Tutorial para hacer un Tateti con GraphQL (Hasura + Apollo client + React)](https://css-tricks.com/multiplayer-tic-tac-toe-with-graphql/)
- El 25 de agosto de 2019, se conmemora el 28 aniversario de la publicación lo que es hoy el kernel de linux, lean algunos hechos intersantes. [Texto](https://www.omgubuntu.co.uk/2018/08/interesting-facts-about-linux)
- Acá charla sobre si habría que poder usar Rust para escribir drivers para el kernel de linux. [Charla en LWN.net](https://lwn.net/Articles/797714/)
- [Retroahoy](https://www.youtube.com/channel/UCE1jXbVAGJQEORz9nZqb5bQ) - [Video Capitulo que trata sobre los juegos point-and-click](https://www.youtube.com/watch?v=9F9ahZQ7oP0)

### Seguridad

- [ODIA](https://twitter.com/ODIAasoc) hizo un pedido de información pública sobre las camarás con reconocimiento facial. [Acá los resultados](./assets/camaras.pdf)

### Otras

- Escuchen la [Entrevista](./assets/maria_elena_casanas.ogg) de 2011 a [Maria Elena Casañas](https://elblogdemec.blogspot.com/) charlando en Oveja Electrónica sobre su experiencia y trayectoria con el software libre y su la filosofia. [Audio](./assets/maria_elena_casanas.ogg)
- Luego de capacitación de @sol: USB drop cerca de la casa de varios cambaces, vayan a buscar data - Av Caseros 2630, Don Bosco, [Más data](https://deaddrops.com/db/?page=view&id=2016)

## Propuestas de contribuciones

- [Agregar reload/refresh](https://gitlab.com/arandr/arandr/issues/24) de configuración para [Arandr](https://gitlab.com/arandr/arandr) (frontend para [Xrandr](http://www.thinkwiki.org/wiki/Xorg_RandR_1.2))
