# Encuesta Capacitación

> Esta encuesta está destinada a recabar las opiniones, deseos y certezas de cada socix de Cambá con respecto a las capactiaciones internas que nos brindamos.
> **Totales:** 20 respuestas (16 completas y 4 incompletas)


## De las capacitaciones que tuvimos hasta ahora alguna te intereso más, en particular ¿Cúal? ¿Por qué?

Muchos dijieron que les interesaban más las que tienen un componente fuerte práctico (*la aplicación*, *trabajo práctico*, *ejemplos*), no simplemente una exposición, pero si hay exposición deberia estar centrado en fortalecer el marco teorico conceptual de la temática.
Otros nombran la dualidad entre presenciar una capactiación y luego enfrentarse solo al tema (*en otro momento no entiendo lo que creo haber entendido*)
Se remarco mucho la idea de incluir gente de afuera argumentando con frases como *por el intercambio, la vinculación y la frescura que genera*.
Algunos catalogan de innecesarias o que necesitan reformulación las capacitaciones/bajadas que surgen a apartir de los eventos que otros presencian
Otros Ponderan la idea de que a traves de las capacitaciones se puede visibilizar/conocer otras partes de Cambá, por ejemplo como se hizo con LTC.
Por otro lado al menos 3 dicen que no participaron de muchas.
Otras temáticas que se mencionaron como temas interesantes: Encriptación,  Género


| Capacitación   | Cantidad |
|----------------| --------:|
|LTC             |        6 |
|Torneo          |        5 |
|Css             |        5 |
|godot           |        5 |
|cypress         |        4 |
|Faircoop        |        3 |
|AWS             |        2 |
|TDD             |        2 |
|Historia..      |        2 |
|Processing      |        2 |
|UI/UX           |        2 |
|GIT             |        1 |
|Aspectos        |        1 |
|Docker          |        1 |
|Blockchain      |        1 |
|Hackaton        |        1 |

## ¿Cual es el stack tecnológico que usas en el proyecto en el que estas?

### 1
- *Frontend:* **React Native con Expo**
- *Backend:* **Laravel (PHP)**
- *Gestión:* **Gitlab**

### 2
- *Frontend:* **Vue.js**
- *Backend:* **Django (Python)**
- *Db:* **Mongo**

### 3
- *Frontend:* **Angular 1.x**
- *Backend:* **Laravel y Zend 1 (PHP)**
- *Db:* **MariaDB**
- *Testing:* **karma, jasmine, protractor, phpunit, mockery**
- *Deploy:* **docker**

### 4
- *Moodle:* **HTML, CSS, JS, PHP**
- *Testing:* **Gherkin**
- *Arduino:* **C++**
- *Db:* **Postgresql**
- *Deploy:* **Dokku, Gitlab runners**

### 5
- *Backend:* **Node (express)**
- *Frontend:* **Vue, jQuery, Ejs**
- *Db:* **mongodb, memcached, redis**
- *Testing:* **mocha, sinon, chai, karma**
- *Ci:* **jenkins**

### 6
- *Backend:* **Feathers (Node)**
- *Frontend:* **React con react-admin**
- *Db:* **Mongodb**
- *Otros:* **Bash scripts**

### 7
- *Frontend:* **React**
- *Backend:* **PHP**
- *Capa de datos:* **GraphQL**
- *Db:* **MySQL**
- *Testing:* **Enzyme**
- *Deploy/Entorno:* **Docker**

### 8
- **Gitlab**
- **Tryton**
- **Thunderbird**
- **LibreOffice**
- **Google Drive**
- **Firefox**


Encontre contestaciones graciosas como "shoy comerchial", "ns/nc jejej", es verdad que la pregunta estaba más pensada para los proyectos productivos pero entiendo que hay muchas herramientas del día a día que son necesarias para el trabajo que se hace (tryton, suiteCRM, etc).
Por otro lado hubo un caso donde se dijo *Angular no es mi fuerte*


## Sobre qué temáticas específicas del desarrollo de software sentis que te falta conocimiento, que te generan inseguridad, etc.

### Maquetación

- Conocimientos más sólidos de css
- Maquetación general
- Maquetado de layouts

### Metdología

- Estimación de un proyecto
- Gestión de proyecto (comunicación)

### Frontend programación

- Programación funcional (ej Redux/Vuex)
- Sincronismo y asincronismo con otros metodos que no sean promesas

### Infraestructura

- Dockerización
- Automatización y customización de ambientes (dev, staging, prod)

### Frameworks

- Vue.js
- React.js

### Herramientas

- Manejo de GIT
- Debugging mas inteligente que consol.log

### Uso de computadora

- Instalaciones de algunos programas
- Entender e instalar hardware en mis computadoras

### Generales

- Generación de API
- Performance
- Estructura de datos
- Conceptos y desarrollo de backend
- Flujos o técnicas de autenticación y Métodos de autorización en backend
- Mejor forma de resolver un problema (¿lógica?)
- Más conocimiento teórico

### Educación

Profundizar mi capacitación en pedagogías y didácticas vinculadas a tecnologías.

### Otros

- Otros lenguajes que no sean javascript
- No programo viejo, que parte no se entiende?? XD

## Detalla ejemplos concretos de problemas/issues que pensas que te demandaron más tiempo de lo crees necesario para resolverlos.

### Maquetación

- Maquetar algunas vistas
- Agregar estilos a los html
- Responsividad multiplataforma
- Diseño para IE 10-11
- Diseño/compatibilidad con iOS en webapps.

## Frontend Programación

- TextInput con estilos customizados para React-Admin
- Tabla dinámica para los formularios de incubación
- Uso de broadcast/listeners (Angular 1.x)
- Uso de localstorage (Angular 1.x)
- Agregar test de unidad a componentes (Angular 1.x)
- Uso ng-transclude
- Comunicación entre los componentes
- Desarrollo de componentes react

### Herramientas

- Ambiente listo

### Otras técnicas

- CORS (Siempre tuve problemas de índole muy parecida y siempre tardé bastante en resolverlos)
- Conectarse a la balanza vía Bluetooth
- Libreria de serial
- Manejo de bash y scripts de sistema
- Cosas de sincronizacion

### Otros comunicacionales/aprendizajes/decisión

- Lo que me pasa es que muchas veces estoy trabado con algo y cuando me doy cuenta era una pavada. (que ya sabía) pero que me costó identificar a tiempo.
- Los que necesitaban feedback de algun compañerx
- ¿Cómo enseñar a trabajar sobre la IDE de Arduino para extraer cuáles son todas la herramientas o instancias que intervienen?
- Decidir la tecnología para trabajar con facebook
- Las tareas de investigación de nuevas tecnologías, me encantan, pero siento tardo demasiado

### Otros

- *N - O - P - R - O -G -R - A - M - O*

## Cuales son las metodológias, técnicas, temáticas básicas sobre tecnología que todos deberiamos apuntar a conocer? (nivel base)

### Técnicas generales

- Conocimiento de OOP
- Conocimiento de programación funcional
- Conocimientos de persistencia en DBs (SQL/NoSQL)
- Concurrencia
- Metodolgía Agile (SCRUM/KANBAN/CUSTOM)
- QA
- Testing, TDD/BDD
- CI/CD
- Debug de código
- Linter
- Servicios REST
- Instalación y configuración de IDE
- IOT

### Habilidades sociales/personales

- Buena comunicación entre nosotros
- Buenas comunicación con el cliente
- Inglés
- Escribir issues descriptivos
- Seguir los estándares de proyectos
- Manejo del tiempo (estrategias para priorizar)
- Estrategias para resolución de issues
- Analisis de requerimientos
- Autogestión del trabajo
- Laburo en equipo
- Manejo de  expectativas del cliente

### Técnicas especificas

- Frameworks de frontend
- CSS/HTML
- Git
- Node, python, php, react, vue, docker, heroku, gitlab, sql, mongodb
- Docker, Docker-compose
- Utilizar crontab


## ¿Cuales son los conocimientos básicos que todos deberiamos saber?

Esta pregunta se presto a confusión dada que se repetía con una anterior pero tenía otro contexto y ese contexto estaba dado por un titulo de mayor orden que había que leer claramente.
Al menos 6 personas volvieron a responder con cosas técnicas cuando lo esperado estaba relacionado a lo social.

### Cooperativismo

- Organismos sobre cooperativismo (FACTTIC, COTECH, INAES, ACI, etc)
- Autogestión Cooperativismo  (Bases, teoría, medios de producción, teoría del valor, etc)
- Reparto de excedentes y capitalizacion
- Coyuntura política y cómo la cooperativa de trabajo se inserta en ella

### Aprendizaje colectivo/personal

- Poder ser autogestivos
- ¿Cómo desarrollar el potencial propio dentro de la coyuntura de la cooperativa? ¿Cómo le sirvo más a la cooperativa? ¿Realmente me gusta hacer lo que hago?
- Inglés
- Convivencia
- Relaciones interpersonales
- Otro idioma
- Oratoria
- Compresión de texto y escritura
- Autogestión del monotributo
- Historia Argentina tanto de la educación como de la política

### Cambá

- Estructura de la cooperativa
- Saber con quiénes nos relacionamos y por qué
- Diferencias entre proyectos llave en mano y servicios profesionales
- Vacaciones
- Herramientas de gestion que usamos en la coope
- Nuestra Mision, vision y valores
- La historia de Cambá

### Conocimientos

- Conocimientos contables mínimos
- Conocimientos legales mínimos
- Conocimientos económicos mínimos
- Conocimientos de Software, hardware y cultura libre


## Sobre qué temáticas específicas no técnicas sentis en que te falta conocimiento para tomar decisiones.

### Generales

- Legal
- Financiero
- Software Libre
- Primeros auxilios
- Cuestiones organizativas
- Cooperativismo y economía social
- Institucional

### Otros

- Tipo de proyecto es conveniente hacer y cual no
- Impacto de elegir hacer tal o cual proyecto
- Vinculación (saber quienes son,de donde vienen, etc)
- Conocimiento de experiencias cooperativas/autogestionadas, formas de organización las mas comunes y no tan habituales
- Marco legal de nuestra actividad
- Transmisión del conocimiento
- Sobre ley de contrato de trabajo
- ¿Cómo invertir de manera sustentable?
- Tener mas seguridad de lo pienso y siento

## En particular que conocimiento deberiamos fortalecer colectivamente

Más de 3 personas contestaron que no tenian idea

### Interpersonal

- Escucharnos más
- Ser un poco más tolerantes
- Ser más participativos
- Estár mas involucrados en lo que pasa acá dentro
- Poder interpretar lo que quiere expresar el otro y asi poder entender un poco mas de como piensa.
- Autogestión
- Conciencia sobre lo que hacemos
- Fortalecimiento de los lazos
- Fortalecer la participación

### Planificación

- Planificacion estrategica
- Planificacion a corto, mediano, plazo.

### Gestión

- Donde seguir ejerciendo presion y poniendo esfuerzo
- Donde visibilizar que podemos relajarnos un poco y disfrutar de lo hecho, de la forma que lo hacemos, de los objetivos cumplidos
- Mejora continua
- Gestión de proyectos, responsabilidad y transparencia
- Involucrarnos más en las relaciones intercooperativas

### Otros

- Charlas de cooperativismo , más reuniones con otras coops
- Integrar material social, político,
- Acentuar los debates sobre géneros

## ¿Qué otra cosas pensas que deberiamos aprender? ¿Por qué?

- Matemática avanzada
- Redes Neuronales
- Blockchain
- Fomentar las actividades "extra" (inglés, yoga música, flamenco, folklore, malabares, etc)
- Participación mas activa en las asambleas, para sentirnos todos igual de responsables (quizás haya ejercicios para fortalecer estas cosas)
- ¿Qué es prioritario y que no?
- Creo que aun nos falta elegir algun/os objetivos en cuanto a producto o servicio que de fondo soluciones o contribuya a mejorar la sociedad, o la calidad de vida de sus personas
- Medio Ambiente
- A cambiar el papel cuando se acaba. Por que sino va a correr sangre
- Planificación estratégica, Diseño de planes de negocio
- Siempre pense que estaría bueno poder enfocarnos a poder hacer aplicaciones mobile hibridos

## ¿Qué te parece el formato de las capacitaciones? ¿Qué enfoque te parece más atractivo o útil?

- La metodología: Dos sesiones, teórica (escuchar, entender), práctica (poner a prueba lo aprendido, plasmar las ideas).
- Las que son del formato de solo ver una presentación no es muy interesante ya que para eso lo puedo ver en mi tiempo libre.
- Tener un espacio más técnico
- Más tiempo a las capacitaciones
- Se me complica quedarme después del horario
- Prefiero la "vista aerea", una capacitación mas informativa
- Extendiéndonos de la hora pactada
- Lo más práctico posible
- Más de un día en las capacitaciones que lo necesiten
- Dinámicas, preparadas con tiempo
- Aumentaria a 90 minutos
- Temas no tecnicos y/o de exposicion sobre trabajos hechos por los socixs dentro o fuera de Cambá
- Tratar un tema específico
- Estilo workshop
- Algo con una práctica especifica, ya sea social o técnico
- Para las capacitaciones con tematicas sociales encararía una lógica de introducción del tema y debate entre todes con textos pasados previamente para que todes participemos activamente.
- TEMA: ¿Cómo se elige? , PREPARACIÓN: ¿Quién la arma? , OBJETIVO/DIRIGIDO: ¿A quien esta dirigido? que estuviera dirigido a todos los socios (en lo posible), EJECUCIÓN: ¿Como se lleva a cabo? (solo expositiva, solo práctica, un mix)

## ¿Creés que existen temas para capacitaciones a los que le deberíamos dedicar más tiempo? es decir más de una capacitación, si es así, ¿Cuáles y Por qué?

- Puede ser el debate sobre temáticas que de alguna manera vienen conectadas (ejemplo, vemos capitalismo y después como luchamos contra el(?)).
- Herramientas que podemos aplicar en el día a día en los proyectos en los que trabajamos
- Hay temas que se dan como sabidos y tal vez estaría bueno primero hacer una aproximación general antes de ir a lo específico
- Capacitaciones estratégicas
- Tecnologías estandar que utilizamos

## **Conclusiones**

Luego de leer las ideas sobre capacitaciones pienso sobre que tenemos que esforzarnos más por reflexionar sobre lo que somos, una cooperativa de trabajo en el área de tecnología. Es importante ya que al tener roles y perfiles diversos tenemos que fortalecer la interrelación de los conocimientos. Esto implica un involucramiento cada vez más grande y ampliando conocimiento de las diversas áreas. Necesitamos más comprensión técnica para los que no saben mucho de ese área y más comprensión de otras áreas para lo más técnicos. Además obviamente necesitamos fortalecer nuestra rol aquí dentro, es un esfuerzo con dos línea diferentes y complementarias.
En general hubo un mayor voluntad de que las capacitaciones sean más prácticas.
Tenemos que respetar más los horarios que nos fijamos para aprovecharlos lo más posible.
La palabra de mayor ocurrencia en las respuestas fue la palabra **autogestión**

### Interrogantes surgidos de la encuesta

- ¿Cómo incluimos a los que no participan?
- Cantidad de tiempo para la capacitación: ¿Es posible ampliarla a 90 minutos?
- ¿Como hacemos para transmitir cierto conocimiento de los eventos en los que se participa?


## **Propuestas**

Luego de charlar con varios cambaces en varias reuniones llegamos plantear un ideal mensual de capacitaciones a seguir. El formato sería una mezcla entre diferentes tipo de capacitaciones que se detalla a continuación. Por otro lado tenemos los interrogantes planteados con respecto:
- ¿Cuáles son las temáticas especificas y con que objetivos?
- ¿Quién prepara las capacitaciones y quien la brinda?

Por más que podamos tener una serie de capacitaciones hechas y pĺanteadas, creo debemos estar permeables a que los asociades de cambá puedan decidir y ponerse en el lugar de brindar capacitaciones a otres. La dinámica que logramos hasta el momento en los últimos meses me parece tenemos que mantenerla y puede complementarse.

Sería muy bueno si en las 1eras capacitaciones de algunos tipo pudieramos juntarnos, revisar lo dicho en la escuesta y obtener una lista definitiva sobre cuales son las capacitaciones estratégecias necesarias basadas en los resultados anteriormente explicitados. Entiendo que una vez hecho lo anterior podemos comprometer tiempo de la CTTE para realizar ciertas capacitaciones.

### Tipos de capacitaciones

 1. Capacitación especifica provenientes desde equipos técnicos internos de Cambá - Técnicas de las problemáticas y soluciones en sus proyectos (25%)
 2. Exploración de nuevas tecnologíais/proyectos/temáticas o consolidación de conocimientos del área (25%)
 3. Otras áreas del conocimiento (25%)
 4. Separación en grupos de estudio más pequeños (ej: separados por rol o temática) (%25)

#### Equipos Técnicos internos

La idea en esté tipo de capacitación es que presenten problemas, issues y soluciones de como fueron arreglados o implementados en los proyectos que actualmente estamos trabajando.
Como primera medida cada proyecto deberia presentar su stack, organización para luego en otra capacitación ya ir a problemáticas especificas.
Esto incluye proyectos de Llave en mano, Servicios Profesionales y Educación.
Según ese 25% deberian una vez por mes hacerse carga de está capacitación uno de este equipos o proyectos (ej: En la actualidad hay 5 proyectos de desarrollo de software + la parte educativa: 6 en total)

#### Exploración o Consolidación

Acá entrarian las capacitaciones generales para consolidar conocimiento, las capacitaciones que necesitemos generar sobre cuestiones especificas, y además las de exploración de nuevas tecnologías.
Algunas temáticas para hacer capacitaciones:

#### Otras áreas

Este espacio debería ser destinado a otro tipo de conocimiento por fuera estrictamente de la programación. Debería estar fogoneado por los que tienen multiples intereses y desean transmitir a otros algo que les interesa particularmente. (ej: Mi imagino, algo musical, baile, deporte, análisis y discusión de textos, etc)

#### Grupos de estudio

Esta dinámica es para evitar el aburrimiento, generar más participación, y encontrar más los relevancia en el material.
Se forman grupos pequeños de 5 personas como mucho y trabajan sobre alguna temática particular con un nivel determinado (ej: escuche sobre vue.js pero nunca lo use)

