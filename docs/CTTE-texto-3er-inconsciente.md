## Libro ["El tercer inconsciente"](https://cajanegraeditora.com.ar/libros/el-tercer-inconsciente/)
> Franco "Bifo" Berardi
> La psicoesfera en la época viral

### PREFACIO

Este libro explora la mutación en curso del inconsciente social. Mi punto de observación es el que habitamos actualmente: el umbral histórico marcado por la pandemia viral y el colapso catastrófico del capitalismo. Desde este umbral podemos ver delante de nosotros, de manera clara e oirrefutable, un horizonte de caos, agotamiento y tendencia a la extinción.
Esta mutación ha sido resumida a la perfección por el filósofo japonés Sabu Kosho. En su libro Radiation and Revolution [Radiación y revolución], Kosho escribe con claridad desesperanzadora: “Filosóficamente, este es un desplazamiento ontológico de la dialéctica a la inmanencia, de la totalización a manos del capitalismo y el Estado a la omnipresencia de eventos singulares. En este desplazamiento, radica la perspectiva de una revolución planetaria que ha de ser asida en la descomposición del Mundo y el redescubrimiento de la tierra.

Los conceptos que despuntan en la lectura que hace Sabu Kosho de los apocalipsis de Fukushima de 2011 resultan clave para interpretar los apocalipsis globales de 2020: la proliferación ubicua e imparable del princicpio de disolución (radiación, virus), la erosión de todos los órdenes simbólicos y políticos, y el retorno de la tierra, largamente negada. Definida por Deleuze y Guatarri como "la absolutamente-desterritorializada", la tierra está reafirmándose y barriendo el patético poder de la politica con la fuerza de tsunamis, incendios forestales y epidemias virales.

Pienso que la filosofía y el psicoanálisis, lejos de entrar en pánico, lejos de despotricar contra el caos, deben asumir el  horizonte de caos y agotamiento como punto de partida de su reflexión. Es necesario redefinir todo, en particular lo que tiene lugar no en el mundo exterior, sino en el espacio íntimo del deseo, la emoción y el miedo.

El inconsciente es un ámbito sin historia, sin secuencialidad, sin antes y después: sería imposible escribir una "historia del inconsciente". Pero es posible escribir una historia de la psicosfera de una sociedad, y, en este sentido, es posible hablar de un "tercer" inconsciente: la tercera forma que adopta el inconsciente en el medioambiente mental de la modernidad tardía.

La "primera" fase fue explotada por Freud, quien concibío el inconsciente como el lado oscuro del armazón bien ordenado del progreso racional.

Ciencia, educación y laboriosidad era los pilares de la moderna vida pública. Matrimonio, monogamia y familia nuclear eran lo pilares de la moderna vida privada.

En el Malestar en la cultura (1930), Freud afirmó que la normalidad social exigía un alto grado de negación del deseo o represión del Trieb [pulsión] (el impulso sexual y la instintualidad). La forma burguesa de la "normalidad" dominante a comienzos del siglo XX producía una forma particular de padecimiento, que freud denominó "neurosis".

Para llevar a cabo las actividades de su vida diaria, el individuo moderno estaba obligado a renunciar a, reprimir y en lo posible olvidar sus propios impulsos sexuales. Y esta remoción era patogénica. La neurosis era la forma general de esta patología.

El marco cambío en las últimas décadas del siglo XX, cuando la aceleración de la infosfera y la intensificación de la estimulación nerviosa (comunicación a través de Internet y globalización cultural) comprometieron la represión sistémica del deseo y el régimen psicopatológico de la neurosis.

La primera intuición de esta transformación del paisaje psicocultural puede encontrarse en el Anti-Edipo de Deleuze y Guatarri, libro que marcó el pasaje del estructuralismo al pensamiento creativo-rizomático, pero que también abrío conceptualmente una caja de Pandora del deseo, anticipando de ese modo la hipermovilización neoliberal de la energía del deseo en tanto separada del placer.

En el Anti-Edipo, Deleuze y Guatarri rechazan la idea de que el inconsciente es una especie de depósito de las experiencias que no queremos ver, recordar o llevar a nuestra vida consciente. El inconsciente no es un teatro, sino un laboratorio: el inconsciente es la fuerza magmática que produce incesantemente nuevas posibilidades de imaginación y de experiencia.

Hoy, cincuenta años después de la publicación de El Anti-Edipo, podemos leer el pensamiento creativo de Deleuze y Guatarri como el molde ambiguo (extremadamente ambiguo y extremadamente rico) para un futuro de doble filo: el futuro utópico de la "liberación del deseo" y el futuro distópico del capitalismo neoliberal, donde el deseo es celebrado como el impulso al consumo, la competencia y el crecimiento económico, mientras que el placer es constantemente postergado.

El entero sistema de los medios de comunicación ha sido movilizado para expandir las promesas de disfrute, pero esta aceleración del flujo de información ha sobrecargado la capacidad de atención humana, posponiendo para siempre la posibiliad del placer, que término por volverse inalcanzable. Este régimen social llevó a la configuración de un nuevo régimen psicopatológico, el cual ha caracterizado a las últimas décadas: la era del pánico, la depresión y, en última instancia, la psicosis.

Pánico quiere decir percepción de un exceso de posibilidad, intuición de una cantidad de placer inaccesible. Una persona entra en pánico porque se ve expuesta un exceso de placer que no puede experimentar realmente. El pánico es una vía de escape de la depresión, y la depresión es el retorno tranquilizador de un viaje de pánico. Esta es la oscilación interior de la psicoesfera posneurótica.

En la era del segundo inconsciente, la neurosis ya no es el modo general de sufrimiento psíquico. Conforme la explosión del inconsciente lleva a una condición de hiperestimulación nerviosa y frustración psicológica, la psicosis toma el lugar de la neurosis.

El torbellino rizomático de la experiencia en red arrastra al inconciente, que Freud definió como innere Ausland (un país extranjero íntimo), fuera de sí mismo, externalizándolo hasta el límite de una explosión psicótica.

Llamo "simiocapitalismo" a este acoplamiento de acumulación, producción semiótica y estimulación nerviosa.

Guatarri sugiere que la esquizofrenia debe ser considerada como una condición de producción libre de sentdio. En su pensamiento, el esquizofrénico se convierte en la figura central de una aventura de liberación, creatividad y conocimiento. Paro ese es solo el lado liberador de la aceleración. También tiene otra faz, como fue denunciado por Jean Baudrillard en El intercambio simbólico y la muerte (1976): la impresionante aceleración de la estimulación nerviosa (seducción, simulación, hiperrealidad) va de la mano con la globalización neoliberal y provoca una perturbación en la esfera de la experiencia.

La psicopatología del semiocapitalismo está marcada por la ansiedad, los trastornos de la tención y el pánico. La depresión aparece como síntoma final del régimen semiocapitalismo: la intensidad del ritmo social y emocional se vuelve insoportable, y la única manera de escapar al sufrimiento es cercenar el vínculo con el deseo, y, en consecuencia, el vínculo deseante con la realidad.

Hoy, en la tercera década del nuevo siglo, la fase del segundo inconsciente parece estar llegando a su fin. Todo indica que estamos ingresando en una nueva psicoesfera donde comienza a tomar forma un tercer inconsciente. Estén prevenidos: la forma de este nuevo ámbito del inconsciente no es fácilmente definible; ni tampoco es predecible, porque la psicoesfera no evoluciona de manera lineal. No hay determinismo en la psicoesfera; no hay mapa del innera Ausland, porque, como afirma Freud, el inconsciente no tiene consistencia ni lógica. Por eso no podemos saber con exactitud en qué dirección(es) se extenderá el paisaje mental, qué tipo de evolución(es) provocará la pandemia del covid-19 al converger con un colapso económico y social generalizado.

Cuando hablo de la tercera era del inconsciente, me refiero a un futuro abierto que será moldeado por nuestra conciencia, por nuestra acción política, por nuestra imaginación poética y por la actividad terapéutica que seamos capaces de desarrollar durante esta transición. Hecha las advertencias necesarias, es posible delinear al menos algunas de las condiciones del actual desplazamiento.

El umbral esta aquí, es ahora: surgió con la llegada del coronavirus al espacio de nuestra conciencia coletiva. Este bio-info-psicovirus está cambiando de manera irreversible nuestra proxemia social, nuestras expectativas afectivas, nuestro inconsciente. Aunque las líneas de esta mutación en curso son todavía borrosas, algunos de sus rasgos generales ya son claros y están dentro de nuestro campo de visión. En primer lugar, la proximidad de los cuerpos se ha vuelto problemática y su supervivencia como parte de nuestra vida social se ve crecientemente amenazada. En segundo lugar, la propagación del sufrimiento en la era pandémica (no solo del sufrimiento médico, sino también del sufrimiento económico, del sufrimiento social y, en último término, del sufrimiento mental) ya está alcanzando niveles tan intolerables que una forma de inmunización contra la emoción puede volverse dominante: el autismo y la alexitimia podrián introducirse en la psicoesfera como internalización del rechazo a sentir las emociones de los otros, y posiblemente también las emociones propias. Lo que describo en este libro no es un trayecto de mutación bien delineado, sino un campo de posibiliad magmático en un paisaje de ansiedad.

En la primera parte de este libro, "En el umbral", describiré los efectos de la irrupción del coronavirus en el espacio de la sensibilidad y la imaginación colectiva.

En la segunda parte, "la psicoesfera inminente", intentaré ponderar las tendencias diferentes (e incluso divergentes) que se hallan inscriptas en la mutación psicológica en curso, en tranto esta afecta a las esferas de la sexualidad, la proxemia social y el deseo.

En la tercera y última parte, "Devenir nada", esbozaré el paisaje de este siglo tal como aparece ante mí desde la perspectiva del presente: un mundo avejentado, un agotamiento de recursos tanto físicos como nerviosos, la extinción como dirección de nuestro tiempo. Solo un nuevo movimiento de la imaginación sería capaz de disipar este horizonte de probabilidad.

Pero si este es el nuevo horizonte del inconsciente, debemos recordar otra vez que el inconsciente no es déposito, sino un laboratorio. La pregunta más urgente no es qué percibe o proyecta desde sí mismo el inconsciente. La pregunta es: ¿cómo hará el tercer inconsciente para encontrar una salida de sus propias pesadillas?

