---
title: Enero 2021
---

# Cambá CTTE Ezine Enero 2021

## Canción
```
En el desierto del alma
Hay que sembrar
Buscando che bien adentro
El agua siempre esta
El agua siempre esta
En esta tieras despojas
La corona sigue aca
Aunque vestida de moda
Se esconde en su disfraz
Se esconde en su disfraz
Por unos pocos peregiles
Que suelen plantarse acá
Nos fuimos desencontrando
Y nos tiraron pa' atras
No nos vayamos pa' atras
Que nos gane la tristeza
Que hoy nos viene a buscar
Nos quiere llevar muy lejos
Y yo me quiero quedar
Y yo me quiero quedar
Si el musgo crece en la piedra
Sin miedo a la sequedad
Busquemos un rinconcito
Y seamos un yuyo mas
Y seamos un yuyo mas
Y seamos un yuyo mas
Recuperemos la tierra
Y vivamos en libertad
Y si hay que mostrar las espinas
Vamo a mostrarlas noma
Vamo a mostrarlas noma
En el desierto del alma
Buscando che bien adentro
El agua siempre esta
Si el musgo crece en la piedra
Sin miedo a la sequedad
Busquemos un rinconcito
Y seamos un yuyo mas
Y seamos un yuyo mas
Y seamos un yuyo mas
```

Canción llamada [*Saya del yuyo*](https://www.youtube.com/watch?v=Cj96DvZl2WU) del disco *Cuando salga el sol* de la banda [*Arbolito*](https://es.wikipedia.org/wiki/Arbolito_(banda))

## Herramientas de software

- [Circuit Python](https://circuitpython.org/) - Lenguaje de programación diseñado para simplificar la experimentación y aprendizaje en microcontroladores. [gtihub](https://github.com/adafruit/circuitpython/)
- [Radicle](https://radicle.xyz/) - Aplicación p2p para reemplazar gitlab o github
- [Vocdoni](https://gitlab.com/vocdoni) - Vocdoni es un stack abierto y descentralizado para votaciones y gobierno electrónico
- [Vetur](https://github.com/vuejs/vetur) - Tooling Vue para VS Code.
- [TabFS](https://github.com/osnr/TabFS) - Montar tu filesystem las tabs del navegador
- [Thefuck](https://github.com/nvbn/thefuck) - Corrector automatico de comandos equivocados en la terminal.

## Informes/Encuestas/Lanzamientos/Capacitaciones
- Algunos resultados de la encuesta anual [State of JS 2020](https://2020.stateofjs.com/es-ES/)
    - Tecnología más utilizada: [TypeScript](https://www.typescriptlang.org/) (Otros finalistas: [Next.js](https://nextjs.org/) y [Cypress](https://www.cypress.io/))
  - Mayor Satisfacción: Testing Library (Otros finalistas: [Jest](https://jestjs.io/) y [GraphQL](https://graphql.org/))
  - Mayor interés: [GraphQL](https://graphql.org/) (Otros finalistas: [Jest](https://jestjs.io/) y [Snowpack](https://www.snowpack.dev/))
  - [Recursos JS mas utilizados](https://2020.stateofjs.com/es-ES/resources/)
- [Port del Linux Kernel para Nintendo 64](https://www.phoronix.com/scan.php?page=news_item&px=Nintendo-64-Linux-2020-Port
) [Código fuente](https://lore.kernel.org/linux-mips/20201225190503.12353218812e1655f56f0bf8@gmx.com/T/#m0862c3484e0da7195dc8989421d30f01b3b1c63a)
- [Aplicaciones de mensajería segura](https://www.securemessagingapps.com/) Comparativa entre distintos sistemas de mensajería
- [Sony publica un driver oficial para el control de PlayStation 5](https://www.phoronix.com/scan.php?page=news_item&px=Sony-HID-PlayStation-PS5)
- [Github y requerimientos para la  autenticación](https://github.blog/2020-12-15-token-authentication-requirements-for-git-operations/)

## Noticias, Enlaces y Textos

### Sociales/filosóficos
- [Entrevista de Soledad Barruti a Vandana Shiva](https://www.lavaca.org/mu147/fase-vandana/)
- [La explotación de la libertad - Byung chul chan](https://www.bloghemia.com/2021/01/la-explotacion-de-la-libertad-por-byung.html)
- [Documental de sobre Aaron Swartz](https://www.youtube.com/watch?v=7jhdj0vKbYo)
- [Resumen del libro de Criptocomunismo](CTTE-text-criptocomunismo), libro escrito por [Mark Alizart](https://fr.wikipedia.org/wiki/Mark_Alizart) de [Ediciones Cebra](https://edicioneslacebra.com.ar/), [Bajar](https://b-ok.lat/book/5562047/de79a0)


### Técnicos
- [React server components](https://addyosmani.com/blog/react-server-components/)
- [Comunicación de componenes parent/child en Vue](https://markus.oberlehner.net/blog/events-and-callbacks-parent-child-component-communication-in-vue/)
- [Discusión sobre vendoring](https://lwn.net/Articles/842319/)


### Otras

- Columna de Tecnología de Neto, Pinky y Cerebro, en Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
   - Entrevista a Leandro Ucciferri de [ADC](https://adc.org.ar/) - [Audio](https://archive.org/details/nota_leandro_adc)

