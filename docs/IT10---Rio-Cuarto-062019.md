# Visita a IT10, en Río Cuarto

Aprovechando la cercanía a Rio Cuarto, mientras estaba en Las Chacras (Traslasierra), me acerqué a Merlo y me tomé el bondi de las 10 para llegar al mediodía a la segunda ciudad más grande de Córdoba. Lo primero que me llamó la atención fue grandes edificios en construcción frente a la terminal, luego me enteraría que son proyectos inmobiliarios que quedaron truncos, a causa de la crisis económica que vive la ciudad, con su actividad orientada al campo y a la (cada vez menos rentable) industria.

Venia charlando con Daniel(1), de IT10, que me pasó la dirección de su oficina, en el centro de la ciudad. Según el mapa estaba a unas 10 cuadras de la terminal, por lo que arranqué caminando. Mientras llegaba al centro, vi una ciudad con muchisimo movimiento y con estudiantes en la calle (hay varias universidades). Justamente, lxs amigxs de IT10 vienen del palo de la militancia estudiantil.


## IT10

Llegué a una pequeña oficina en el primer piso de una galería comercial y ahi me recibieron los IT10. En la oficina eran cuatro, y en total son cinco, el quinto, Daniel(2), llegaría por la tarde. 

Me estaban esperando para almorzar, así que eso hicimos, y ahí empezamos la charla con Daniel(1), desde la realidad en nuestras coopes, el vínculo con las actividades militantes, software libre, género, "el sistema", en fin, las mismas charlas *detodistas* de siempre.

Luego me contaron que vienen realizando varios proyectos, uno de ellos para un cliente de UK. Algunos trabajan full time y otros tienen otros trabajos vinculados a la universidad. Después del almuerzo laburamos un rato, y fuimos charlando acerca de los proyectos y las tecnologías que utilizan. También me mostraron el cuartucho donde están preparando un clúster de servidores, y donde además tienen varias herramientas para el desarrollo de la placa con un arduino integrado que utilizan para su proyecto educativo.

### Edukit10 

Estos locos vienen hace varios años con un proyecto de [robótica educativa](https://edukit.it10coop.com.ar/), y para eso desarrollaron una placa con un arduino y varios componentes, que utilizan en sus talleres. Charlamos acerca de lo frustrante que es a veces tener la idea del proyecto y no poder hacerlo sustentable, presentar proyectos y que nunca te den bola, o directamente te los choreen, e inclusive, que te digan que no vale la pena presentarse a una licitación porque ya está arreglada...

Eso, más su fuerte impronta de militancia social, los llevó a desistir de tratar de hacer sustentable (en plata) el proyecto, y se dedicaron a dictar talleres en los lugares donde FECOOTRA, su federación, tiene presencia territorial. Es así que están haciendo actividades en un barrio super detonado de la ciudad. La historia de ese lugar es que ese barrio estaba a orillas del Río Cuarto, que divide la ciudad, y como hay un proyecto inmobiliario para poner chetas las orillas del río, desplazaron un barrio entero, y ellos están yendo al lugar donde re-localizaron a la gente.

Al comentarles nuestro recorrido con LTC, nos dimos cuenta que nosotros tenemos desarrollado la plataforma didáctica, con las bases pedagógicas y el espacio virtual para apoyo de las actividades, pero no el kit... y ellos, tienen el kit y les falta lo otro! Entonces identificamos ahí una buena linea para laburar en conjunto.

### La historia de IT10

Daniel(2) es socio fundador y el único que quedó de esa camada. Yo lo conocí hace varios años, cuando IT10 participaba de FACTTIC, pero luego se alejaron. Charlando por la tarde, me contó que les costó mucho armar grupo, y que varios se fueron yendo. Intentaron trabajar con su proyecto de robótica educativa, pero nunca lograron formalizarlo. También charlamos acerca de los vínculos con la universidad (Daniel es docente y participa de varios espacios de la uni) y de cómo articular con las cooperativas de tecnología, hablamos desde liberar contenidos académicos hasta desarrolar software para data science. 

Luego, con la incorporación de los socios actuales, empezaron a ponerle laburo más comprometido y la cosa comenzó a tomar vuelo, gracias al trabajo sostenido. Ahora, nuevamente en FACTTIC, están participando y se los ve entusiasmados.

La cosa es que con Daniel(2), salimos a fumar un pucho y estuvimos como una hora y media charlando, y casi llegamos tarde al asado que nos esperaba en el taller gráfico [Bases](https://www.facebook.com/CooperativaBases/), otra cooperativa de FECOOTRA.

### Asadazo cooperativo

Bases es una cooperativa gráfica que arranca de un grupo de militantes estudiantiles que consiguen unas máquinas viejas de imprenta, donadas por una gráfica que actualizó sus máquinas. Ahí comenzaron a imprimir [El megáfono](https://elmegafono.net/que-es-el-megafono/), un medio de comunicación popular de distribución gratuita. Luego crecen el taller gráfico a la provisión de otros servicios del rubro.

Uno de sus socios, Carlos Leigedder, es el representante de juventud ante la ACI, y varios se cruzaron con lxs cambá en el evento de juventud de Uruguay.

El asado estuvo genial, varixs de ellos estaban a las corridas cerrando la edición de El Megáfono, mientras el resto meta rosca en la mesa. Hablamos de Cooperar (escuche varias historias interesantes contadas de primera mano), de la ACI, del sentido social de nuestras empresas, de si tenemos que generar guita o no, es decir, una bocha.

Asi se hicieron las 3 de la mañana, y mi bondi salía a las 6, así que me tiré un par de horas y luego arranqué para la terminal, ya con ganas de volvet a visitarlxs.