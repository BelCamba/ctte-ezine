---
title: Abril 2023
---

# Cambá CTTE Ezine Abril 2023

<div style="text-align: center;">

![Libros](@img/abril2023.png)

</div>


Leído por ahí:
```
El problema no es que la Inteligencia Artificial pueda reemplazar a los humanos,
sino que demasiados humanos actúan como bots y trolls. Si tus modos de decir y
escribir pueden ser sustituidos por automatismos computacionales es porque no
te estás jugando nada al hacerlo, poco importa si cuentas con mucha o poca
información. Si no escribes con el cuerpo, si al decir no expones tu herida,
lo mismo valdría que fueses una máquina funcional a la acumulación de goce o
volcada a la destrucción del planeta. Tu vida no vale nada.
```

## Contribuciones Artísticas
- Nuestra artista visual favorita participa de la Muestra "Más libros para más memoria": [Su intervención](https://mariabelensanchez.wordpress.com/2023/04/03/mas-libros-para-mas-memoria/)


## Herramientas de software

- [Newsboat: Lector RSS para la terminal](https://newsboat.org/)
- [Ctop: Monitorear contenedores en la terminal](https://ctop.sh/)
- [Lazygit: Simple UI de terminal para comandos de git](https://github.com/jesseduffield/lazygit)
- [Evals: OpenAI Evals: Como evaluan sus modelos](https://github.com/openai/evals)
- [Furnace: Chiptune tracker](https://github.com/tildearrow/furnace)

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Encuesta de Sueldos de Sysarmy 2023 #1](https://sysarmy.com/blog/posts/resultados-de-la-encuesta-de-sueldos-2023-1/)
- [Flashparty 2023: Somos satélite Oficial de Revisión Flashparty, 8 y 9 de abril en Buenos Aires](https://flashparty.rebelion.digital/index.php?option=com_content&view=article&id=114:somos-satelite-oficial-de-revision&catid=2&lang=es&Itemid=132)
- [BattleMesh v15 en Barcelona](https://exo.cat/battlemesh-v15-en-calafou-barcelona/): Convención para promover temas como el derecho a la conectividad, la infraestructura comunal y los proyectos libres y abiertos relacionados con las redes malladas, las redes comunitarias y el desarrollo del firmware Openwrt
- [Crear un identificador contenido libre de AI](https://notbyai.fyi/)
- Repositorios de proyectos del Pycamp 2023: [Http-tryout-server](https://github.com/facundobatista/http-tryout-server) - [pyempaq](https://github.com/facundobatista/pyempaq/) - [pytimeline](https://github.com/cacrespo/pytimeline) - [wp-dialer-desktop](https://github.com/matiasdemarchi/whatsapp-dialer-desktop) - [mini-autos](https://github.com/PyAr/mini-autos)- [simpyt](https://github.com/fisadev/simpyt) - [PyCampetes](https://github.com/WinnaZ/PyCampetes)


## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Declaración de Montevideo sobre Inteligencia Artificial y su impacto en América Latina](https://www.fundacionsadosky.org.ar/declaracion-de-montevideo-sobre-inteligencia-artificial-y-su-impacto-en-america-latina/)
- [Estereotipos y Discriminación en Inteligencia Artificial](https://huggingface.co/spaces/vialibre/edia) - [Video EDIA](https://www.youtube.com/watch?v=ZLV1ex2S6YI)

#### GPT4
- Leer ["potential for risky emergent behaviors" del paper de GPT4 ](https://cdn.openai.com/papers/gpt-4.pdf)
- [Decepciones e ideas alucinantes](https://www.youtube.com/watch?v=6Hewb1wlOlo)
- [Teoria de la mente: La consciencia de la IA & desacuerdos en OPENAI](https://www.youtube.com/watch?v=4MGCQOAxgv4)
- [Podcast de Lex Fridman - Entrevista a Sam Altman: CEO de openai](https://www.youtube.com/watch?v=L_Guz73e6fw)

### Tecnología
- [Ingeniería revresa de prompts para diversión y ganancia](https://lspace.swyx.io/p/reverse-prompt-eng)
- [Programemos Tetris con Rust, WASM, y React]( https://www.youtube.com/watch?v=_lAr7JveRVE)
- [Ejemplo con Rust y RayCasting](https://grantshandy.github.io/posts/raycasting/)
- [Programación Asincrónica en Python](https://superfastpython.com/python-asynchronous-programming/)
- [Creando Fractales en Python](https://towardsdatascience.com/creating-fractals-in-python-a502e5fc2094)
