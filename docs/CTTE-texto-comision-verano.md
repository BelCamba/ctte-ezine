# ¿Como hizo la comisión de verano para no saber los nombres que le tocaban a cada persona?

Una tarde en alguna sala de cambá un grupo de intrepidas personas tuvieron una discusión que potencialmente
podia ser transcental para la organización de las Jornadas de reflexión de verano.
Ahí sobre la superficie del querer hacer se dío en el llano una pregunta que revoluciono las sienes en juego.
El dilema en cuestión era como hacer repartir los nombres del "amigx invisible" de tal manera que no hubiera trampa (o sea tener un conocimiento sobre a quien le toca que).
Como buenas informáticas alguien propuso para ese motivo [una apliación](https://play.google.com/store/apps/details?id=com.thelittletechie.secretsanta&rdid=com.thelittletechie.secretsanta), ¿quizas fuera una discusión trivial?, pero trivial o no se selecciono un algoritmo y se hizo de modo análogico:

- Dos socios encargados de repartir los nombres (A y B)
- Dividir la cope en dos grupos(1 y 2)
- El socio A no está en el grupo 1 y el socio B no está en el grupo 2.
- El socio A prepara las cajitas de regalo del grupo 1 y el socio B hace lo propio con el 2.
