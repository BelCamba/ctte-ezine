---
title: Febrero 2021
---

# Cambá CTTE Ezine Febrero 2021

## Canción
```
Oh, oh, oh
Yo seré lo que quiera ser!
Una estrella fugaz
Cualquiera...
Viajera...
Agárrate bien fuerte de mí
Que vamos a salir volando
Hacia otro lugar...
Más lejos...
Lejos... lejos de acá
Vos serás
Lo que quieras ser!
Una ola de mar salvaje...
Con su tempestad...
Yo me agarro muy fuerte de vos
Y me dejo llevar hacia otro lugar
Lejos de acá
```

Canción llamada [*Ola salvaje*](https://www.youtube.com/watch?v=oWFsm5I6wto) del disco *Espejismos* de la banda [*El otro yo*](https://en.wikipedia.org/wiki/El_Otro_Yo)

## Herramientas de software

- [Cloudgram](https://github.com/dedalusj/cloudgram) - Generación de diagramas por código - [Demo](https://cloudgram.dedalusone.com/)
- [Wger]( https://github.com/wger-project/wger) - Sistema para hacer seguimiento de entrenamiento y peso hecho en django.
- NASA (Software de la Administración Nacional de Aeronáutica y el Espacio de USA)
    - [Fprime](https://github.com/nasa/fprime) - Plataforma y framework para manejar el software de vuelo para sistemas embebidos (utilizado en los helicópteros autónomos en Marte)
    - [OpenMCT](https://github.com/nasa/openmct) - Tecnología abierta para el control de misiones (visualización de datos)
-  [Big Sleep](https://github.com/lucidrains/big-sleep) - Herramienta para generar imagenes a partir de texto, usando OpenAI's CLIP y BigGAN
- [Barrier](https://github.com/debauchee/barrier) - Software Libre para KVM (Control de multiples maquinas potencialmente con distintos SO desde una sola maquina)

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Codersrank 2021: Encuesta de preferencias sobre busqueda de trabajo de desarrolladorxs](https://blog.codersrank.io/2021-developer-job-search-preference-survey/)
- Lanzamiento del segundo número de la [revista Ludorama - Ciencia del juego](https://wg4fest.com/ludorama-revista-videojuegos/ludorama-verano-2021/)
- [Release de Vuex 4.0](https://github.com/vuejs/vuex/releases/tag/v4.0.0)
- [Release de Vite 2.0](https://dev.to/yyx990803/announcing-vite-2-0-2f0a)

## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Esteban Magnani hace una reseña de "The age of surveillance capitalism (Hachette Book Group, 2019) de Shoshana Zuboff"](https://revistas.unlp.edu.ar/hipertextos/issue/view/756/342) en la Revista Hipertextos
- [El sentido interno de la capitanía](https://fs.blog/2021/02/inner-sense-of-captaincy/)

### Técnicos

- [Porque los cientificos se están volcando a usar Rust](https://www.nature.com/articles/d41586-020-03382-2)
- [Cumpleaños de 30 de Python](https://www.theregister.com/2021/02/20/happy_birthday_python_youre_30/)
- [Decisiónes sobre tecnología con ADRS y Radares](https://stackshare.io/posts/how-github-spotify-and-ebay-use-adrs-and-tech-radars-for-technology-decisions) - [Construir tu propio radar](https://github.com/thoughtworks/build-your-own-radar)
- [Historia: Como el router más famoso de Linksys, el WRT54G, se convirtio en legendario por una funcionalidad sin documentar](https://www.vice.com/en/article/qjpnpb/the-famous-router-hackers-actually-loved)

### Otras

- [Camino a Salto Escondido (Salto más alto de la provincia de San Luis) en el pueblo San Francisco del Monte de Oro](https://www.youtube.com/watch?v=EXbDlYsFrsk) - Fue unos de los lugares del itinerario de  las últimas vacaciones de @josx.

- [La Runfla](https://www.facebook.com/larunfla.teatro), teatro callejero, estrenó **FRAGMENTOS DE OSCURIDAD, los caprichosos objetos del destino"** y cumple 30 años. Se puede ir a ver a la gorra los Sábados las 21hs, bajo las estrellas de Parque Avellaneda, CABA. Reservás al 1136287542.
