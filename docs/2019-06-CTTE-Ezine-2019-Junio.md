---
title: Junio 2019
---

# Cambá CTTE Ezine Junio 2019

## Igor Stravinsky dixit

```
Mi libertad entonces consiste en moverme en un entorno delimitado que me asigne a mi mismo
para cada una de las cosas de hago. Es más, mi libertad será mayor y más significativa cuanto
más límites al campo de acción y más obstaculos tenga que sortear. Cualquier cosa que disminuya
las restricciones también hacer disminuir la fuerza. Cuanto más restricciones uno se impone,
más uno se libera de las cadenas que atán al espíritu.
```

> Extraido y traducido del libro [Poetics of Music in the form of six lessons](https://monoskop.org/images/6/64/Stravinsky_Igor_Poetics_of_Music_in_the_Form_of_Six_Lessons.pdf) de [Igor Stravinsky](https://es.wikipedia.org/wiki/%C3%8Dgor_Stravinski)

## Aportes a proyectos de Software libre

- Sergio Ivanic continua agregando funcionalidad a [Vue-admin](https://github.com/Cambalab/vue-admin/tree/develop)
- Nueva versión de [ra-data-feathers 2.1.0](https://github.com/josx/ra-data-feathers/releases/tag/v2.1.0)

## Herramientas de software

- [Zdog](https://zzz.dog/) - Motor pseudo 3d para estructuras chatas y redondeadas amigable para el diseñador.
- [Workrave](http://www.workrave.org/) - Asistente para la prevención y período de recuperación de [lesiones por esfuerzos repetitivos](https://es.wikipedia.org/wiki/Movimientos_repetitivos)
- [Tui.grid](https://ui.toast.com/tui-grid/) - Grilla basada en [TOAST UI](https://ui.toast.com/)
- [Ramda](https://ramdajs.com/) - Biblioteca especifica para usar paradigma funcional para Programadorxs JS.

## Informes/Encuestas

- Nueva versión de Angular, ahora disponible su [versión 8](https://blog.angular.io/version-8-of-angular-smaller-bundles-cli-apis-and-alignment-with-the-ecosystem-af0261112a27)
- [JavaScript trends of 2019](https://cvcompiler.com/blog/game-of-frameworks-javascript-trends-of-2019/) <br>
  Un listado de las habilidades/tecnologías que los programadores javascript necesitan saber. En orden de importancias: React, Angular, Node.js, Git, Redux, Ecmascript, Java, webpack, Typescript, jquery, aws, CI, Rest APIs, SQl, Docker, Python, Jest, Vue.js, TDD, Design Patterns, Unit testing, npm, React native, Mocha, OOP, GraphQL

### Notebooks con gnu/linux

- Dell lanza nuevos modelos de notebooks para *developers* con Ubuntu 18.04 instalado. [Precision 5540, 7540, 7540](https://bartongeorge.io/2019/05/29/ladies-and-gentlemen-introducing-the-dell-precision-5540-7540-and-7540-developer-editions/) [Precision 3540, 3541](https://bartongeorge.io/2019/05/01/announcing-the-budget-friendly-linux-based-dell-precision-mobile-workstations/)
- [System76](https://system76.com/) lanza [Gazelle](https://system76.com/cart/configure/gaze14)

### Visualizaciones, Estadísticas y rankings sobre Desarroladorxs

> Acá algunos sitios interesante para poder visualizar datos de desarrolladorxs

- [artzub](http://ghv.artzub.com)
- [git-awards](http://git-awards.com)
- [codersrank](https://codersrank.io)


### State of Css 2019

- [Resultados finales](https://2019.stateofcss.com/)

> En líneas generales la encuesta fue contestada por desarrolladorxs mayormente de frontend (En particular 11% mujeres + disidencias). Para los equipos que trabajan mucho con la parte visual en frontend es muy interesante para poder justificar opioniones, caminos a seguir/tomar, nuevas funcionalidades, herramientas para conocer, etc.


### Participar en la encuesta de TC39

Mozilla esta haciendo encuestas directamente a les que programan para evaluar la inclusión de nuevas funcionalidad en el estandar de Javascript basada en evidencia. Algo más [sobre la motivación](https://hacks.mozilla.org/2019/05/javascript-and-evidence-based-language-design/).

Acá la [encuesta del grupo TC39](https://qsurvey.mozilla.com/s3/2019-TC39-Feature-Experiment) que esta trabajando sobre las nuevas features.


## Noticias, Enlaces y Textos

### Más sociales

- ¿Cómo hacer crecer el liderazgo distribuido? [Versión Gitlab](CTTE-texto-crecer-lideres-distribuidos), [Versión publicada por Alanna Irving en Medium](https://medium.com/@alanna.irving/como-hacer-crecer-el-liderazgo-distribuido-be28a39520d8)
- [La sociedad del cansancio](CTTE-texto-sociedad-del-cansancia)
- [El Valor de la Libertad - Jose Pepe Mujica](https://www.youtube.com/watch?v=WR0WBXXXwI0)
- @Charlie nos recomendo leer una [entrevista](CTTE-texto-dejours) de [Christophe Dejours](https://es.wikipedia.org/wiki/Christophe_Dejours)
- Entrevista a Gerardo Richarte - [Debemos ser hackers o dominados](https://www.lanacion.com.ar/tecnologia/gerardo-richarte-debemos-ser-hackers-o-dominados-controlas-la-tecnologia-o-ella-te-domina-a-vos-nid2251996) <br>
  Este personaje es a mi entender una de las personas argentinas más importantes en nuestra área a nivel técnico.
- [996ICU](CTTE-texto-996icu)
- [Molecula de la cultura de Satellogic](CTTE-texto-satellogic)


### Más técnicos

- [Reemplazar gifs y videos por videos av1](https://www.singhkays.com/blog/its-time-replace-gifs-with-av1-video/)
- [El límite de carácteres por línea en el kernel](https://www.linuxjournal.com/content/line-length-limits) <br>
  Para pensar: 1) ¿Cómo se modifican y discuten las prácticas habituales?  2) Leer en clave filosófica el argumento que brinda [Linus Torvalds](https://es.wikipedia.org/wiki/Linus_Torvalds)
- Interesante articulo que expone diferentes enfoques: Eventos vs Reactividad y como hicieron para [Construir un sistema reactivo similar a vue en 0.7kb](https://medium.com/@toastui/building-a-reactivity-system-similar-to-vue-in-under-0-7kb-58dd705df4a7)
- ¿Cómo Obtener promedio de array? - [Programación funcional](https://jrsinclair.com/articles/2019/five-ways-to-average-with-js-reduce/) - [Magical, Mystical JS TRANSDUCERS](https://jrsinclair.com/articles/2019/magical-mystical-js-transducers/)
- Primer RFC de Vue.js - [Function API](https://github.com/vuejs/rfcs/blob/function-apis/active-rfcs/0000-function-api.md#high-level-qa)
- Vue.js tomando estrategias de React - [Lifecycle hooks](https://blog.logrocket.com/introduction-to-vue-lifecycle-hooks/)

### Seguridad

- Se ha encontrado la Vulnerabilidad remota llamada [BlueKeep](https://www.hackplayers.com/2019/05/ya-llega-bluekeep-rce-en-rdp.html) [1](https://github.com/zerosum0x0/CVE-2019-0708) [2](https://github.com/Ekultek/BlueKeep) que afecta a casi todos las versiones windows en el protocolo [RDP](https://en.wikipedia.org/wiki/Remote_Desktop_Protocol) (remote desktop protocol).
- Conocidos de la casa: [Javier Smaldone](https://blog.smaldone.com.ar/) y también [hackan](https://hackan.net/) dan a conocer filtración de datos de ciudadanos a través de la web del Ministerio del Interior. [Articulo en infotechnology](https://www.infotechnology.com/online/Alertan-por-un-hackeo-a-la-base-de-datos-del-Estado-todos-los-CUIT-y-claves-del-DNI-en-peligro-20190618-0009.html)
- Agentes policiales detienen a cooperativista asociado a BITSON por equivocación del sistema de reconocimiento facial [Su Historia](https://mobile.twitter.com/lecovi/status/1144072654598619137)


### Otras

- [Facebook, no microsoft, es la amenaza para el software libre](https://www.linuxjournal.com/content/facebook-not-microsoft-main-threat-open-source)
- Transformar dibujo cara de personaje de DOOM en foto, post de [reddit](https://www.reddit.com/r/interestingasfuck/comments/c1a9vc/neural_network_generated_drawings_of_the_man_from/ercpve4/), algunos de los software utilizados [stylegan](https://github.com/NVlabs/stylegan), [waifu2x](https://github.com/nagadomi/waifu2x)
- Entrevista del 2010 de la [Oveja Electrónica](http://ovejafm.dreamhosters.com/) a [Paola Raffetta](http://www.paolaraffetta.com.ar/) sobre Amor libre, Software libre y Apostasía colectiva. [Audio](./assets/paola_raffetta.ogg)


## Propuestas de contribuciones

- Arreglar bug [Forwarded media is not rendered properly ](https://github.com/majn/telegram-purple/issues/425) en [telegram-purple](https://github.com/majn/telegram-purple/)
- Este proyecto es muy interesante y quizás a alguien le interese probar y hacer una devolución. [Webthings](https://hacks.mozilla.org/2019/04/introducing-mozilla-webthings/)
- Agregar más que envío de solo mensajes a [Plugin WP para purple](https://github.com/EionRobb/purple-gowhatsapp)
- Mejorar soporte para [Plugin Instagram para purple](https://github.com/EionRobb/purple-instagram)
