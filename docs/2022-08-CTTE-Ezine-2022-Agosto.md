---
title: Agosto 2022
---

# Cambá CTTE Ezine Agosto 2022

<div style="text-align: center;">

![Alexgray](@img/alexgray.jpg)

</div>

```
Tenía siete años apenas, apenas siete años
Qué siete años!,  No llegaba a cinco siquiera!
De pronto unas voces en la calle
Me gritaron Negra! Negra! Negra!
Soy acaso negra? - me dije SÍ!
Qué cosa es ser negra? Negra!
Y yo no sabía la triste verdad que aquello escondía.
Negra! Y me sentí negra,
Negra! Como ellos decían
Negra! ￼Y retrocedí
Negra! Como ellos querían
Negra! Y odie mis cabellos y mis labios gruesos
Y mire apenada mi carne tostada
Y retrocedí Negra!
Y retrocedí . . .
Negra! Negra! Negra! Negra!
Y pasaba el tiempo, Y siempre amargada
Seguía llevando a mi espalda, Mi pesada carga
Y cómo pesaba!...
Me alacié el cabello, Me polvee la cara
Y entre mis entrañas siempre resonaba la misma palabra
Negra! Negra! Neeegra!
Hasta que un día que retrocedía, retrocedía y qué iba a caer
Negra! Negra! Negra! Negra!
Y qué? Negra!
Si Negra! Soy Negra! Negra soy Negra!
Si Negra! Soy Negra! Negra soy
De hoy en adelante no quiero
Laciar mi cabello
No quiero
Y voy a reírme de aquellos,
Que por evitar -según ellos-
Que por evitarnos algún sinsabor
Llaman a los negros gente de color
Y de qué color! Negro
Y qué lindo suena! Negro
Y qué ritmo tiene! Negro Negro
Al fin comprendí
Ya no retrocedo, Al fin
Y avanzo segura, Al fin
Avanzo y espero, Al fin
Y bendigo al cielo porque quiso Dios
Que negro azabache fuese mi color
Y ya comprendí, Al fin
Ya tengo la llave!, Al fin
Negro Negro Negra soy
```

**Me Gritaron Negra** es uno de los poemas más importantes e intensos de [Victoria Santa Cruz](https://es.wikipedia.org/wiki/Victoria_Santa_Cruz) - [Video](https://www.youtube.com/watch?v=cHr8DTNRZdg)


## Herramientas de software

- [Pretalx](https://github.com/pretalx/pretalx) - Herramientas para planificar conferencias (CfP, calendario, manjejo de oradorxs, etc)
-  Proyecto de investigación [No language left behind](https://ai.facebook.com/research/no-language-left-behind/) - Entrenamiento y generación de modelos de traducción multilingual - [Código fuente](https://github.com/facebookresearch/fairseq/tree/nllb/)
- [Individous](https://invidious.io/) - Alternativa de front-end para Youtube - [Código fuente](https://github.com/iv-org/invidious)
- [WaybackProxy](https://github.com/richardg867/WaybackProxy) - Proxy HTTP para hacer pedidos a través de Internet Archive Wayback Machine
- [Librex](https://github.com/hnhx/librex/) - Motor de busqueda para torrents y por google que respetatu libertad.


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Nerdearla](https://nerdear.la/agenda/) - Conferencia técnica híbrida de dos días durante este 5 y 6 de Agosto de 2022.
- [Resultado de la Encuesta 2022 de Stackoverflow](https://survey.stackoverflow.co/2022/)
- [Google lanza reemplazo experimental para C](https://thenewstack.io/google-launches-carbon-an-experimental-replacement-for-c/) - [Código fuente](https://github.com/carbon-language/carbon-lang)
- [Nuevos lanzamientos de notebooks con linux](https://linux.slashdot.org/story/22/07/10/2019230/six-ground-breaking-new-linux-laptops-released-in-the-last-two-weeks)
- [Revista Digital Software libre, educación y sociedad de Mendax número 1](https://hernanalbornoz.wordpress.com/2022/06/23/revista-digital-no-1-software-libre-educacion-y-sociedad-grupo-mendax/)


## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [2K Hombre Digital - Entrevista: Pat Pietrafesa](https://open.spotify.com/episode/2vrkCp5nc6KiRJnRhbhCbM)
- [Historia del TERFismo (Feminismo Radical Trans Excluyente)](https://www.youtube.com/watch?v=q-QNPZQ49GE)
- [Conferencia de Rita Segato: Instituciones y vulnerabilidad: Pensar la política en clave femenina](https://www.youtube.com/watch?v=lCdXyrdeWvY)
- [Entrevista con la filósofa italiana Chiara Bottici, autora de AnarcaFeminismo](https://www.pagina12.com.ar/387846-no-podemos-combatir-una-forma-de-opresion-sin-combatirlas-to)


### Sociales/filosóficas

- [El Método Rebord #32 - Juan Grabois](https://www.youtube.com/watch?v=PbOTsaLBlfE) - Entrevista
- [Slavoj Žižek & Yuval Noah Harari | Debemos confiar en la naturaleza más que en nosotras mismas?](https://www.youtube.com/watch?v=3jjRq-CW1dc)
- [Punto de Emancipación 21 - AMADOR SAVATER y JORGE ALEMÁN](https://www.youtube.com/watch?v=NYK9aLpFlws)
- [DRONEGOD$ Manifiesto Hacker](https://www.youtube.com/watch?v=B6fl5zOqImA) - Booktrailer del libro Detalle Infinito, de Tim Maughan (Caja Negra, 2022)

### Tecnología

- [TensorFlow Lite para Commodore 64s](https://www.hackster.io/nickbild/tensorflow-lite-for-commodore-64s-29e0db)
- [Haciendo real demo graficas pequeñas en webassembly](http://cliffle.com/blog/bare-metal-wasm/)
- [Empaquetando demos en PNG para Web](https://github.com/daeken/Benjen/blob/master/daeken.com/entries/superpacking-js-demos.md) - [Implementación](https://gist.github.com/gasman/2560551)
- [Iconburst NPM: Ataque a la cadena de valor que recoleta datos desde apps y sitios web](https://blog.reversinglabs.com/blog/iconburst-npm-software-supply-chain-attack-grabs-data-from-apps-websites)


### Cooperativas/Economía Popular

- [Facttic 10 años](https://fb.watch/erO7Kg1pYf/) - [Nota ansol](https://ansol.com.ar/facttic-celebra-sus-diez-anos-queremos-que-la-tecnologia-este-del-lado-de-la-gente/tecnologia/)
- [ROMEO JOVEL en INFO ▶️ SOBERANA #15](https://www.youtube.com/watch?v=P9baqnMoNw8) - Entrevista a integrante de [Redjar](https://redjar.com.ar/)
- [OrgulloCooperativo | Cooperativa Eryx Ltda](https://www.youtube.com/watch?v=aQt5MmY7XdU)
- [Cooperativa Obrera abre Centro de desarrollo informático y tecnológio](https://www.essapp.coop/noticias/un-centro-de-desarrollo-informatico-y-tecnologico)
