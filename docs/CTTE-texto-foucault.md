# La Historia de la sexualidad - Michel Foucault
> [HISTORIA DE LA SEXUALIDAD DE MICHEL FOUCAULT | Darío Sztajnszrajber es #DemasiadoHumano -Ep.07 T7](
https://www.youtube.com/watch?v=hkQBtJnK2-0)

Sus ideas enfrentan la idea del poder como externo, soberano, sustantitiva, entidad, la idea represiva
No existe el poder sino los efectos, las relaciones, micropoderes. Micropoderes que generan efectos que generan individuos

Hay 4 tomos y tratan sobre la Relación entre sexualidad y poder

- 1 La voluntad de saber  (es mas autonomo, tratado del poder)
- 2 El uso de los placeres
- 3 La inquietud de si
- 4 Las confesiones de la carne (hace unos años apocrifo)

Modos de vivencia de la vida sexual es siempre algo inducido, supone dispositivos previos.
El deseo sexual y la percepcion del mismo esta arraigado a la naturaleza intima es una construcción.
Las formas mas arraigadas al cuerpo es donde el disciplinaiento ejerce el control mas absoluto.

El poder no solo reprime sino normaliza, invade, genera cuerpo más productivos, politicamente dociles
Autodisciplina frente al espanto y obsesion frente al placer. El cuerpo es un territorio de batalla.
Modalidades de vivenciar ciertas sensaciones, percepciones

Objetivo ultimo del poder hasta la modernidad, exlusividad en hacer morir.
En contraposición la Biopoitica: Elel poder de hacer vivir, produce cada intimidad de nuestra experiencia vital normalizada.

¿Como logran los dispositvos que seamos como somos?
Estrategias a traves del sexo, el dispositivo de la sexuialidad es decir a traves de saberes, instituciones nos han hecho creer que en nuestro sexo se cifra la clave esquiva de nuestro ser, nuestra verdad ultima. Para ser mas libre hay que acceder a ese nucleo oscuro, introspeccion busqueda eterna de una identidad oculta que nos atornilla a un punto inmovil.

¿Qué propone? Desenroscarse, divorciar la sexualidad de la identidad de la verdad de su deseo. Superficies del placer, superficialidad del sexo.
