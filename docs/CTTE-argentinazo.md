# Argentinazo - 19/20 Diciembre 2001

> Pasaron 20 años desde el diciembre de 2001. Y muchas personas como yo, lo consideramos un nacimiento, ella fue nuesta madre.

**Gaston Riva** - Asesinado por la represión policial en la rebelión popular del 20/12/01

[Placa memorial](https://upload.wikimedia.org/wikipedia/commons/2/20/Buenos_Aires_-_Avenida_de_Mayo_-_Gast%C3%B3n_Riva_memorial.jpg)

Poesía escrita en la placa de Mari para Gastón.

```
Tu ruido en el aires
Otros día gris
Amaneció esta mañana.
Las nuves se anudaron
tan blancas como la luz,
tan negras como
el descanso eterno
de tus pisadas.

Aún sigo sin despertarme
de esta pesadilla
que el tiempo reconstruye

Pero lo miembros
que me componen
día a día se desarman
se desgajan,
se atormentan
cómplices de tu ausencia

El efímero calor
de tu sonrisa
me visita en sueños,
en imágenes pasadas...

Solo el invisible
ruido de tu presencia
quedó en el aire sin pausa,
sin distancias

Instalado para siempre
dentro de nuestra casa,
afianzado como nunca.
Despedido sin consultas
de la vida
que sin prisa disfrutabas

Sorprendido sin defensas
en la trampa mortal
de tu pero enemigo.
Y en el callejón
sin salida de la muerte,
de tu caminar firme y libertario
el feroz dictador
se acomoda culpable
en su cárcel
enferma y homicida.

El fuego del infierno
le abre paso.

Mari
```
