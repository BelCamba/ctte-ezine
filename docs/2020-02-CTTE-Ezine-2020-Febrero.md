---
title: Febrero 2020
---

# Cambá CTTE Ezine Febrero 2020

## Canción

```
A veces me hecha de mi propia casa
Una hora antes que me lo merezca
El tiempo es curioso como aquel jurado
De ese show de baile que todos pretenden ganar o participar
Donde se muestra gente al borde

La pregunta es
La vida es un vaso de gaseosa aguada
Como una secuencia de bromas pesadas
Disfrutá de este trago porque al terminar, habrá que pagar
Y quizá pagarlo de más, habrá que insistir
Como lo hicimos tantas veces

La pregunta es
Quién está dispuesto a matar?
Quién está dispuesto a morir?
Quién va a defender?

La pregunta es
Quién va a defenderte de mi?
Quién está dispuesto a luchar?
Quién está dispuesto a luchar por amor?
Quién está dispuesto a pelear?
Quién está dispuesto a pelear por honor?

Por lo que no vale nada
Por lo que no vale nada
Cuál sería la gracia?

La pregunta es
Quién va a reclamar, para qué?
Quién va a reclamar para si?
Quién se va a ensuciar si al final nunca le va a pertenecer?

La pregunta es
Quién va a defenderte de mi?
Quién va a defenderte de mi?
Quién va a defenderte de mi?

Quién va a defenderte?
Quién va a defenderte de mi?

La pregunta es

A veces conspiran en mi propia cara
Con una cascada de p*taradas
No se puede solo desatar el nudo con un estribillo pop
Que lo repetís hasta que lo puede cantar un conjunto de orangutanes

La pregunta es
Quién está dispuesto a matar?
Quién está dispuesto a morir?
Quién va a defender?

La pregunta es
Quién va a defenderte de mi?
Quién está dispuesto a luchar?
Quién está dispuesto a luchar por amor?
Quién está dispuesto a pelear?
Quién está dispuesto a pelear por honor?

Por lo que no vale nada
Por lo que no vale nada
Cuál sería la gracia?

Gracias, quiero que pensemos la pregunta
Y que nos la dejen preguntar

Quién va a reclamar?
Quién va a reclamar?
Quién va a reclamar?
Quién va a reclamar?

La pregunta es
La pregunta es
Quién está dispuesto a matar?
Quién está dispuesto a morir?
Quién va a defender?

La pregunta es
Quién va a defenderte de mi?

La pregunta es
Quién va a defenderte de mi?

La pregunta es
Quién va a defenderte de mi?

La pregunta es
Quién va a defenderte de mi?

La pregunta es
Quién va a defenderte de mi?
Quién va a defenderte de mi?
```

Canción de [Babasónicos](https://es.wikipedia.org/wiki/Babas%C3%B3nicos) - [La pregunta](https://www.youtube.com/watch?v=7Iqj54OHV4A)


## Aportes a proyectos de Software libre

- Cambá libera la 1er versión de [websocket-remote-control-server](https://github.com/Cambalab/websocket-remote-control-server)
- Cambá libera la 1er versión de [websocket-remote-control-client](htttps://github.com/Cambalab/websocket-remote-control-client)
- Cambá libera la 1er versión de [Tracky](https://github.com/Cambalab/Traky)
- Cambá libera la versión [Vue-admin v0.0.9](https://github.com/Cambalab/vue-admin/releases/tag/v0.0.9)
- Cambá libera la versión [ra-data-feathers v2.6.0](https://github.com/josx/ra-data-feathers/releases/tag/v2.6.0)


## Propuestas de contribuciones


## Herramientas de software

- Alternativa a Postman, [PostWoman](https://github.com/liyasthomas/postwoman)
- Calcula el costo real para mantener un buen rendimiento de tu código JS - [Size Limit](https://github.com/ai/size-limit)
- Emulación de PC en JavaScript - [pcjs](https://github.com/jeffpar/pcjs)
- Cliente para spotify en la terminal - [Spotify-tui](https://github.com/Rigellute/spotify-tui)
- Se alcanzo la primer prueba interna de Aplicación [Okupación-devies-app](https://okupacion-devies-app.staging.camba.coop/)
  ```
  Perfil Administrador u: admin@camba.coop p:admin
  Perfil Asociade  u:<reemplazar por tu email en cambá>  p: <lo que hay antes de la arroba en tu email>
  ```
## Informes/Encuestas

- [Linux Preloades](https://linuxpreloaded.com/) - Proveedores de Hardware con Gnu/linux como Sistema Operativo
- [Rising Stars 2019](https://risingstars.js.org/2019/en/) - Resumen anual de popularidad javscript basado en me gusta
  - Ranking de todos los proyectos en orden: Vue.js, Vs Code, React, Vue Element Admin, Svelte, Axios, Ant desing, Typescript, Puppeter, Create react app (Se pueden ver muy bien categorizados en la web).
- [Top developer tools 2019](https://stackshare.io/posts/top-developer-tools-2019)
- [Lanzamiento de Ionic 5](https://ionicframework.com/blog/announcing-ionic-5/) - Aún la versión de Vue aún esta con soporte beta.

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Manifiesto SCUM](https://es.wikipedia.org/wiki/Manifiesto_SCUM) - [Valerie Solanas](https://es.wikipedia.org/wiki/Valerie_Solanas) [**Bajar PDF**](https://www.iztacala.unam.mx/errancia/v1/PDFS_1/POLIETICAS6_SCUMMANIFESTO.pdf)

### Más sociales/filosóficos

- [Reporte anual del CoTech Fund Pilot](https://community.coops.tech/t/cotech-fund-pilot-year-report/1948)
- [Enrique Dussel](https://es.wikipedia.org/wiki/Enrique_Dussel) - Charla de 20 Tesis de teoría política - [Video](https://www.youtube.com/watch?v=45fdW9wDmBE&feature=youtu.be&t=1016)  [Más info](https://www.enriquedussel.com)

### Más técnicos

- [Cursos en videos de VueJs](https://www.vuescreencasts.com/)
- [¿Qué es Rust y porque es tan popular?](https://stackoverflow.blog/2020/01/20/what-is-rust-and-why-is-it-so-popular/)
- @Charlie nos comparte como Gcoop Migro de SugarCRM a SuiteCRM luego de 10 años de desarrollo - [Historia por José Masson](https://twitter.com/JoseMasson/status/1219612115708915716)
- Dan Callahan en GOTO 2019 - WebAssembly más allá del browser  [Video](https://www.youtube.com/watch?v=TGo3vJVTlyQ) - [Slides](https://files.gotocon.com/uploads/slides/conference_15/997/original/WebAssembly%20Beyond%20the%20Browser%20-%20Dan%20Callahan.pdf)



### Otras

- [Audio de la entrevista](https://archive.org/details/coop.camba) a Natalia Suarez sobre Camba.coop en la [Radio Comunitaria la Minga 94.7](https://radiominga.org.ar/)

- A 37 km de distancia, y tras subir más de 1400 metros desde la casa de @neto nos encontramos el [Salto del Tigre](https://ca.wikiloc.com/rutes-senderisme/puesto-el-tono-pueblo-escondido-cascada-salto-del-tigre-desde-merlo-12158836/photo-7453894). [Croquis](./img/CTTE-tigre.jpg) - [Mapa](https://www.openstreetmap.org/#map=17/-32.42850/-64.89452&layers=G)

### Seguridad

- [Colección de enlaces curados sobre hackers, pentesters y investigadores sobre seguridad informática](https://github.com/Hack-with-Github/Awesome-Hacking)
