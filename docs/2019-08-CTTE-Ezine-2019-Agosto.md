---
title: Agosto 2019
---

# Cambá CTTE Ezine Agosto 2019

## Frase

```
La lenta flecha de la belleza no cautiva de un solo golpe, no libra asaltos
tempestuosos, se insinúa lentamente y se apodera de nosotros casi sin que
nos demos cuenta, ...en ensueños.
```

Frase de [Friedrich_Niestzsche](https://es.wikipedia.org/wiki/Friedrich_Nietzsche)


## Herramientas de software

- Manejando configuración para aplicaciónes: [Vue-preferences](https://github.com/streaver/vue-preferences)
- Creador de historias interactivas no líneales: [Twine](http://twinery.org/)
- Framework para gestionar estadísticas: [Cube.js](https://github.com/cube-js/cube.js)

## Informes/Encuestas

- Sale la [version 2 de Vuetify](https://github.com/vuetifyjs/vuetify/releases/tag/v2.0.0)
- Nuevo Router Wireless [Turris Omnia](https://www.turris.cz/en/omnia/) con [nueva versión de Webthings Gateway](https://hacks.mozilla.org/2019/07/webthings-gateway-for-wireless-routers/)
- Comenzó la competencia ["Desarrollando un juego con html5 y js en solo 13k"](https://js13kgames.com/)


## Noticias, Enlaces y Textos

### Más sociales/filosóficos

- Booklets escritos por Kate Whittle e ilustrados por Angela Martin [Del conflicto a la cooperación](https://www.cooperantics.coop/2019/07/01/from-conflict-to-co-operation-revisited/)
- [La salvación de lo bello](CTTE-texto-salvacion-de-lo-bello)
- [La filosofía de la libertad de Spinoza](CTTE-texto-spinoza)
- [Texto de referecnia que expusieron Flor y Neto en la Video Conferencia, La Juventud Cooperativa Frente a la Transformación Digital](CTTE-texto-conferencia-crf)
- [Extracto de tesis: Habita - suelo y cielo](CTTE-text-belen-tesis)


### Más técnicos

- [Resource Adaptive Vue Apps](https://logaretm.com/blog/resource-adaptive-vue-apps/)
- Ionic: [¿Por que usamos Web components?](https://dev.to/ionic/why-we-use-web-components-2c1i)
- Acá el creador de Vue.js en un evento en Singapur del mes pasado - [Evan You on Vue.js: Seeking the Balance in Framework Design](https://www.youtube.com/watch?v=ANtSWq-zI0s)
- Buenas prácticas: [Clean-code-js](https://github.com/ryanmcdermott/clean-code-javascript) y [Frontend Checklist](https://frontendchecklist.io/)
- Performance: [Minifica tus SVGs](https://victorzhou.com/blog/minify-svgs/)
- [Vue Single file Component factory](https://github.com/maoberlehner/vue-single-file-component-factory) - [Repo](https://github.com/maoberlehner/vue-single-file-component-factory)
- [Estructura de datos y algoritmos 2 en javascript](https://github.com/amejiarosario/dsa.js-data-structures-algorithms-javascript)

### Seguridad

- Vulnerabilidades en software de [smartmatic](https://www.smartmatic.com/) para la elecciones Argentinas de 2019  - [Análisis](https://www.vialibre.org.ar/2019/08/03/elecciones-2019-reporte-de-vulnerabilidades-criticas-en-el-sistema-de-escrutinio-provisorio/)
- **¡ATENCION! Al entran a los enlaces del contenido filtrado, es posible que estes cometiendo un delito** [Lagorraleaks 2.0](http://zggtzf2fjdaoazu7777zhlz2qwtwbchpkl5lgca53htfvf2i7umvudid.onion/) - [Más de 700gb de datos](http://zggtzf2fjdaoazu7777zhlz2qwtwbchpkl5lgca53htfvf2i7umvudid.onion/leak) comprometidos de la Policia federal Argentina y la Prefectura. (Para poder verlo hay que configurar [Tor](https://www.torproject.org/download/) en sus maquinas).

### Otras

- Entrevista del 2011 de la [Oveja Electrónica](http://ovejafm.dreamhosters.com/) a [Lila Pagola](https://www.researchgate.net/profile/Lila_Pagola) sobre software libre, arte y mujeres en tecnología. [Audio](./assets/lila_pagola.ogg)
- [LTC en Rafaela](http://ltc.camba.coop/index.php/2019/08/12/iv-festival-de-cultura-libre-en-rafaela/)

