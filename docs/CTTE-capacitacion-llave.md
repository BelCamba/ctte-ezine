## Capacitación Llave

##### Información sobre procesos y flujos de Llave

Para gestionar proyectos actualmente utilizamos distintas herramientas, siguiendo determinados flujos que nos ayudan a mantener la organización interna.

## <div> <img src="https://media.giphy.com/media/RiQsoQAmYOePDb4hXS/giphy.gif"> </div>

## Gestión de Proyectos

### Etapa 0: llega un posible proyecto

##### Cuál es el flujo con Comercial? Canales de Comunicación

##### Proyecto Nuevo - Herramientas de Gestión: Estimación, Propuesta, Project Plan, Primer organización en Nubecita.

---

## Gestión de Proyectos

### Inicio del Proyecto y Desarrollo

##### Comunicación con Cliente, Demos, Status.

##### Proyecto Activo - Herramientas de Gestión: Plantillas de Informe de Horas, Status, Horas Productivas.

---

## Gestión de Proyectos

### Finalización

##### Proyecto Cerrado - Herramientas de Gestión: Restrospectiva.

---

## Información sobre Plantillas

##### Utilizamos plantillas, mantenemos convenciones y definimos nomenclaturas, para facilitar la gestión de cada proyectos en sus diferentes etapas.

Las planillas son una base estándar que vamos modificando con cada proyecto de acuerdo a las necesidades específicas del Cliente, en algunos casos contamos con varias versiones y las mismas estan abiertas a seguir modificandose.

---

#### Nomenclatura general para herramientas de proyecto

Usamos una nomenclatura general para estas plantillas:

* `ORG` - nombre corto de la empresa/organización involucrada, en mayúsculas.
* `XXX` - número incremental, que inicia en **000,** para proyectos con el mismo nombre y de la misma orgnización.
* `template` - nombre del proyecto en [Pascal Case](https://medium.com/better-programming/string-case-styles-camel-pascal-snake-and-kebab-case-981407998841#7e0d).
* `YYYY` - año actual.

---

#### Plantillas para gestión de proyectos

* Estimación
* Propuesta de trabajo
* Project-Plan
* Informe de consultoría
* Informe de horas
* Status de proyecto
* Reporte de horas
* Retrospectiva de proyecto
* Retrospectiva de soporte

---

#### Etapa 0

Se considera un **proyecto nuevo** cuando el proyecto no existe o aún no se ha **formalizado**, ni **aceptado** ninguna propuesta.

**Cuál es el flujo con Comercial? Canales de Comunicación**

1. Como primer vía de comunicación utilizamos el mail. Comercial nos envía información acerca de un posible proyecto a proyectos@camba.coop.
2. Se abre un Issue en el board de Llave en mano Gestión. El mismo cuenta con información sobre los requerimientos. Por lo general, es dónde se van a pilotear las consultas y preguntas con Comercial. En primer lugar vamos a chequear las consultas técnicas que tengamos que hacerle al Cliente. *Puede ser necesaria una Reunión del equipo de Llave junto con Comercial, cabe la posibilidad de que sea con el Cliente también para relevar Requerimientos técnicos. En ese caso la vía de comunicación es el mail.*
3. Una vez hecho el relevamiento de requerimientos, avanzamos con la Estimación para que comercial pueda cranear una Propuesta.

---

### Herramientas de Gestión: Plantillas de Estimación y Propuesta

Cada proyecto nuevo va a tener su propia carpeta donde vamos a guardar los archivos de ésta etapa.

* Herramientas de gestion en nubecita

  Dentro de Nubecita tenemos que **copiar** el directorio `ORG-XXX-template-YYYY` dentro de **propuestas** y renombrar la nomenclatura con el formato indicado anteriormente.
  1. Estimación
  2. Propuesta

---

### 1. Estimación

* Herramienta en nubecita

##### Objetivos y Uso

* La estimación está dividida en Etapas/Hitos, las cuales se espera poder desglosar en tareas asequibles en tiempos aproximados de entre una y treinta horas.
* Es útil arrancar presentando la coyuntura de proyecto, lo que se sabe:
  * quién es el cliente, qué hace, que espera
  * qué features entran en el scope
  * con qué alternativas tecnológicas contamos
  * los dibujos siempre son útiles para reconocer cómo se pensó el proyecto

---

### 1. Estimación

##### Objetivos y Uso

* Redactamos las tareas desglosadas una a una y cada uno estima cuánto cree que se tarda, dejando en la planilla también su Seniority (jr,ssr,sr).
* Comparamos cada estimación grupalmente y discutimos diferencias en base a los criterios que cada une tomó. En éste punto encontramos consideraciones que no hayamos tenido en cuenta y reconocemos realmente qué implica realizar tal o cual tarea.

---

### 1. Estimación

##### Objetivos y Uso

* La planilla cuenta con un *Factor de Seniority*. El mismo se utiliza para obtener la duración real proyecto y depende del seniority de la persona que realice la estimación. *Al estimar las horas y cargar el seniority se calculan automágicamente la cant. de horas para los 3 senioritys.*
* Adicionalmente la Estimación cuenta con descuentos que aplican si el proyecto es de Software Libre o con Impacto Social.

#### Output

Cálculo de horas y tiempo de esfuerzo para poder elaborar una propuesta de trabajo. **Será el input para formalizar una Propuesta**

---

### 2. Propuesta

* Herramienta en nubecita

##### Sobre la Propuesta

* Partes principales de la propuesta:
  * **Objetivos del proyecto** Descripción sobre el sistema o aplicación a desarrollar, junto con tecnologías que se utilizarán. Contemplar características primordiales que se deben cumplir para ésta etapa. Esta sección no se ocupa de describir “el cómo” sino “el qué” se va a hacer. Es importante remarcar features u otros aspectos que le proporcionan un alto valor al cliente.
    * **Consideraciones para el kickoff** Se listan los recursos necesarios con los que se debe contar para iniciar el proyecto. Assets indispensables, bases de datos, credenciales de acceso, etc.

---

##### Sobre la Propuesta

* **Objetivos del proyecto**
  * **Consideraciones para el desarrollo** Consideraciones y disclaimers referidos exclusivamente al desarrollo del producto. Se espera declarar en ésta sección factores que aportan valor al cliente (aquellas características que estarán contempladas en el proyecto) y mencionar aquellas características que no serán tenidas en cuenta.
  * **Necesidades de Infraestructura** Información sobre los ambientes de prueba y desarrollo, detalle sus requisitos y los costos.

---

##### Sobre la Propuesta

* **Propuesta Técnica** Se describe un análisis inicial de las especificaciones técnicas y comentarios referidos al proyecto. Por ejemplo "Preparación de ambiente de desarrollo","Implementación de secciones", "Carga de Usuarios", "Despliegue: Puesta en ambiente de pruebas y producción" con las tareas necesarías en cada caso.
* **Propuesta General**
  * **Entrega de Desarrollo** Especificación de los Hitos correspondientes con su alcance respecto de la Propuesta Técnica detallando tiempos y garantía.
  * **Reuniones de planificación y seguimiento**
  * **Informes**

---

#### Output

El output debe ser un detalle técnico para que comercial pueda generar la propuesta y realizar una propuesta Económica incluida dentro del mismo documento.

* **Propuesta económica** Dentro de esta sección se especifican los plazos de inicio y entrega, Modalidad de pago y Condiciones generales para la propuesta económica.

---

#### Inicio del proyecto y Desarrollo

Entendemos por **proyecto activo** aquellos en los que sabemos que la estimación y propuesta fueron aprobadas por el cliente.

##### Comunicación con Cliente

## Desde el equipo nos encargamos de la comunicación con el cliente a nivel producción. Por eso, definimos canales y mantenemos cierta lógica en este sentido.

##### Comunicación con Cliente

*Es IMPORTANTE hacer un perfil de las personas que tenemos de clientes:*

* Cómo es su estructura, quien es la persona responsable de dialogar con nosotrxs en proyecto, quién toma las decisiones.
* Que prefieren respecto a sus ambientes, permisos y accesos al board. Accesos a repositorios.
* Qué necesidad tiene de status (cada cuánto tiempo, que info, es una investigación que necesita detalle o simple producto)
* Es un cliente que necesita info más técnica o que la información le llegue de forma más accesible para entender cómo funciona el proyecto.

> Es una buena práctica acostumbrar a recibir un status de visibilidad del proyecto con temas bloqueantes, requerimientos de forma semanal o según lo necesitemos.

---

###### Comunicación con Cliente

**Definimos Canales de Comunicación**

Al tener varios canales, es importante saber por cual vamos a comentar lo crítico o lo que podemos ver como documentación clave de proyecto.

* El email es el más formal, no así la mensajeria.
* En caso de comunicarnos via mensajeria, dejar en claro los horarios y enviar emails si se habló algo clave que modifica el proyecto.
* Se puede intercambiar consultas via repositorio con accesos especificos
* Tener calls también acerca la producción al cliente., lo mismo que las reuniones. Es una buena práctica hacer una minuta de lo que se habló enviada por email.

---

###### Comunicación con Cliente

* Podemos usar titulos claros o códigos para seguir un proyecto, hashtags.
* Es importante asegurar la comunicación:
  1. che, que onda, realmente vamos por acá?
  2. confirmemos que se entiende porque lo pedimos para esto
  3. porque es importante puntos criticos (alfabetización digital al cliente)
  4. solucion posible: te resuelvo la vida
  5. igual lo que el cliente mande
* Comunicación con 3eras partes: Hay veces que hay terceras partes involucradas.

  En estos casos es clave analizar si es necesario copiarlas en algo puntual, o no, si el cliente está en copia, y como manejar el tema de solicitudes que nos traban.

  También dejar en claro que son nuestras responsabilidades y cuales las de estas terceras partes.

---

### Herramientas de Gestión: Plantillas de Informe de Horas, Status, Horas Productivas.

En éste caso el proyecto que en un principio se copió a **propuestas** ahora lo tenemos que mover a **proyectos**.

> Toda herramienta que se utilice para un proyecto **activo** debería guardarse en la carpeta de ese proyecto, dentro de **proyectos**.

---

### Informe de Horas

##### Objetivos y Uso

* Es necesario especificar el Nombre del proyecto, el equipo y las tareas realizadas junto con las horas dedicadas.
* Se utiliza para reportar al cliente las horas dedicadas de x cant de semanas.
* Lo utilizamos de ser necesario, probablemente a pedido del Cliente.

#### Output

Total de horas según cada tarea ordenadas semanalamente.

---

### Status de Proyecto

##### Objetivos y Uso

* Se deben especificar Nombre del Proyecto, Etapa (según propuesta) y un desglose de los avances realizados en las fechas que abarca el Status.
* En el bloque amarillo vamos a poner alertas y avisos que aún no bloquean el proyecto.
* En el bloque rojo vamos a remarcar los bloqueantes.
* Lo utilizamos a pedido del Cliente o según la Propuesta.

#### Output

Un status del proyecto para enviar al Cliente.

---

#### Finalización del proyecto

Al cerrar el proyecto hacemos la Retrospectiva.

La misma tiene como finalidad abrir un debate interno, entre lxs miembrxs involucradxs en el proyecto, basado en las diversas perspectivas.

La idea es resolver conflictos para próximas etapas, prevenir riesgos mejorar continuamente o simplemente detectar maduración en los procesos.

> Input para la mejora contínua de los procesos internos: estimación, trabajo grupal, decisión tecnológica, entre otros aspectos.

---

### Retrospectiva de proyecto

##### Objetivos y Uso

* Una vez subida al repositorio, todxs tendrán acceso a ella. Todavía no está muy claro cuál es la mejor manera entre éstas dos posibilidades:
* Cada miembro del equipo debe contar con una copia local y completar cada campo con sus calificaciones, según corresponda. Si nuestro rol o criterio no encaja en alguna fila (comercial/técnico/financiero, etc.) no hace falta completarla.
* La otra es que todxs usen el mismo documento compartido en Nubecita y se vayan contando las calificaciones ahí mismo.
* Las calificaciones se realizan de manera individual. Al finalizar, se comparan las distintas perspectivas.

*Si eligen que cada miembrx complete la plantilla tranqui de manera individual, al arrancar con la retrospectiva grupal no queda otra que cargar cada calificación en la plantilla original en Nubecita para que quede registro.*

---

### En resumen

##### - Plantillas de Gestión de Proyectos en todas sus etapas: Estimación, Propuesta, Status, Retro

##### - Comunicación con el Cliente

##### - Flujo con Comercial

##### - Gestión en Nubecita

Dudas? Consultas? Sugerencias?

---

<h2 style="padding:1em;"> <3 Eso es todo amigxs <3 </h2>

---
