# Circulo de Estudio

> Belén nos compartío este texto en donde la Univesidad Nacional de San Martín trata de encontrar un formato de formación alternativa para formalizarlo y certificarlo a nivel universitario.

[Texto original](http://www.unsam.edu.ar/secretarias/extension/PDF/Circulos_de_estudio.pdf)

## ¿Qué es?

> Autoformación y Co-formación en grupos reducidos

## Principios básico

- Recuperación y articulación de los saberes del orientador y de los participantes
- Respeto de todas las opiniones
- Reivindicación del derecho a equivocarse
- Libertad de expresión y circulación de la palabra
- Fomento de la franqueza y al debate de ideas
- Incitación a los participantes a tener confianza en si-mismo

## ¿Cómo?

- Grupos voluntarios de 5 a 20 personas
- Entre 3 a 6 encuentros (2 o 3 horas)
- Un temática
- Una persona que es la "orientadora" (rol: Asistir a que sea un diálogo animado y enfocado)
- Una persona que es la "secretaria" que apoya organización y a la información interna
- Al fin de cada encuentro se da material (el material es propuesto por la "orientadora", pero los demás participantes pueden proponer o crear material)
- Definición de agenda colectivamente
