# Baruch Spinoza

> Su pensamiento es un materialismo que permite la elaboracion de un sistema liberador, tolerante y anti religioso.

## ¿Quién era?

- Vivío durante el Siglo XVII
- Era Pulidor de lentes
- Su vida fue marcada por la pobreza y la soledad, por opción deliberada.
- Su libro más importante [Ética](https://es.wikipedia.org/wiki/%C3%89tica_(Spinoza)), lo escribío durante 15 años y fue publicado luego de su muerte.

## Objetivo

> Se propone la liberación del hombre que coincide con la conquista de la felicidad.
> La felicidad, la suma libertad, es el conocimiento de todo o sea de la naturaleza.

## Estuctura

Su método 3 géneros del conocimiento:

- Imaginación
- Razón
- Intuición

### Potencia

- El *individuo* se define por cierto *tiempo de movimiento y de reposo*
- La posibilidad de pasar del reposo al movimiento es *potencia*
- Potencia = Conatus: Prepararse, disponerse, alistarse a hacer algo (emprender una acción)

### Naturaleza

Las partes de la naturaleza son finitas, pero conforman una potencia infinita.
La naturaleza es, toda entera, un solo individuo.

La potencia es pura afirmación (excluye la extinción).

El deseo no es lo que falta sino una potencia que le permite concervarse, ser, actuar y padecer.
La duración de la vida no es infinita, ni finita, *es indefinida*: la certeza de la muerte nos quita la infinitud, pero la ignorancia de sus circunstancias nos concede la indefinición.

Hay una sustancia divina infinita: dios o la naturaleza

## Libertad

Es monista, no cree en un dualismo, cree en la unicida del cuerpo/alma.

Una vez que el hombre acepta y entiende las leyes que rigen el mundo puede actuar libre y racionalmente.

Pero es posible que exista la libertad humana si todo esta sometido a una regulación permanente: no.

No existe la libertad humana.

El hombre es una paradoja: un esclavo porque se cree libre y esta dominado y condicionado por la necesidad

La libertad humana aparece cuando el ser humano acepta que todo esta determinado. La libertad no depende de la voluntad sino del entendimiento.

El hombre se libera mediante el conocimiento intelectual.

## Estado

Reivindica una democracia amplia.

El estado es proteger a sus integrantes de la injusticia a partir de los dictados de la razón.

La ley, el derecho, la moralidad existen por el estado.

Los particulares acuerdan limitarse deacuerdo a estas convenciones sociales.

## Conclusión

La auténtica comprensión de la realidad es poder captar la unidad de este todo, a partir de lo que sentimos y experimentamos.

