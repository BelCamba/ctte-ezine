# No cosas - Byun-chul-han

Frenesi de comunicación e información lo que hace que las cosas desaparescan
El reino de información que se hace pasar por libertad
En lugar de guardar recuerdos almacenamos inmensas cantidades de datos
Nos volvemos ciegos para las cosas silenciosas, discretas, incluidas las habituales, las menudas o las comunes, que so nos estimulan, pero nos anclan en el ser.

Estabilizar la vida humana
No es posible detenerse en la información. Tiene un intervalo de actualidad muy reducido. Vive del estimulo que es la sorpresa. La fugacidad desestabiliza la vida.

El telos del orden digital es la superación de los cuidados
La existencia es cuidarse. La inteligencia artificial se halla ahora en proceso de librar de cuidados a la existencia humana, optimizando la vida y velando el futuro como fuente de preocupación (sobreponiendose a la contigencia del futuro). El futuro calculable como prensete optimizado no nos causa ninguna preocupacion.

Las informaciones son aditivas, no narrativas. Pueden contarse, pero no narrarse.
Los largos espacios de tiempo que ocupa la continuidad narrativa distinguen a la historia y la memoria. Solo las narraciones crean significado y contexto. El orden digital, es decir, númerico, carece de ghistoria y de memoria, en consecuencia, fragmentan la vida.

En el mundo controlado por los algoritmos, el ser humano va perdiendo su capacidad de obrar por si mismo, su autonomía. Se ve frente a un mundo que no es el suyo, que escapa a su comprensión. Se adapta a decisiones algorítmicas que no puede comprender.

No solo las cosas del mundo, sino también la verdadm estabilizan la vida humana.

Hoy las prácticas que requieren un tiempo considerable están en trance de desaparecer. También la verdad requiere mucho tiempo. No tenemos tiempo para verdad. La confianza, las promesas y la responsabilidad tambien son practicas que requieren tiempo. Todo lo que estabiliza la vida humana requiere tiempo. La felicidad, el compromiso y las obligaciones tambien necesitan mucho tiempo.

Para estabilizar la vida es necesaria otra politica del tiempo.

La información excluye la observacion larga y lenta. Es imposible detenerse en la información.
Hoy corremos detras de la información sin alcanzar un saber. Tomamos nota de todo sin obtener un conocimiento. Viajamos a todas partes sin adquirir una experiencia. Nos comunicamos continuamente sin participar en una comunidad. Almacenamos grandes cantidades de datos sin recuerdos que conservar. Acumulamos amigos y seguidores sin encontrarnos con el otros. La información crea asi una forma de vida sin permanencia y duración.

El ser humano del futuro, sin interes por las cosas, no sera un trabajador sino un jugador.
Eligira en lugar de actuar.
Tampoco querra poseer nada, sino experimentar y disfrutar (juegue y disfrute, que no tenga preocupaciones).
El juego no interviene en la realidad.

Experimentar significa consumir informacion. Hoy queremos experimentar mas que poseer, ser mas que tener. Yo soy tanto más cuanto más experimento.
Las cosas queridas suponen un vínculo libidinal intenso. En la actualidad no queremos atarnos a las cosas ni a las personas. Los vinculos son inoportunos. Restan posibilidades a la experiencia, es decir, a la libertad en el sentido consumista.
El mundo de la información no esta hecho para la posesion, puesto en el rige el acceso.
Ya la continua necesidad de movilidad dificulta la identificación con las cosas y lugares.
