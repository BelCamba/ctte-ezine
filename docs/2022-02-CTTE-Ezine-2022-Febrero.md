---
title: Febrero 2022
---

# Cambá CTTE Ezine Febrero 2022

![posibilidad](@img/posibilidad.png)

```
El peor laberinto no es esa forma intrincada que puede atraparnos para siempre,
sino una línea recta única y precisa.
```

Aquí frase de [Jorge Luis Borges](https://es.wikipedia.org/wiki/Jorge_Luis_Borges) leída en el mirador del [Laberinto de Borges](https://laberintodeborges.com/) en Finca Los Alamos en San Rafael, Mendoza.


## Herramientas de software
- [Zoneminder](https://github.com/ZoneMinder/ZoneMinder/) - Software para Circuito cerrado de TV con soporte para camaras analogas, IPs y USB.
- [Formik](https://github.com/jaredpalmer/formik) - Construcción de formularios en React sin llorar.
- [Briar](https://briarproject.org/) - Mensajería segura en todos lados
- [Open Visual Trace](https://visualtraceroute.net/) - Herramienta de visualización para redes.

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Imowi](https://essapp.coop/noticias/cooperativas-anunciaron-el-lanzamiento-de-imowi-en-enero) - Puesta en marcha Servicio de Operación móvil cooperativo.
- [Una década en perspectiva de Dyne.org](https://medium.com/think-do-tank/dyne-org-a-decade-in-perspective-55685c538dfb)
- [Criterio de evaluación de repo de gnu.org](https://www.gnu.org/software/repo-criteria-evaluation.html)
- Conectar Igualdad y Sistema opertaivo
    - Recurso de reconsideración - [Web](https://www.vialibre.org.ar/presentamos-un-recurso-de-reconsideracion-conectar-igualdad/), [PDF](https://www.vialibre.org.ar/wp-content/uploads/2022/01/Recurso-Reconsid-CONECTAR-IGUALDAD-Publico.pdf)
    - Debate sobre Windows y/o Huayra - [La Nación](https://www.lanacion.com.ar/tecnologia/windows-o-huayra-linux-vuelve-el-debate-por-el-sistema-operativo-que-deben-usar-las-notebooks-de-nid19012022/), [Clarín](https://www.clarin.com/tecnologia/-microsoft-asunto-separado-conectar-igualdad-reaviva-polemica-equipos-venir-windows_0_ylJUfmWMlD.html)
- [Resultados de encuesta a desarrolladores de django](https://lp.jetbrains.com/django-developer-survey-2021-486/)
- [Nuevo Release de Django 4.0.1](https://docs.djangoproject.com/en/4.0/releases/4.0.1/)


## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Manifiesto del Software Libren la Educación Superior](https://mega.nz/file/quAnRQYa#_XRR-n1cbyz9_P_wQTFkWTsL3RC3ITuPjEK6Dxw57KY) de Carlos Brys
- [Explicación de Claudio Alvarez Teran sobre No Cosas - Byung Chul Han](https://www.youtube.com/watch?v=JlyNCRWGYBY)
- [¿Cómo imponer un límte absoluto al capitalismo?](https://www.youtube.com/watch?v=bh3pVbYgN0I)
- [Santi siri- Buenos aires es la capital cripto del mundo por excelencia](https://www.youtube.com/watch?v=t0GoRUvNf70)

### Tecnología

- Usando [TestDisk](https://www.cgsecurity.org/wiki/TestDisk) como herramienta de recuperación. [Tutorial](https://www.tecmint.com/install-testdisk-data-recovery-tool-in-linux/)
- [Captura](https://56k.es/fanta/rtl2838-003-capturar-senal-mandos-de-garaje-en-linux/) y [emisión](https://circuitdigest.com/microcontroller-projects/raspberry-pi-fm-transmitter) de señales fm (ej: alarmas de autos)
- [La transformada de Coseno discrecta](https://es.wikipedia.org/wiki/Transformada_de_coseno_discreta)
    - [Video con Histotria de Nasir Ahmed](https://www.youtube.com/watch?v=nNmREQIF4Ik)
    - [¿Como funciona la compresión jpeg](https://www.image-engineering.de/library/technotes/745-how-does-the-jpeg-compression-work)
- Programador decide corromper sus librerias npm **colors** y **faker**, rompiendo miles de aplicación. Importate alegato a favor de la libertad.
    - [Commit](https://github.com/Marak/colors.js/commit/074a0f8ed0c31c35d13d28632bd8a049ff136fb6?diff=unified)
    - [Nota bleepingcomputer](https://www.bleepingcomputer.com/news/security/dev-corrupts-npm-libs-colors-and-faker-breaking-thousands-of-apps/)
    - [Nota xataka](https://www.xataka.com/aplicaciones/desarrollador-dos-librerias-open-source-populares-nodejs-ha-decidido-corromperlas-afectando-a-millones-usuarios/)



### Otras

- Texto [Del nacimiento a la consolidación. Modalidades de conformación de cooperativas de software y servicios informáticos en el AMBA - García, Gonzalo Ezequiel](https://www.idelcoop.org.ar/revista/235/del-nacimiento-consolidacion-modalidades-conformacion-cooperativas-software-y-servicios)
- [Cooperativas en la Argentina 2020 - Cooperativas en la Argentina 2020, Acerca de las cooperativas de trabajo en el área de la tecnología, la innovación y el conocimiento](https://rephip.unr.edu.ar/xmlui/handle/2133/20912) - Pablo A. Vannini pág 281.
- [Revista colmena de cooperativa de trabajo Redjar](./assets/colmena_redjar.pdf)
- [Red Comunitaria de Internet ComunidadRioSalado - Salta](https://www.youtube.com/watch?v=gPz0Rd1rSa8)
