# Entrevista a [Rita Segato](https://es.wikipedia.org/wiki/Rita_Segato) por Mario Santucho - Periscopio 13/08/20

> [Video](https://www.youtube.com/watch?v=32dWeXEcn38)

## Lectura sobre la Pandemia

- El capital necesita de la cosificación  y para eso hay que ocultar la muerte
- El deseo se colpasa cuando la muerte esta cerca
- La pandemia pone a la vista la capacidad de apegarse a otras ideas, ejemplo los vinculos
- Esta situación es disfuncional al proyecto del capital
- La episteme omnipotente (fin de la historia, mundo controlado) ve por primera vez una pandemia.
- Primera vez que aplica sobre el **Sujeto publico universal** (padre, varón, blanco, letrado, propietario) de nuestra civilización.
- Revalorización de la red de cuidados o de los vinculos

## Error de las izquierdas

- No es posible una resolución de los antagonismos
- La pulsión, piel politica, deseo se enmarca sobre: **Insatisfechos vs Conformista** (en todas las sociedades humanas).
- La tensión en inherente a la naturaleza humana, encontrar la belleza en el discurso es la diferencia, lo que tuerce la historia.

## Decadencia del discurso moral

- El campo critico se ha valido de un discurso moral durante mucho tiempo y ha perdido impacto.
- ¿De que nos valemos entonces?

## Fuerzas existen pero no están presentes

- Existencia de paraestado, paracomuniación, etc
- Hay un Universo propio que controla la vida de mucha gente con volumenes económicos ilicitos que son mayores a los de la economía licita.
- [TaxJustice](https://www.taxjustice.net/)



