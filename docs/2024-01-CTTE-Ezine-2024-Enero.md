---
title: Enero 2024
---

# Cambá CTTE Ezine Enero 2024

<div style="text-align: center;">

![Constitucion](@img/enero2024.jpg)

</div>

```
Siento que hay fronteras
que jamás voy a cruzar
y que hay maneras
de vivir innegociables.
Píldoras doradas
que no quiero tragar más,
semillas buenas
que hoy son plantas detestables.
Sé que quiero estar
en la mitad del mundo
que se juegue el cuero
por el otro medio mundo.
Sé que va a doler
con un dolor de humanidad
cuando separe
a la doctrina de mi rumbo.
La verdad de verdad
es verdad aunque la grite un mentiroso
y tiene cada mitad
atrapada por un cíclope orgulloso.
Lejos de los miedos
y los bronces y los mármoles
y de cualquier blasón
de los cuarteles.
Fiel, pero sin vendas
en los ojos,
lejos de los viejos lobos
que hoy estrenan nuevas pieles.
Ya no sé qué dice
aquel que dice hablar por mí
y no le compro
al mercader de las ideas
las espadas que trafica
bajo el manto del poder
que enfrenta al pobre
contra el pobre en esta arena.
La verdad no es verdad
si es la hija de verdades absolutas,
vuela en la oscuridad
como pájaro perdido en una gruta.

```

Letra del tema [Manifiesto](https://www.youtube.com/watch?v=lTIdeUChkTk) de [Tabaré Cardozo](https://es.wikipedia.org/wiki/Tabar%C3%A9_Cardozo) y [Juan Carlos Baglietto](https://es.wikipedia.org/wiki/Juan_Carlos_Baglietto)


## Cooperativas/Economía Popular
- [Cambá: ¿Cómo nos distribuimos el dinero?](https://blog.camba.coop/needs-based-money-distribution-scheme/)
- [Proyecto Colmena: Cambá organiza encuentro en las Chacras sobre radios comunitarias](https://nubecita.camba.coop/s/nCNPt3EWdeD8ye8)
- [Estrategia de negocios anti Milei que usa esta empresa](https://www.cronista.com/infotechnology/it-business/La-estrategia-de-negocios-anti-Milei-que-usa-esta-empresa-20190208-0007.html)
- [Tecnología colaborativa y cooperativas](https://ar.radiocut.fm/audiocut/tecnologia-colaborativa-y-cooperativas/)
- [Cooperativismo: Derecho y obligaciones de las asociadas](https://www.youtube.com/watch?v=1vbVbsIKy-o)

## Herramientas de software
- [Telegram NearBy Map](https://github.com/tejado/telegram-nearby-map): Descubrí donde están las usuarias de telegram cercanas
- [Wall of Flippers](https://github.com/K3YOMI/Wall-of-Flippers): Encontrá los dispositivos Flipper Zero
- [Api Detector](https://github.com/brinhosa/apidetector): Exponé endpoints de openapi
- [Compiler Explorer](https://github.com/compiler-explorer/compiler-explorer): Corré compiladores en el navegador
- [Cosmopolitan](https://justine.lol/cosmopolitan/index.html): Compila una vez y correlo donde quieras.


## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Resultados de encuesta de mantenedores de software libre](https://explore.tidelift.com/open-source-software-intelligence/2023-open-source-maintainer-survey)
- [Linus Torvalds sobre el estado de linux y como influye la IA en su futuro](https://www.zdnet.com/article/kernel-security-now-linuxs-unique-method-for-securing-code/)
- [Lanzamiento de Django 5](https://www.djangoproject.com/weblog/2023/dec/04/django-50-released/) - [Video](https://www.youtube.com/watch?v=lPl5Q5gv9G8)
- [OWASP: Consejos de seguridad de Django](https://cheatsheetseries.owasp.org/cheatsheets/Django_Security_Cheat_Sheet.html):

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Los realmente woke son los antiwokes](https://www.youtube.com/watch?v=gJJKuDl7NKw)
- [Mirada sobre Argentina](https://www.youtube.com/watch?v=Vk45cupgvRY)
- [¿Por qué tu TRABAJO destruye tu SALUD MENTAL?](https://www.youtube.com/watch?v=Z_uOhoviHPU)
- [Estoy cansado, Jefe](https://www.revistaanfibia.com/burn-out-estoy-cansado-jefe/)
- [Propuestas económicas del anarquismo clásico: COMUNISMO LIBERTARIO](https://www.youtube.com/watch?v=pf5HkhR7FgE9)

### Tecnología
- **Django instant messaging:** [Tutorial #1](https://www.photondesigner.com/articles/instant-messenger) - [Tutorial #2](https://centrifugal.dev/docs/tutorial/intro)
- [Automatizar para innovar](https://osiux.com/2023-04-22-flisol-caba-automatizar-para-innovar.html)
- [Herramientas para hacer Tracing](https://thume.ca/2023/12/02/tracing-methods/)
- [αcτµαlly pδrταblε εxεcµταblε](https://justine.lol/ape.html)



### LLMs
- [Llamafile]( https://hacks.mozilla.org/2023/11/introducing-llamafile/): Correr y distribuir LLMs con un solo archivo [Código fuente](https://github.com/Mozilla-Ocho/llamafile)
- [Self Operating computer](https://github.com/OthersideAI/self-operating-computer): Framework para permitir con modelos multimodales operar una computadora.
- [vimGPT](https://github.com/ishan0102/vimGPT): Navega la web con GPT-4V y Vimium
- [Modelo Mixtral of experts](https://mistral.ai/news/mixtral-of-experts/) - [Explicación](https://huggingface.co/blog/moe)
- [LLM Contamination](https://www.youtube.com/watch?v=dxH1GFCfdF0)
