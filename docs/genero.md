# Recursero Género

## [1. Código de conducta Facttic](#1)
## [2. Economía Feminista y la relación entre el trabajo reproductivo y productivo](#2)
## [3. Ecofeminismos](#3)
## [4. Tareas de Cuidado](#4)
## [5. Género y finanzas](#5)
## [6. Feminismo y tecnología](#6)
## [7. Género y políticas del conocimiento](#7)
## [8. Género y ciencia ficción](#8)
## [9 Conversación con Julio Maiza](#9)
## [10 Comunicación no Violenta](#10)
## [11 Sara Ahmed y el Giro afectivo](#11) 
## [12 Filosofía Queer](#12)
## [13 Sesgo de género en IA](#13)


-------------
<a id="1"></a>
## 1. Código de conducta Facttic 

> Lectura y reflexiones del Código de conducta de la Federacion Argentina de Cooperativas de Trabajo de Tecnología Innovación y Conocimiento

### Material bibliográfico de referencia

[código de conducta de FACTTIC](https://nubecita.camba.coop/s/mgk8LMFp6oy9TyW)

### Propuestas de trabajo

* Repasar puntos importantes.
* Hacer énfasis en los que nos llamen la atención y que nos atraviesan dentro de nuestras organizaciones.

--------------------
<a id="2"></a>

## 2. Economía Feminista y la relación entre el trabajo reproductivo y productivo 

> Reflexionar sobre Economía Feminista y la relación entre el trabajo reproductivo y productivo. Identificar cuáles son los límites, las tensiones y las armonías hacia dentro de nuestras organizaciones. 


### Material bibliográfico de referencia

La Economía feminista tiene como objeto de análisis el conjunto de vínculos que establacen las personas para organizar sus relaciones sociales ligadas con la satisfacción de sus necesidades y la reproducción material de la vida, haciendo hincapié en que éstas son también relaciones afectivas, de apoyo mutuo y de colaboración. 
Sostiene la importancia del reconocimiento de los ciclos vitales, y de la necesidad de cuidados y atenciones que tenemos unos de otros para desarrollar una vida de calidad.

Por otro lado, la Economía Social y Solidaria, tiene sus orígenes en los movimientos sociales del siglo XIX (corrientes ideológicas como socialista, social-cristiana, anarquista) que pretendían superar las relaciones de opresión generadas por las estructuras capitalistas.

Ambas ponen a las personas y sus condiciones de vida en el centro del análisis y visibilizan aquellos valores colaborativos intrínsecos en las relaciones entre las personas a la hora de llevar a cabo los trabajos
socialmente necesarios para la reproducción social

El siguiente es un manifiesto difundido por el grupo de trabajo EkoSolFem:

"¿Cómo construimos entre todos vidas más habitables?

El capitalismo heteropatriarcal nos presenta un mundo compuesto por dos
esferas dicotómicas -productiva y reproductiva-, aparentemente
armónicas, que, sin embargo, está plagado de tensiones.

Cuando nos dicen que sólo el ámbito productivo genera riqueza y aumenta
el PIB, nosotras respondemos que los hogares son productores de bienes y
servicios esenciales para la vida y que han sido sistemáticamente
olvidados en los análisis económicos, con todos los efectos perversos
que esa constante invisiblización tiene para la vida de las mujeres.
Cargas ingentes de cuidados escasamente repartidas que nos enferman y
empobrecen y que se ven como una limitación en un mercado laboral
excluyente que nos relega a sectores feminizados muy mal remunerados.

Cuando nos cuentan que el crecimiento económico revierte en toda la
sociedad y que la mejor manera de crecer es producir más, nosotras les
recordamos, por un lado, que el funcionamiento del libre mercado no
responde a las necesidades de la mayoría de las personas ni de los
pueblos; y, por otro, que la idea de que el crecimiento es deseable e,
incluso, posible en un mundo finito, no tiene ningún sentido.

Cuando insisten en que el mercado es autónomo, que se autoregula y que
las personas son libres para elegir y moverse en él, nosotras decimos
que se ha dejado en manos de las mujeres la responsabilidad de la
subsistencia y el cuidado de la vida, y nos preguntamos quién cuidaría
de la vida si mujeres y hombres nos comportáramos con absoluta libertad,
dando prioridad absoluta a la participación en el mercado capitalista.

Y cuando nos dicen que la Vida no puede estar en el centro de la
economía, nosotras respondemos que la economía es la gestión de la Vida
y que es precisamente la sostenibilidad de esta vida lo que debería
estar en el centro de cualquier análisis o toma de decisión en el ámbito
económico.

Tenemos claro que el cuestionamiento y la confrontación con el
patriarcado y el capitalismo son elementos clave para la construcción de
alternativas. Y, también, que para construir una economía solidaria y
feminista debemos transformar no sólo nuestras formas de hacer, sino
también a nosotros y nosotras mismas, y a nuestras organizaciones."


[2] texto completo
https://camba.gitlab.io/ctte-ezine/CTTE-texto-flor-economia-feminista.html


### Propuestas de trabajo

Guías para los grupos:
- ¿Estamos cuidandonos para poder ser personas felices, sanas,
equilibradas y productivas?
- ¿Estoy/Estamos haciendo un buen trabajo ayudando a otres?
- ¿Estoy/Estamos creando dinamicas de equipo y oportunidades para ayudar
a los demás a hacer un mejor trabajo?
- Estamos pendientes del cuidado del entorno, de nuestra huella ecológica.
- ¿Hay límites con respecto al trabajo reproductivo tanto
organizacionalmente como individual? Si lo hay ¿Cuales son? ¿Como
hacemos para que todes estemos dentro de ellos?
- Podemos identificar desigualdades en nuestra organización. ¿Donde?
¿Acciones concretas para eliminarlas?

------------------------------
<a id="3"></a>

## 3. Ecofeminismos 

> Retomamos la temática sobre el cuidado de la vida y quisimos ampliarla y conocer sobre ecofeminismos.

### Material bibliográfico de referencia 

* [Feminismos del sur y ecofeminismos](https://nuso.org/articulo/feminismos-del-sur-y-ecofeminismo/) 

### Propuestas de trabajo

* Luego de leer el texto, habilitamos el debate colectivo.

-------------------------------
<a id="4"></a>

## 4. Tareas de Cuidado

> Repasamos material sobre las tareas relacionadas con la reproducción, el bienestar y el sostenimiento de la vida.

### Material bibliográfico de referencia 

La desigualdad en la distribucion de los trabajos de cuidado incide en limitar la participacion economica y disminuye la proteccion social de las mujeres que mayoritariamente los llevan a cabo.
Impacta directamente en su autonomia y favorece situaciones de violencia.

Segun la OIT (Organizacion Internacional del Trabajo), existen indicadores a nivel mundial donde se visibilizan los tiempos que dedican los diferentes individuos a las tareas de cuidado, donde:

* Mujeres dedican 3 horas diarias de trabajo remunerado y casi 5 horas de trabajo no remunerado
* Varones casi 6 horas de trabajo remunerado y 1.5 horas de trabajo no remunerado

En Argentina, segun los ultimos datos del INDEC (2014), las **mujeres dedican casi 6 horas diarias** al trabajo de cuidados no remunerados y los **varones solo 2 horas**. Esta distribucion impacta en la vida de las mujeres disminuyendo la cantidad de tiempo para dedicar a su desarrollo laboral o personal.

Segun el INDEC, para el último trimestre del 2021, la tasa de actividad de las mujeres es del 50% mientras que la de los varoes es del 70%, con una tasa de empleo del 46% para las mujeres y del 64% para los varones.

Segun datos del Ministerio de las Mujeres, Géneros y Diversidad, en 2021, en **hogares con presencia de niñes menores de 4 años, las mujeres participan en el mercado laboral en un 60%, mientras que los varones un 85%**

De estos datos, se evidencia que la interrupcion de las trayectorias laborales debido a los periodos de tiempo dedcidados al cuidado inciden en la segregacion vertical o "techo de cristal", haciendo que los varones accedan a posiciones de decision en los puestos de trabajo.

Como último dato, **1 de cada 10 mujeres cumple con los 30 años de aporte** para poder jubilarse y **el 74% debe recurrir a regimenes de moratoria** para poder percibirla, segun el ANSES.

### Cuidados de Adultos Mayores

Según la Encuesta Nacional de Calidad de Vida de los adultos Mayores (INDEC 2012) el 12% de mujeres mayores de 60 años presentan dependencia de la actividades básicas de la vida diaria, mientras que los varones solo el 6%. En los mayores de 75 años, 25% las mujeres y 13% los varones.

Apenas un 20% de las adultas y adultos mayores logra acceder a un servicio externo de cuidado.

## [Proyecto de Ley "Cuidar en Igualdad"](https://www.argentina.gob.ar/sites/default/files/2022/06/sinca0008-pe-2022.pdf)

Reconoce los cuidados como una **necesidad, un trabajo y un derecho para un desarrollo con igualdad**

Establece la creación del **Sistema Integral de Cuidados de Argentina con perspectiva de género (SINCA)** y la modificación del régimen de licencias en los **sectores público y privado**.

Creara **un registro nacional de trabajadoras y trabajadores del cuidado remunerado** para facilitar la instrumentacion de politicas de cuidado

Impulsara políticas de **compensación para personas que realicen tareas de cuidado no remunerado** (ejemplo, cuidado de hijos).


### Modificaciones a la Ley de Contrato de Trabajo

* Los periodos de licencia seran cubiertos por seguridad social y no por los empleadores.

* Licencia extendida para **personal gestante, de 90 dias a 126**.
    * 30/45 dias previos al parto
    * 81/96 dias post parto
    * **incluye a otras identidades de genero a la licencia**

* Licencia extendida para **personas no gestantes, de 2 a 90 dias**.
    * 15 dias a partir del nacimiento
    * los dias restantes pueden utilizarse dentro de los 6 meses posteriores.
    * **no distingue generos**

* Licencia para **futuros adoptantes, de 2 a 12 dias** para llevar a cabo el proceso.

* Licencia **por adopcion, de 90 dias** una vez notificados de la resolucion judicial.
    * 15 dias a partir de la resolucion
    * los dias restantes a utilizarse dentro de los 6 meses posteriores.

* Licencia de **acompañamiento, de 2 a 6 dias** para conyuge o conviviente con tratamiento de tecnicas de reproduccion medicamente asistida.
    * extendida de 3 a 10 dias si tuviesen menores a cargo.

* Extension de la licencia por maternidad en caso de nacimientos o adopciones multiples, con discapacidad y nacimiento prematuro o con enfermedades cronicas.
    * 180 dias en caso de nacimiento o adopcion de hije con discapacidad o enfermedad cronica.
    * 30 dias a partir del segundo hije en los otros casos.

* Permite a las personas no gestantes y adoptantes optar por un **periodo de excedencia**.

* No se **asume la renuncia** del personal gestante, no gestante y adoptante en los casos de:
    * No reincorporarse a su empleo luego de vencidos los plazos de licencia.
    * No comunicarse con su empleador dentro de las 48 horas anteriores a la finalizacion de las mismas.

### Licencias especiales

* Crea una licencia por violencia de genero, **de hasta un maximo de 20 dias por año**.

* Da entidad a una licencia especial por cuidado por enfermedad de persona a cargo, conviviente o conyugue, **que pasa de 2 dias a 20 dias por año**.

* Amplia licencias por fallecimiento de conyugue o conviviente (este ultimo no estaba contemplado anteriormente) de 3 a 5 dias y de hermano/a de 1 a 3 dias.

### Cuidado de adultos mayores

Si bien no hace foco particular en como instrumentar licencias, si busca promover la visibilizacion y reconocimiento de las tareas de cuidado como impulsador de la economia generando nuevos puestos de trabajo formales.


* [Proyecto de ley Cuidar en Igualdad](https://www.argentina.gob.ar/generos/proyecto-de-ley-cuidar-en-igualdad)
* [Creación del SISTEMA INTEGRAL DE POLÍTICAS DE CUIDADOS DE ARGENTINA(SINCA)](https://https://www.argentina.gob.ar/sites/default/files/2022/06/sinca0008-pe-2022.pdf)

### Propuestas de trabajo

* Lectura y debate del material propuesto

-------------------------------
<a id="5"></a>

## 5. Género y finanzas 

> Reflexiones y material sobre Género y finanzas 

### Material bibliográfico de referencia 

## If Lehman brothers were Lehman sisters

> Irene van Staveren | TEDxEde

En el siguiente vídeo se profundiza en la intrincada relación entre género y finanzas, arrojando luz sobre varios conceptos significativos: la brecha de género, la inclusión financiera y la educación financiera. La oradora explora las consecuencias de la crisis financiera de 2008, destacando las consecuencias de la cultura bancaria hipermasculina. Basándose en investigaciones, subraya las diferencias de comportamiento entre hombres y mujeres, particularmente en términos de asunción de riesgos, moralidad y liderazgo. Ella sostiene que estas diferencias no se deben a disparidades de género innatas, sino que están determinadas por estereotipos sociales y comportamientos aprendidos.

<p class="video-container">
    <iframe
        width="420" height="315" class="video"
        src="https://www.youtube.com/embed/pcl0kEeN4mk">
    </iframe>
</p>



<style>
    .video-container {
        display: flex;
        justify-content: center;
    }
    .video {
        display: flex;
        justify-content: center;
        margin: 3rem;
        border-radius: 10px;
        box-shadow: 4px 4px 4px 2px #666;
        transition: all 0.2s ease-out
    }

    .video:hover {
        transform: scale(1.05);
        box-shadow: 12px 12px 12px 2px #666;
        transition: all 0.2s ease-out
    }
    
    #doc {
        font-family: 'Montserrat'
    }
</style>

A través de sus análisis enfatiza la necesidad de una cultura bancaria más equilibrada e inclusiva, donde se recompensen tanto los comportamientos de "agencia" como los de conexión.

La propuesta de este encuentro viene de alguna forma a proponer explorar e intercambiar opiniones acerca de la violencia financiera, la importancia o no importancia de la educación financiera, concientizarnos con datos que conforman la brecha de género en el sector económico/financiero e imaginar significados para definiciones y/o frases como por ejemplo:

* **Inclusión financiera**
* **Educación financiera**
* **Finanzas éticas**
* **Economía feminista y economía de la vida**
* **Feminización de la pobreza**
* **Pedagogía financiera**

La **ética en las finanzas** nos impulsa a contemplar la brújula moral que guía las decisiones y acciones financieras. Las **finanzas inclusivas** nos alientan a explorar formas de garantizar que los servicios financieros sean accesibles y beneficiosos para todos los segmentos de la sociedad. La **economía feminista** nos invita a imaginar un modelo económico que desafíe las normas tradicionales de género y promueva la igualdad. La **educación financiera** nos permite considerar la importancia de dotar a las personas del conocimiento y las habilidades para tomar decisiones financieras informadas.

A través del diálogo abierto y perspectivas compartidas, podemos descubrir el significado y la importancia de estas ideas y moldear colectivamente nuestra comprensión de sus roles en la configuración de un panorama financiero más justo y equitativo.

#### Lo financiero

> Fuentes: [1](https://www.cronista.com/finanzas-mercados/inclusion-financiera-el-rol-de-las-fintech-y-la-deuda-pendiente-en-el-pais-y-la-region/), [2](https://www.pagina12.com.ar/311199-como-seria-una-inclusion-financiera-feminista)

* 77% de las personas de la región considera que necesita más educación financiera
* 57% de los hombres tienen cuenta bancaria, mientras que en las mujeres este porcentaje asciende al 51%
* 30% de las cuentas de inversión corresponden a mujeres y disidencias
* Mercado de capitales: alrededor del 40% de la nómina son mujeres
* 70% aproximadamente de cuentas comitentes están administradas por hombres y un 30% por mujeres

#### Lo económico y social

> Fuentes: [1](https://ecofeminita.com/1er_2023_td/?v=5b61a1b298a0), [2](https://www.es.amnesty.org/en-que-estamos/blog/historia/articulo/la-pobreza-tiene-genero/), [3](https://ecofeminita.com/1er-trimestre-2023-resumen/?v=5b61a1b298a0), [4](https://ecofeminita.com/1er-trimestre-2023-resumen/?v=5b61a1b298a0)

* 70% de las personas pobres en el mundo son mujeres
* Las mujeres realizan el 66% del trabajo en el mundo
* Las mujeres producen el 50% de los alimentos, solo reciben el 10% de los ingresos y poseen el 1% de la propiedad
* El 75.9% de trabajo informal en el rubro doméstico
* El 69% de las tareas domésticas son realizadas por mujeres
* La brecha de ingresos formales entre hombres y mujeres es de 27.9%

### Recursos y referencias

* 📰 [Género e inclusión financiera](https://www.ilo.org/empent/areas/social-finance/WCMS_737756/lang--es/index.htm), *OIT (Organización Internacional del Trabajo)*
* 📰 [La pobreza tiene género](https://www.es.amnesty.org/en-que-estamos/blog/historia/articulo/la-pobreza-tiene-genero/), *Amnistía Internacional*
* 📰 [¿Cómo sería una inclusión financiera feminista?](https://www.pagina12.com.ar/311199-como-seria-una-inclusion-financiera-feminista), *Página 12*
* 📰 [El rol de la mujer en las ciencias económicas](https://archivo.consejo.org.ar/consejodigital/RC61/castelli.html), *Revista consejo*
* 🎥 [El rol de la mujer en la actualidad](https://www.youtube.com/watch?v=94qK86P9rvI)
* 🎥 [Inclusión financiera digital desde la perspectiva de género](https://www.youtube.com/watch?v=qZzfq6pz9MU)
* 🎥 [If Lehman brothers were Lehman sisters](https://www.youtube.com/watch?v=pcl0kEeN4mk)
* 🎥 [Inclusion financiera de la mujer en México
](https://www.youtube.com/watch?v=fG4TI5yoSXE)
* 🎥 [Economía feminista en Jujuy: un taller de la Iniciativa Spotlight](https://www.youtube.com/watch?v=t5vRRrWMU5g)
* 🎥 [Les finances que volem](https://www.youtube.com/watch?v=F_WxUTfY9vI), en catalán

#### 📰 [Hacia una definición de violencia financiera](https://revistas.untref.edu.ar/index.php/ellugar/article/view/1026/839), *Untref*
#### 🖼️ [Finanzas éticas y economía feminista](https://finantzazharatago.org/wp-content/uploads/P%C3%93STER-Finanzas-%C3%A9ticas-y-econom%C3%ADa-feminista.pdf), Finantzaz Haratago

### Propuestas de trabajo

* ¿Existe la violencia financiera hoy en día? ¿Nos toca de cerca?
* ¿Qué se entiende por estereotipos en relación al dinero?
    * El hombre proveedor
    * La mujer gastadora
    * La falta de interés financiero en las mujeres
    * La inversión es para hombres
    * La falta de habilidades de negociación en las mujeres
    * Los hombres no necesitan ayuda financiera
    * La maternidad como barrera financiera para las mujeres
* ¿Cómo podemos contribuír a una definición de Violencia Financiera?
* ¿Qué entendemos por educación financiera? ¿Consideramos que es esencial en nuestras vidas?
* ¿Consideramos que tenemos una buena, mala o nula educación financiera?
    * Podríamos relacionar ese grado de educación con la brecha de género en relación al trabajo y las finanzas?
* Expansión del sistema financiero en sectores populares en forma de endeudamiento
* Conceptualizaciones acerca de la relación entre violencia y moneda
* El rol político y estratégico que desempeña en su función de crédito y deuda
* El  modo  específico  en  que  la  violencia  se operativiza y se aterriza a través de la deuda en los territorios de la reproducción social, articulando y conjugando violencias económicas y violencias machistas

### Debates, análisis y conclusiones

¿Si no elegimos los roles por esteretipos, por que los elegimos?

- Recorridos
- Seguridad de las personas
- Deseo
- habilidades
- potencialidades
- ¿como se define colectivamente?

**Formas de evaluación:**

- están sesgadas de acuerdo a como vemos el mundo. No evaluamos "imparcialmente". ¿por que queremos evaluar?¿como evaluamos?¿como se valoran las cualidades?
- pensar en el diálogo, proyectar lo que pensamos del otrx y recibir devolucion. Quizas evaluación no sería la palabra. Interconocimiento?
- ¿cual es el set de valores contra el cual evaluamos evaluar?
- Buscar armonía entre la necesidad de las organizaciones y los deseos y virtudes de las personas

**Violencia financiera**

- Rol de proveedor (tradicionalmente masculina) / rol de administración (tradicionalemnte femenina + roles de cuidado)
- Hoy esa división está en tensión desde el sistema (los dos generos tienen que proveer)
- no solo muta por la tension del ssitema sino por deseos de tener otros roles e independencia financiera.
- rol de Administración de las mujeres = capacidad financiera, no implica que sea la toma de decisiones.
- sigue sucediendo? en  que medida? en que clases?
- Origen de la violenca: la propiedad privada? la adminstración de la propiedad privada?

-------------------------------
<a id="6"></a>

## 6. Feminismo y tecnología 

> Abordamos colectivamente los temas referidos a ciberfeminismo y el lugar de la mujer en la ciencia.

### Material bibliográfico de referencia 

## Ciberfeminismo

* [Ciberfeminismo. Otra Internet es posible](https://www.youtube.com/watch?v=UfHCxaum4iI&ab_channel=AGESEXUNR) 


## Dificultades de las mujeres en la ciencia

* [8M: la ciencia es machista](https://www.youtube.com/watch?v=J7M9jfxh8R4&ab_channel=Televisi%C3%B3nP%C3%BAblicaNoticias) 


## Efecto Matilda

Robert Merton publicó en 1968 un célebre artículo titulado “The Matthew Effect” (“El efecto Mateo”), en el que describía un patrón de reconocimiento sesgado a favor del científico de más prestigio en los casos de colaboración así como en los casos de descubrimientos independientes. Este sesgo se debía,
según el autor, a un proceso circular de “acumulación de ventajas”, pues los reconocidos solían tener una buena reputación previa, puestos en grandes universidades o centros de investigación y discípulos bien situados, todo lo cual les ayudaba más. En cambio, los “perdedores” suelen ser figuras marginales, sin posición sólida ni discípulos que los puedan defender. Merton no criticaba en absoluto este sistema, sino que lo describía como algo “funcional” e incluso sugería que los científicos poco conocidos aprenden rápidamente cómo conseguir ventajas de esta asimetría. Lo llamó el “efecto Mateo” por la primera parte del versículo 13:12 del Evangelio según Mateo que dice: “Porque a cualquiera que tiene, se le dará más, y tendrá en abundancia”. Podríamos decir que el propio Merton es un ejemplo del “efecto Mateo”, pues aunque su artículo se basaba en la documentación procedente de la tesis doctoral de la historiadora Harriet Zuckerman, esta solo aparece en una nota a pie de página y apenas se reconoce su trabajo de recopilación de casos y el “efecto Mateo” solo se atribuye a Merton.En 1993, la historiadora Margaret Rossiter escribió un artículo titulado “The ~~Matthew~~/Matilda Effect” que comenzaba contando cómo el matemático Mark Kac describe en su biografía un viaje a Polonia para dar una conferencia en honor al físico Marian Smoluchowski, casi olvidado por completo. Lo interesante, señala Rossiter, es que no atribuye el olvido a que muriera a la temprana edad de 45 años, a su largo y difícil nombre, ni a haber desarrollado su trabajo en un país de la Europa del Este. En cambio, Kac atribuye el olvido al “efecto Mateo”, ya que Smoluchowski fue eclipsado por Albert Einstein, quien también trabajaba sobre el movimiento browniano en la misma época; pero Kac se centra en la segunda parte del proverbio, que no se suele mencionar cuando se habla de dicho efecto: “Y a quien no tiene, se le quitará incluso lo poco que tiene”.
Margaret Rossiter propuso llamar a esta segunda parte el “efecto Matilda” por la sufragista, estudiosa de la Biblia y
pionera en la sociología del conocimiento Matilda Joslyn
Gage, quien a finales del siglo XIX ya percibió el patrón,
sobre todo aplicado a las mujeres.

> García,_S_y_E_Pérez_Las_'mentiras'_científicas_sobre_las_mujeres

### Propuestas de trabajo

* ¿Qué situaciones vimos en la industria que dificulten la llegada de las mujeres y diversidades de género a la tecnología? y una vez que llegaron, ¿que dificulta y potencia su desarrollo?
* ¿Qué políticas estamos implementando desde cambá en torno al feminsmo tecnológico?
* ¿Cuáles podríamos empezar a desarrollar o mejorar?


-------------------------------
<a id="7"></a>

## 7. Género y políticas del conocimiento 

> Abordaremos el tema de géneros y políticas del conocimiento a partir de las palabras de Diana Maffía.

### Material bibliográfico de referencia 

Ideas principales:

* Aristóteles
* Androcentrismo
* Feminismo
* La ciencia como producto
* Ignorancia

1. [Conferencia LNF 2018: Género y políticas del conocimiento, Por Diana Maffía - Canal Encuentro](https://https://youtu.be/edT2LIQLEPo) 

### Propuestas de trabajo

1. Cada persona anota una palabra/frase claves que no hayamos charlado o profundizado en nuestra organización.
2. Preguntas disparadoras:
   * ¿Cómo podemos relacionarlo con nuestra organización?
   * ¿Se puede hacer algo al respecto?¿Qué cosa?
3. Luego de ver el video se propone realizar una puesta en comun charlando que sensaciones, ideas, frases nos dejo.

-------------------------------
<a id="8"></a>

## 8. Género y ciencia ficción 

> A partir de un capìtulo de Star Trek propuesto, se intentará reflexionar sobre los cruces entre géneros y ciencia ficción. 

### Material bibliográfico de referencia 

## Star Trek - "The Outcast"

### Intro:

Vamos a ver un capítulo de la serie Star Trek: The Next Generation (de ahora en más TNG). Esta serie es la segunda del universo de Star Trek. La serie original se emitió por TV durante la segunda mitad de la década de 1960. TNG, salió al aire desde 1987 hasta 1994.

TNG fue, de alguna manera, la revancha de su creador Gene Roddenberry. Parece que la primera vez no se había quedado conforme. Por eso, se desquitó con esta versión que tiene casi el doble de episodios que la primera y que, para muchos, es la mejor obra dentro de este universo de historias.

TNG, pese a ser una serie de ciencia ficción y naves espaciales, no se caracteriza por estar plagada de acción, tiro, lío y explosiones. Al contrario, es una serie en donde abunda el diálogo, el conflicto diplomático, el debate, la argumentación y la filosofía.

Además, en muchísimos episodios se tocan temáticas íntimamente ligadas a lo humano: la diversidad, el género, la sexualidad, las emociones, para dar sólo algunos ejemplos.

Sin ahondar sobre el capítulo que vamos a ver y discutir, sería bueno agregar tan solo un poco de contexto como para reponer las bases de este universo: TNG está situada en el siglo XXIV, un siglo después que la serie original. Existe el planeta tierra, no hay guerras, no hay pobreza y las personas viven en una cierta igualdad de condiciones. El planeta está gobernado por una sola fuerza que a su vez está agrupada en una confederación interplanetaria e interespecie que se rige por principios y normas compartidos.

Hay una invención de ese siglo, que hace posible el avance, la paz y la igualdad de condiciones entre seres, que es "el replicador". Es una tecnología que permite sintetizar casi cualquier cosa. Desde comida, hasta herramientas. Le podés pedir que te haga un té (con la taza y todo) o un sánguche de crudo y queso.

Bueno, para terminar, dejamos traducido el párrafo que la voz en off de Patrick Stewart lee al principio de cada capítulo, que también informa bastante: "El espacio, la frontera final. Estos son los viajes de la nave espacial Enterprise. Su continua misión: explorar extraños nuevos mundos, buscar nuevas formas de vida y nuevas civilizaciones, viajando temerariamente a donde nadie ha llegado antes."

Y ahora sí, los links pertinentes para que puedan ver el capítulo antes de nuestra próxima reunión de Género que será el miércoles 1 de noviembre.

La serie está en Netflix. Este es el [link](https://https://www.netflix.com/watch/70177979?trackId=14170286) al capitulo.


Es el capítulo número 17 de la 5ta temporada. 

## Precalentamiento (15' a 20')
### Introducimos la escena "Percepción del género entre razas": 

La raza humana en Star Trek ha trascendido casi todos los que consideramos grandes males de nuestras sociedades o conductas autodestructivas. Se han acabado las guerras, no hay pobreza, y las personas viven en una cierta igualdad de condiciones. Por otro lado, los Klingon son una raza de guerreros que prefieren solucionar sus diferencias y conflictos mediante la fuerza bruta. Estos prefieren entablar relaciones amorosas con los humanos, ya que los perciben como seres extremadamente frágiles. A continuación, veremos este pequeño fragmento del capítulo, donde podemos observar una conversación entre Deanna Troi (humano), Beverly Crusher (humano) y Worf (Klingon).
 
<iframe title="vimeo-player" src="https://player.vimeo.com/video/880107647?h=70c70fd091" width="640" height="360" frameborder="0" allowfullscreen></iframe>

[Link](https://https://vimeo.com/880107647) 

### Introducimos las escenas INTERACCIONES RIKER Y SOREN.
Los escritores del capítulo pretendían, en cierta forma, ponernos en los ojos del primer oficial William Riker a la hora de interactuar con Soren, integrante de la raza J'Nai. Buscando sorprender al espectador con esta figura disruptiva, la cual venía a compartir una visión distinta para la época en cuanto al género, sexo y sexualidad.

<iframe title="vimeo-player" src="https://player.vimeo.com/video/880113234?h=9a4cd120c9" width="640" height="360" frameborder="0" allowfullscreen></iframe>

[Link](https://vimeo.com/880113234)  



### Propuestas de trabajo


#### Introdución (5')
- Pequeña introducción comentando la breve historia que mandamos vía mail.
- Preguntar brevemente qué les pareció el capítulo, quiénes lo pudieron ver y para aquellos que no pudieron verlo, seleccionamos dos fragmentos muy interesantes.
- Igualmente, recomendar verlo de nuevo. Y además, ver Star Trek.
- Separamos en grupos de debate.
- Nos separan 30 años de este capítulo. ¿Cómo lo vemos hoy desde el 2023?
- Introducir esta idea: ¿Se puede pensar algún tipo de analogía entre todos los grupos o colectivos que aparecen en este capítulo (los J'Nai, los Klingon y los humanos) y nuestras sociedades actuales?

#### Consigna (20')
Si tuvieran la posibilidad, ¿reescribirían este capítulo? ¿Qué cosas le cambiarían? Reescribir el capítulo en grupo (en uno, dos o tres párrafos máximo), estando de acuerdo en cada una de las oraciones entre todos los miembros del grupo. Si les parece que modificarían nada, explicar por qué no (en uno, dos o tres párrafos máximo).

#### Puesta en comun (25')
Cada grupo lee a los demas compañeros el resultado de su intereaccion. Explicando si hubo algun punto que genero diferencias a la hora de poder completar la consigna.

### Debates, análisis y conclusiones

#### Frutilla del Postre
Relevando datos sobre el episodio, nos encontramos con repetitivos comentarios alegando que Rick Burmang, productor de la serie, era una persona un tanto homofóbica. Esto impactaba directamente tanto en el guion de cada capítulo, como también en los actores que se mostraban un tanto disconformes con este comportamiento.
Es Jonathan Frakes, quien interpreta a William Riker, el cual años después sostuvo que "Rechazar a un hombre para el papel de Soren, el alienígena de género indefinido del que se enamora Riker, dijo, 'siempre me ha parecido una oportunidad perdida'."

--------------------------
<a id="9"></a>

## 9 Conversación con Julio Maiza

> Julio Maiza es descendiente de pueblos originarios y como el mismo se define, "transculturizado", ya que recibió educación formal y además es psicólogo social.
Charlamos sobre el Tercer Malón de la Paz, cosmovisiones de pueblos originarios en relación al planeta y también a los colectivos.

### Material bibliográfico de referencia 

> [Link a la charla final](https://nubecita.camba.coop/s/2YasTKs5WPbNHe4)


### Debates, análisis y conclusiones

Recabamos a continuación algunas reflexiones de compañeras y compañeros sobre la conversación con Julio. 

1) Cuando menciona a los artesanxs ("artesanos y artesanAs!" citando a Julio xD), de aquella comunidad - golpeada por el ébola, si mal no recuerdo y no mezclé historias -, y cómo entre ellxs acuerdan no regatearles a los foránexs.
Esto en particular me resultó super interesante porque hace unos meses en redes comenzaron algunos movimientos (sobre todo en México) pidiendo que se termine el regateo ya que son lxs artesanxs quienes preservan las tradiciones (patrimonio histórico de cada lugar), a través de su trabajo y comercialización. Cada obra y pieza es única, hecha a mano e implica el esfuerzo, tiempo, conocimientos y recursos del autor (más alla de que representa su principal fuente de ingresos).

2) Cuando se "diferencia" a los malones y se menciona a las cautivas. El movimiento malón de la paz para emancipar o reivindicarlo por ejemplo, de esa idea del malón violento que arrasa y rapta mujeres "blancas". Navegando para investigar un poco sobre los malones de la paz encontré esta nota del 2 de agosto del presente año que menciona el tema (y algo más): https://agenciatierraviva.com.ar/un-malon-de-la-paz-para-interpelar-al-poder-y-al-falso-progreso/ y quienes están detrás de la nota: https://agenciatierraviva.com.ar/quienes-somos/.

3) Algo que se mencionó al pasar pero no se pudo debatir: que en las comunidades de pueblos originarios (sin querer generalizar), se considera mujeres a las niñas que tienen la menarca, y se las comienza a ver como madres que cuidan y protegen a la comunidad.

4) Y por último, la relacion de las comunidades con la tierra y la naturaleza y la importancia que le dan a la perservación y cuidado del medioambiente.

* Yo me quedé intrigado por saber si en los lenguajes de los pueblos originarios existen también dos verbos como "ser" y "estar". No en todos los idiomas existen esas dos variantes. En inglés, por ejemplo hay un solo verbo que condensa el uso que nosotros le damos a ser y a estar.

* "El lider es la tarea"

* "Responsabilidad: Responder con Habilidad"

* Por esto que comentas lo decía en contexto de unas personas (médicas de una ONG o fundación nueva, cruz del sur) que lo contactaron para interceder por ellas con la comunidad. Estas personas ya habían comprado artesanías y esto de "no regatear" era una de las condiciones que puso Julio para participar de esa tarea. La otra era que la comunidad decidiera para que usar el dinero que llevaban desde esta fundación.


* Esto es un tema bastante complejo en relacion con las niñeces y los "malones" también existe esto a lo que llaman "chineo":
- https://www.clarin.com/sociedad/25-mujeres-wichis-denunciaron-abusos-sexuales-violaciones-criollos-cacerias-humanas-salta_0_1z5EWw4L78.html

- https://www.mdzol.com/sociedad/modus-operandi/2020/8/19/que-es-el-chineo-la-aberrante-practica-de-abuso-contra-ninas-mujeres-aborigenes-99470.html

* También me llamó la atención, sobre todo esto de que no tienen el concepto de "adolescencia". Tanto niñas y niños pasan de niñez a adultez. También podríamos indagar en otra ocasión que onda con las otras identidades (trans, no binaries, etc).


* Súper interesante la charla, está bueno también recuperar el cuidado de los espacios como prioridad, más en estas épocas de súper explotación de recursos. Puede ser buen punto para trabajar con energías renovables (generadores eólicos/hidráulicos) y/o aportar ecología/reutilización/reciclaje a los talleres de arduino quizás?

* Vengo pensando bastante sobre esto, sobre todo después de la participación en las JRSL de córdoba[1], y hay algo que me interesaría laburar en cambá el año próximo que tiene que ver con las herramientas que usamos. Más allá de las fuentes de energía, ¿es necesario gastar procesamiento y memoria levantando una interfaz gráfica de un procesador de texto como libreoffice, o Word, para escribir unas líneas?¿podemos tener un set de herramientas austeras, que funcionen en máquinas con menos recursos pero que igual nos permitan laburar?¿como sería un "uso racional del software?

* "La noción de grupo es primero"
* "En distintas cosas vamos a ser líderes" La tarea te hace líder. 
* "El pensamiento es también una circulación de energías cognitivas entre multiplicidad de personas y colectividades, y que florece a través del encuentro con interlocutorxs concretxs." Silvia Rivera Cusicanski. Sociología de la Imagen: miradas Ch´ixi desde la historia andina.


--------------------------
<a id="10"></a>

## 10 Comunicación no Violenta

> La Comunicación No Violenta® (CNV) se orienta al desarrollo de capacidades de comunicación e
interacción para la colaboración, el intercambio fluido entre las personas y la resolución de
diferencias y conflictos teniendo en cuenta a todos y todas los/ las involucrados/as.

### Material bibliográfico de referencia 

[**Pequeño Manual de Comunicación no Violenta**](https://nubecita.camba.coop/s/EoQPM2szqHEHTPg)

«El objetivo de la comunidad es asegurarse de que todos sus miembros son escuchados y hacen entrega de los dones que han traído a este mundo. Sin esa entrega, la comunidad se muere. Y sin la comunidad, el individuo se queda sin lugar donde hacer su contribución. La comunidad es ese lugar que nos equilibra y al que las personas acuden para compartir sus dones y recibir los de los demás»

_Sabonfu Somé. Enseñanzas Africanas sobre el Amor y la Amistad_

El objetivo de la Comunicación No Violenta® (CNV) es mejorar la calidad de nuestras relaciones y conversaciones para así establecer vínculos basados en la sinceridad, el respeto y la empatía, a la vez que tenemos en cuenta las
necesidades propias y ajenas.

La cultura de toda organización se entreteje a partir de las conversaciones que mantenemos y, si bien la evolución y la mejora en las conversaciones suele llevar tiempo, es momento de comenzar a transitarlo sembrando las primeras semillas para la escucha empática, el hablar con intención, autorregulando nuestro impacto.

Un supuesto básico de la CNV es que cada vez que actuamos estamos respondiendo a una necesidad e intentando satisfacerla. Por esta razón, es muy importante tomar conciencia de nuestras necesidades y asumir la  responsabilidad sobre aquello que estamos eligiendo atender o responder.
Para aprender y practicar esta propuesta, Marshall Rosenberg propone entrenar cuatro capacidades, que también podemos considerar como componentes de la CNV:
● Observar sin evaluar.
● Conectar con nuestros sentimientos.
● Explorar dentro nuestro hasta tomar contacto con las necesidades que subyacen a los
sentimientos.
● Hacer un pedido claro y de acción positiva.

### Propuestas de trabajo

1. Describir una situacion donde haya interacción entre 2 o más personas. Puede ser de la coope o individual, feliz o alegre, real o ficticia, es indistinto, lo importante es que haya comunicación entre les involucrades. (10 minutos)

2. Leer el material que le toca a cada grupo, buscando los puntos claves. (15 minutos)

3. Reveer y adecuar la situación teniendo en cuenta el material leido y explicar brevemente como lo aplicarían para que exista CNV (25 minutos)

4. Puesta en comun. (30 minutos)

-----------------
<a id="11"></a>

## 11 Sara Ahmed y el Giro afectivo

> Ahmed ha hecho importantes aportaciones al giro afectivo, corriente metodológica interesada en el estudio de los afectos en tanto cómo se relacionan entre sí, sus zonas de contacto y los efectos y prácticas que producen. Ahmed analiza cómo los discursos o escenas afectivas ayudan a reproducir un modo de vincularse hegemónico y normativo.

### Material bibliográfico de referencia

Video ["palabras en llamas Sara Ahmed"](https://www.youtube.com/watch?v=2tjLj0c33y4)

[Texto Anexo](https://lab.cccb.org/es/feministas-aguafiestas/)

[Giro Afectivo](https://www.redalyc.org/pdf/6060/606066848013.pdf)


### Propuestas de trabajo

"Emociones enfrascadas"

En un frasco ponemos papelitos con nombre de las emociones:

interés-disfrute-alegría-sorpresa-susto-disgusto-angustia-indignación-ira-miedo-terror-vergüenza-humillación-repugnancia-repulsión-energía-entusiasmo-excitación-tristeza-felicidad-simpatía-turbación-culpa-orgullo-celos-envidia-admiración

Nos dividimos en grupos.
Sacar 3 emociones del frasco.

1. ¿Cuáles son los condicionamientos sociales que hay identificamos sobre esa emoción?
2. ¿Cómo impactan en la división génerica, heteronormativa y modélica del mundo en esas emociones?
3. ¿En qué situaciones cotidianas de nuestra vida podemos vivenciar algunos de los preceptos de Sara Ahmed vinculado a esa emoción?
4. ¿Cuáles creemos que son los desafíos que tenemos en este tema en nuestra construcción colectiva?

### Debates, análisis y conclusiones

* Angustia: Emoción aplacadora. 
* Zanahoría ajena. Sirve de motor para otros, no para uno mismo. 
* Redes sociales: Solo mostrar cosas buenas, no se 
* Vidriera ejemplificadora. 
* Cambiar modelo de ama de casa a emprendedora. Modelo de felicidad. 
* Modelo que se referencia a un tipo de capitalismo. Falso, independencia, libertad. 
* Afinar las estrategias del capitalismo. 
* Modelo de ser ofrecido, un envase vacío de promesas. 
* ¿Para quienes? Hombre-mujer blanca heterosexual. 
* Para el mismo sentimiento hay diferentes tratamientos. 
* Individualismo. 
* Exotismo. 
* Alimentar maquinaria. 
* Preguntarnos: ¿dónde vamos a construir qué cosas? Para que se salga de esa lógica. Red social Mastodon. Algoritmo. 
* Subjetividad: disgusto, sentido social de esto. 
* Dimensión social.  
* Según a qué clase social perteneces. ¿Cuánto tiempo duran las emociones aceptablemente en nuestra sociedad? Juzgamiento sobre determinadas emociones válidad y otras no. 
* Orgullo como bloqueante para la construcción colectiva. 
* Cómo está nuestro sujeto colectivo (la coope) armado para poder ser caja de resonancia copada. 
* La moda. La compasión, el perdón. 
* Pasividad. No confrontamiento - enfrentamiento. No exposición. No sentirse mal, zafar de la situación. 
* Gestión individual vs gestión colectiva. 

-----------------
<a id="12"></a>

## 12 Filosofía Queer

Vamos a indagar en este concepto y a cruzarlo con las percepciones personales sobre el tema. 

### Material bibliográfico de referencia

[Video "Queer: Qué es y con qué se come" subido por la cuenta Abrazo Grupal](https://www.youtube.com/watch?v=oFUU8Qv2l20&t=12s)

## Ideas

- LGBT <> HETERONORMA CIS PATRIARCAL: Se subdivide a la gente desde la perspectiva sexual y de expresión de género.

- QUEER <> STRAIGHT: Es una oposición de la que se apropia el colectivo LGBT..., pero que permite pensarse más allá de la sexualidad o la perspectiva de género.

Pautas para seguir pensando y analizando:

- El término queer significa 'raro', 'retorcido', 'distinto'. Por lo contrario, straight significa 'recto', 'liso', 'derecho'.

- Históricamente el término se usó para discriminar, menosprecias, desacreditar, separarse de lo "normal". El colectivo se lo apropia como autodefinición, sintiéndose orgullosamente del otro lado de lo violento y discriminatorio. Se resignifica el término. 

- Hay una analogía entre las personas y los árboles. Como los árboles, como las plantas, todes crecemos en condiciones diferentes. Y nos vamos formando y estirando para alcanzar lo que necesitamos. En ese esfuerzo, algunes quedamos un poco retorcides (o queer).

- Concepto de Normalización (Foucault). Lo normal es convenido socialmente, no es natural. Se tiende a pensar que la naturaleza es normal. Y para Judith Butler, no hay nada más queer que la naturaleza.

### Propuestas de trabajo
- ¿Podemos nosotros pensarnos en estos términos (Queer o Straight) yendo un poco más allá y pensándolo no exclusivamente sexual o de expresión de género?
- ¿Podemos pensarnos en estos términos a nivel personal? ¿Qué de todo lo que hacemos y de lo que somos nos aleja o nos acerca a estos conceptos de QUEER o STRAIGHT (raro o normal)?
- ¿Podemos pensarnos en estos términos como colectivo? ¿Qué es todo aquello que no es sexual o expresión de género que no nos hace entrar en la norma?
- Atreviéndonos a jugar con estos conceptos, como venimos haciéndolo, pensándolos desde una perspectiva más amplia, ¿Quiénes son/somos más en el mundo? ¿Les Queer o les straight? ¿Seguimos hablando de una minoría excluida o de una mayoría alienada?

-----------------
<a id="13"></a>

## 13 Sesgo de género en IA

En este taller proponemos iniciar un debate sobre violencias de género tomando como disparador el sesgo de género en la IA.

### Material bibliográfico de referencia

Sesgo en la inteligencia artificial: https://www.youtube.com/watch?v=QJJy1odKDe4&ab_channel=EnPerspectiva

https://www.pagina12.com.ar/726000-el-riesgo-es-que-la-ia-perpetue-los-sesgos-de-genero

### Propuestas de trabajo

Pensar violencias de género que ocurren habitualmente tanto en los ámbitos institucionales, como en el cotidiano y escribirlas cada una en un papelito.

Juntar lo elaborado por los grupos y ubicar las violencias mencionadas en un Iceberg que diferencie las mas visibles de las menos visibles.

-----------------

