const sidebarGenerator = require('./sidebar-generator');
const path = require('path');

const deploySubdir = '/ctte-ezine/'
const extendsNetworks = {
  email: {
    sharer: 'mailto:info@camba.coop?subject=@title&body=@url%0D%0A%0D%0A@description',
    type: 'direct',
    icon: './email.png',
  },
  linkedin: {
    sharer:
      'https://www.linkedin.com/shareArticle?mini=true&url=@url&title=@title&summary=@description',
    type: 'popup',
    color: '#1786b1',
    icon:
      '<svg viewBox="0 0 1024 1024" xmlns="http://www.w3.org/2000/svg"><path d="M910.336 0H113.664A114.005333 114.005333 0 0 0 0 113.664v796.672A114.005333 114.005333 0 0 0 113.664 1024h796.672A114.005333 114.005333 0 0 0 1024 910.336V113.664A114.005333 114.005333 0 0 0 910.336 0zM352.256 796.330667H207.189333V375.466667h145.066667z m-72.021333-477.866667a77.824 77.824 0 0 1-81.237334-74.069333A77.824 77.824 0 0 1 280.234667 170.666667a77.824 77.824 0 0 1 81.237333 73.728 77.824 77.824 0 0 1-81.237333 73.386666z m582.314666 477.866667H716.8v-227.669334c0-46.762667-18.432-93.525333-73.045333-93.525333a84.992 84.992 0 0 0-81.237334 94.549333v226.304h-140.629333V375.466667h141.653333v60.757333a155.989333 155.989333 0 0 1 136.533334-71.338667c60.416 0 163.498667 30.378667 163.498666 194.901334z" /></svg>',
  }
}


module.exports = {
  title: 'CTTE ezine',
  description: 'Ctte ezine - el ezine de Camba.coop',
  base: deploySubdir,
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700&display=swap' }]
  ],
  plugins: [
    ['@vuepress/search', {
      searchMaxSuggestions: 10,
    }],
    [ 'feed', {
      canonical_base: deploySubdir,
      posts_directories: ['/']
    }],
    [ 'social-share', {
      networks: [ 'twitter', 'facebook', 'telegram', 'linkedin', 'email'],
		  twitterUser: 'cambacoop',
			extendsNetworks
    }],
    [ 'matomo', {
			trackerUrl: 'https://metricas.camba.coop',
			siteId: '6',
      trackerPhpFile: 'matomo.php',
      trackerJsFile: 'matomo.js'
		}]
  ],
  themeConfig: {
    editLinks: true,
    editLinkText: 'Viste un error? Edita esta página',
    logo: '/img/logoc.svg',
    docsDir: 'docs',
    repo: "https://gitlab.com/camba/ctte-ezine",
    searchPlaceholder: 'Buscar...',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Género', link: 'genero.md' },
      { text: 'Website', link: 'https://camba.coop' },
      { text: 'Blog', link: 'https://blog.camba.coop' },
      { text: 'LTC', link: 'https://ltc.camba.coop/' },
      { text: 'Licencia', link: 'https://labekka.red/licencia-f2f/' },
    ],
    sidebar: [{
      title: 'Ediciones',
      collapsable: false,
      children: sidebarGenerator()
    }],
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@img': path.resolve(__dirname, 'public', 'img'),
      },
    },
  },
}

// Alternativa sin encabezado
// sidebar: {
//   '/': sidebarGenerator('ediciones')
// }
