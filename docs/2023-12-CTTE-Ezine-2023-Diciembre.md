---
title: Diciembre 2023
---

# Cambá CTTE Ezine Diciembre 2023

<div style="text-align: center;">

![Ilus](@img/diciembre2023.jpg)

</div>

```
Cuando te despiertes cada día
con el cuerpo de aire y ese olor
feliz del sueño manso de las lilas
sin miedo al movimiento ni al dolor.

Cuando yo no tenga casi nada
de sangre en la garganta de papel
ni un agrio pez nadando en la mirada
ni quiera más amparo que la piel.

Van a ser los días esos barcos
de luz que una vez pude escribir
y la alegría que hemos olvidado
volviendo por los huesos a subir.
Yo me alimento con una quimera
en que los ojos al sol verán brillar
los brazos de mi padre en las banderas
y una ceniza negra, y una ceniza negra
y una ceniza negra que se va.

Cuando me convenza que la suerte
me rige a la par que la pasión
y no el temible arcángel de la muerte
velando sobre el campo del reloj.

Si lo consumado y lo posible
tienen siempre la cara del horror
en esta patria de lo inaccesible
en este tiempo olvidado de Dios.

Yo digo que mis ávidos amores
son fuertes y viven más que yo
son gigantes tenues como flores
que alientan este turbio corazón.
Los alimento con una quimera
en que los ojos al sol verán brillar
los brazos de mi padre en las banderas
y una ceniza negra, y una ceniza negra
y una ceniza negra que se va
```

Letra del tema [Cuando](https://www.youtube.com/watch?v=wgrjdMGzgPY) de [Jorge Fandermole](https://es.wikipedia.org/wiki/Jorge_Fandermole)

## Cooperativas/Economía Popular
- [Belén Sánchez: "El arte tiene mucho para contar y para despertar en cuanto a la sensibilidad"](https://ar.radiocut.fm/audiocut/belen-sanchez-arte-tiene-mucho-para-contar-y-para-despertar-en-cuanto-a-sensibilidad/)
- [Julio Maiza: Charla para Cambá](https://nubecita.camba.coop/s/2YasTKs5WPbNHe4)
- [Tutorial - La Asamblea](https://www.youtube.com/watch?v=ojRJtm-iaCs)


## Herramientas de software
- [LibreTranslate](https://github.com/LibreTranslate/LibreTranslate) - Api de traducción para diferentes Idiomos Libre y Local
- [Kiwix](https://kiwix.org/en/) - Contenido de Internet sin acceso a Internet
- [Lago](https://github.com/getlago/lago) - Software Libre para crear diferentes tipos de Facturación
- [Fasttracker 2 clone](https://github.com/8bitbubsy/ft2-clone) - Mítico tracker reversionado
- [kitforstartups](https://github.com/okupter/kitforstartups) - SvelteKit SaaS boilerplate

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Resultado de la Encuesta "El estado de WebAssembly 2023"](https://blog.scottlogic.com/2023/10/18/the-state-of-webassembly-2023.html)
- [Lanzamiento de Blender 4.0](https://wiki.blender.org/wiki/Reference/Release_Notes/4.0)
- [Análisis de Notebook Lemur Pro de System76](https://www.wired.com/review/system-76-lemur-pro-laptop/)
- [Carta a Jóvenes Educadores](https://chiquigonzalez.com.ar/project/carta-a-jovenes-educadores/)


## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Viaje por la mente de Berardi](https://www.youtube.com/watch?v=jVI1fQWovpY)
- [¿Por qué las masas desean al fascismo? Una lectura de Wilhem Reich](https://www.youtube.com/watch?v=KcP9cjT7Fi0)
- [Filosofia Unix](https://video.hardlimit.com/w/a7af7f23-76ed-41e8-86b3-d6bc08ab03b4)
- [La toma. Ocupar, resistir y producir](https://kolektiva.media/w/gCrHEzS6KUohhtezsiCPhH)


### Tecnología
- Eko party Videos
    - Intro de [Leonardo Pigñer](https://youtu.be/YT67JE9o8d8?t=722)
    - [KeyNote de Iván Arce: Referente Argentino Histórico de H/P/V/C/A](https://youtu.be/YT67JE9o8d8?t=1217)
    - [Entrevista a Iván Arce](https://youtu.be/YT67JE9o8d8?t=7934)
    - [Charla de Cesar Cerrudo](https://youtu.be/YT67JE9o8d8?t=12733)
- Web Componentes: ¿Necesitamos los frameworks? [Articulo 1](https://blog.jim-nielsen.com/2023/html-web-components-an-example/) - [Articulo 2](https://jakelazaroff.com/words/web-components-eliminate-javascript-framework-lock-in/)
- eBPF Kernelless Kernel Programming - [Explicación](https://www.youtube.com/watch?v=Wb_vD3XZYOA) - [Documental](https://www.youtube.com/watch?v=J_EehoXLbIU)
- [Argentina: hackeando en el culo del mundo](http://phrack.org/issues/45/27.html#article)


### LLMs
- Detección de uso de IA
    - [ZeroGpt](https://www.zerogpt.com/)
    - [Ghostbuster](https://github.com/vivek3141/ghostbuster) - [App](https://ghostbuster.app/)
    - [DetectGpt](https://detectgpt.com/)
- DeepSeek-Coder: Modelo especial para programación
    - [Video Intro](https://www.youtube.com/watch?v=POSOmtJCOeI)
    - [Código](https://github.com/deepseek-ai/deepseek-coder)
    - [Web](https://deepseekcoder.github.io/)
- Construyendo ChatGPT libres desde cero
    - [Video intro](https://www.youtube.com/watch?v=rIRkxZSn-A8)
    - [Código](https://github.com/jmorganca/ollama)
    - [Web](https://ollama.ai/)
- [The Wrecking Ball: Demoliendo IAs, por Enrique Chaparro](https://www.youtube.com/watch?v=NY6FF14cALQ)
