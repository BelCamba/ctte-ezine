---
title: Septiembre 2022
---

# Cambá CTTE Ezine Septiembre 2022

<div style="text- align: center;">

![arde.ruge.urge](@img/fuego.png)

</div>

```
Imposible es rescatar, volver a vivir ciertos momentos
si por un desengaño juntaste mucha bronca y
si por un desengaño juntaste mucha rabia
pero vamos que nada es para tanto y tanto no lo es todo,
con firmeza y ante todo mantiene tu espiritu con humor

Vence tus tabúes, deja atras tu timidez
si en verdad lo nesesitas ve a buscar lo que te gusta
no te engañes, no te mientas
que nada es para tanto y tanto no lo es todo,
con firmeza y ante todo mantiene tu espiritu con humor.
```

Canción [**Mantiene tu espiritu con humor**](https://www.youtube.com/watch?v=hcc0CKAK8h4) de [Fun People](http://www.funpeople.com.ar)


## Contribuciones de Cambá al Software libre

- [Vue3 Open Layers](https://github.com/MelihAltintas/vue3-openlayers/): Capa de Vue3 para utilizar la libreria de Mapas Open Layers
    - [Add Heatmap layer support](https://github.com/MelihAltintas/vue3-openlayers/pull/89)

- [cubejs-client/vue3](https://github.com/cube-js/cube.js/tree/master/packages/cubejs-client-vue3): Cliente Vue3 de CubeJs - Sistema Headless BI
    - [fix removeOffset warning](https://github.com/cube-js/cube.js/pull/5083)
    - [Avoid setQuery Vue3 warnings](https://github.com/cube-js/cube.js/pull/5120)
    - [Support logical operator filters](https://github.com/cube-js/cube.js/pull/5119)



## Herramientas de software

- [Ruffle](https://ruffle.rs) - Un emulador flash player construido en Rust
- [SafeEyers](https://slgobinath.github.io/SafeEyes) - Aplicación para reducir y prevenir Lesiones por movimientos repetitivos
- [Aerc Mail](https://aerc-mail.org) - Un cliente de email bastante bueno
- [Simplex](https://github.com/simplex-chat/simplex-chat) - Encriptada y privada - La única plataforma win identificación de usuaries


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Flash Party 2022](https://flashparty.rebelion.digital/) - Preparate para Flashparty 2022! Presencial - en Buenos Aires - 1-2 de octubre!
- [Manual Software Libre para docentes con Huayra GNU/LINUX](https://hernanalbornoz.wordpress.com/2022/04/15/manual-software-libre-para-docentes-con-huayra-gnu-linux-para-descargar/)
- Lanzamiento público de [Stable Difussion](https://stability.ai/blog/stable-diffusion-public-release) [Probar online](https://huggingface.co/spaces/stabilityai/stable-diffusion) - [Modelo](https://huggingface.co/CompVis/stable-diffusion)
- [Lanzamiento de Docusaurus 2.0](https://docusaurus.io/blog/2022/08/01/announcing-docusaurus-2.0)
- [Dell tiene una landing page para compus con linux](https://dell.com/en-us/work/lp/linux-systems)


## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Presentación del libro «El tercer inconsciente», con Franco ‘Bifo’ Berardi](https://www.youtube.com/watch?v=vkIrKbnVEpg)
- [Eric Sadin - La ingobernabilidad permanente](https://lavaca.org/notas/eric-sadin-nuestra-epoca-esta-marcada-por-la-ingobernabilidad-permanente/)
- Adam Jacob en OSCON 2019 - [La guerra por el alma del "open source"](https://www.youtube.com/watch?v=8q5o-4pnxDQ)
- [Cuatro cosas que se necesitan para ser un experto](https://www.youtube.com/watch?v=5eW6Eagr9XA)

### Tecnología

- [¿Como Correr software sin sistema operativo](https://stackoverflow.com/questions/22054578/how-to-run-a-program-without-an-operating-system)
- [Vulnerabilidad - Linux kernel dirty cred](https://zplin.me/papers/DirtyCred-Zhenpeng.pdf) - [Video explicativo](https://www.youtube.com/watch?v=gqB3w-M711o) - [Código fuente](https://github.com/Markakd/DirtyCred)
- [Evitar el colapso de la civilización Jonathan Blow (Thekla, Inc)](https://www.youtube.com/watch?v=ZSRHeXYDLko)
- [Scratch? Python? C? Kernighan hablando sobre lenguajes para infancias](https://www.youtube.com/watch?v=h8LTEFNLZ6M)


### Cooperativas/Economía Popular

- [Cambá: Difusión Taller fábrica de inventos en Radio Ahijuna](https://radiocut.fm/audiocut/nota-difusion-del-taller-fabrica-inventos-radio-ahijuna-jueves-25-08-2022/)
- [Podcast de FACTTIC -  Desmuteando: Episodio 1 - Manu Leiva (Redjar)](https://www.youtube.com/watch?v=ZPHiS2gpMrQ)
 [Agustín Mosteiro cuenta sobre la Cooperativa Eryx en AM870](https://radiocut.fm/audiocut/agustin-mosteiro-cooperativa-eryx/)
- [¡CoopCycle en Argentina!](https://platform.coop/blog/coopcycle-en-argentina/)
- [Show&Tell por los 10 años de FACTTIC](https://www.youtube.com/watch?v=fsvkr1pVT_Y)

