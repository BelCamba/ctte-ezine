---
title: Septiembre 2020
---

# Cambá CTTE Ezine Septiembre 2020

## Texto

```
Suena brisa de incertidumbre, nuestras hojas bailan.
alguna lluvia encontramos, mas no fué inesperada.
positiva expectativa, en sosiego nos mantiene.
caminamos un sendero, de colores entrelazados.
la primavera nos alerta, hay desafíos aguardando.

desde un segundo piso, las calles observamos.
distantes a la realidad, refugiades nos sentimos.
destellos en el horizonte, corazones iluminan.
el verano nos invita, fijemos nuestro rumbo.
sobre idilia discutimos, soslayando enfrentamiento.

bajo abrigo de otoño, nuestros hogares nos reciben.
tras la tormenta hojas caen, inestabilidad resuena.
desde lejanías inmensurables, despedimos en silencio.
frío viento en lo profundo, suspira lluvia que no llega.
de tempestades aprendimos, no bajamos nuestros brazos.

las nubes se despejan, nocturno el cielo que encontramos.
el amanecer comienza, un suspiro nos regala.
con amable calidez, el invierno nos alberga.
gozando lucidez, nuestras diferencias abrazamos.
nos queremos reencontrar, no estemos esperando.

volvemos a florecer, con la primavera comenzando.
```

Texto escrito por Estefania Prieto, utilizado para el [video que se genero para el Jornadas de Reflexión de Primavera 2020](https://www.youtube.com/watch?v=8HNk08bzgDE)


## Proyectos de Software libre

- Contribución de Facundo Mainere a Proyecto [Lesspass](https://lesspass.com/):
    - [PR](https://github.com/lesspass/lesspass/pull/549)
- [Fiqus](https://fiqus.coop/) libera:
    - [Pirra](https://github.com/fiqus/pirra) - Facturas electronicas AFIP como Zeus manda]
    - [CooBS](https://github.com/fiqus/coobs) - [Web](https://coobs.io/) - La plataforma cooperativa libre para el balance social
- Contribución de Santiago Bota al Proyecto [Coopcycle](https://coopcycle.org/):
    - [PR Aplicación móvil](https://github.com/coopcycle/coopcycle-app/pull/768)
    - [PR Aplicación Web y Backend](https://github.com/coopcycle/coopcycle-web/pull/1655)

## Herramientas de software

- [WebGazer.js](https://github.com/brownhci/WebGazer) - Seguidor de ojos por medio de Webcam
- [Webamp](https://github.com/captbaritone/webamp) - Reimplementación Winamp 2 para el navegador
- [ButterChurn](https://github.com/jberg/butterchurn) - Implementación WebGL de Visualizador Milkdrop
- [AdminBro](https://github.com/SoftwareBrothers/admin-bro) - AdminBro es un panel de administración para aplicaciones en Node.js
- [AudioStellar](https://gitlab.com/ayrsd/audiostellar) - Instrumento para el descubrimiento y experimentación de música

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [ADC estrena en Chile la app PubliElectoral](http://adc1.mailrelay-iv.es/mailing/126054/69.html?t=525849485a07075450495405575905360c500601010c57010f0c0a075202535758164c5d5453505c1252) - Herramienta creada para el control ciudadano del gasto electoral en redes sociales - [Web](https://publielectoral.lat/)
- [Show&Tell Internacional: Loomio ayuda a tomar decisiones colectivas](https://www.facebook.com/watch/live/?v=821960125300034&ref=watch_permalink)
- [Evan You presente Vue 3](https://www.youtube.com/watch?v=Vp5ANvd88x0)
- [Lanzamiento de Ionic Vue Beta](https://ionicframework.com/blog/announcing-the-new-ionic-vue-beta/)
- [Lanzamiento github cli 1.0](https://github.blog/2020-09-17-github-cli-1-0-is-now-available/) - [Web](https://cli.github.com/)


# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Primera conferencia de seguridad con charlas técnicas impartidas por mujeres para todo el mundo](https://www.linkedin.com/company/notpinkcon/)
- [Khipu Computador Prehispánico electrotextil - CONSTANZA PIÑA Pardo](https://www.youtube.com/watch?v=-L5RoBMKOdM)
- ["La potencia feminista" en la Inauguración de la Feria del Libro Independiente de México](https://www.youtube.com/watch?v=3tkgpio4ZZI)
- [En letras de sangre y fuego - Trabajo, máquinas y crisis del capitalismo Caffentzis y Federici](https://tintalimon.com.ar/post/en-letras-de-sangre-y-fuego/)
- Participación de Florencia Otarola en [Encuentra Digital: Jóvenes y Economía Social para el alcance de los ODS](https://www.facebook.com/watch/?v=311384083277681&extid=KxTrLOdvEUydKHk7) - Desde 1:03:50 hasta 1:13:42

### Sociales/filosóficos

- [Entrevista a Diego Saravia sobre Software Libre](https://www.youtube.com/watch?v=zU7yj9qT_C0)
- [Herramientas digitales para el trabajo, la educación y socialización](https://www.idelcoop.org.ar/revista/231/herramientas-digitales-trabajo-educacion-y-socializacion) - Escrito por Neto Licursi en Revista Idelcoop, Número 231 / Año 2020
- [El Laboratorio de tecnologías creativas de Cambá y la pandemia](https://ltc.camba.coop/2020/08/26/el-laboratorio-de-tecnologias-creativas-de-camba-y-la-pandemia/) - Escrito por Neto Licursi
- [Participación de Fiqus, Nayra y Cambá en Experiencias de organización comunitaria de la Secretaria de Juventud de Neuquén](https://www.facebook.com/watch/live/?v=330367018293341&ref=watch_permalink)

### Técnicos

- ["Cloaking" de imagenes para privaidad de las personas](http://sandlab.cs.uchicago.edu/fawkes/) - Usar [Código fuente](https://github.com/Shawn-Shan/fawkes) para que la IA no nos pueda reconocer
- [Cinco técnicas no obvias para el trabajo remoto](https://queue.acm.org/detail.cfm?id=3417752) - Caso basado en Stack Overflow
- [Práctica básica de Webassembly](https://evilmartians.com/chronicles/hands-on-webassembly-try-the-basics)
- [Sindrome del impostor y trabajo remoto](https://about.gitlab.com/blog/2020/09/02/imposter-syndrome-and-remote-work/)

### Otras

- Columna de Tecnología de Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
  Entrevista a Julián Macías Tovar - Sonámbules 2020 - FM LasChacras 104.9 [Audio](https://archive.org/details/sonambules-entrevista-julian-m-tovar)
- [Recuerdo de Casi hacker](https://www.farco.org.ar/se-realizo-casi-hacker-el-taller-de-software-libre-para-las-radios-comunitarias/) - Cambá participo en un Evento organizado pr Farco en Radio Ahijuna
