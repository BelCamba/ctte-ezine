# Molecula de la cultura de Satellogic

![cultura molecula](@img/cultura-satellogic.jpg)

## Satellogic

- Esta empresa es una empresa Argentina de las más reconocidas en el área técnica
- Tiene gente conocida y reconocida tecnicamente muy capaz
- También tienen un condimento negativo de estar alineados ideológicamente a varios de los postulados del gobierno de los últimos años

## Su Cultura

- Esta centrada en el *Hay que hacerlo*.
- Para eso hay que alentar a los demás, focalizar en una visión más macro, tener motivación propia y generar valor.
- Fomentando El espiritu de equipo, la humildad, la curiosidad y los desafios.
- Basandose en ir más allá del ego, de los límites, orientadx a las meta y nunca parar de aprender.

