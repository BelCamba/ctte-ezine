# Canciones con temática feminista

Durante mucho tiempo en mi adolescencia escuche mucha música y mis bandas favortias tenian letras que hacían referencias a temas feministas o con una fuerte carga ideológica con valores feministas. Acá alguno de ellos:


## [Fun people - Run away](https://www.youtube.com/watch?v=zf4-f8JG5MM)

```
Vi a una mujer escapar de madrugada y 
su rostro expresaba dolor 
una vez mas triunfa el desengaño una
vez mas una patada al corazón, oh run away, 
vi a un niño correr tras su madre 
escando de un ente opresor,
será otra victima de la mentira 
cuando el amor se transformo en dolor, 
oh run away.
```

## [Fun people - Faćil venir/ Easy to come](https://www.youtube.com/watch?v=KPkERyynlUs)

```
Ahora te es facil venir,
papa tu nena no tiene nada dentro de su panza 
y la gente del barrio no hablara mal de nosotros,
ya no (humm...)
Ahora te es facil venir, tu orgullo esta a salvo 
y yo tengo un agujero en mi corazon. 
Ahora te es facil venir pero, que hay de mi? 
tengo mucho frio en esta oscura pieza, 
pero no tengo ganas de salir, 
yo queria tener a mi bebe...
```

## [Fun People - Lady](https://www.youtube.com/watch?v=9j2JVIMkLv4)

```
Miel! Yo lo se, no es tan facil  Señorita! 
no estas preparada, no tienes el dinero
no estas sana para tener un bebe  
Honey, vos no querias abortar, pero no tenias opcion!!!
Te ciudaste, pero eso no funciono eso no hizo su trabajo  
Mujercita vos no sos una mala mujer por haber quedado embarazada 
y haber pensado en abortar, vos no sos la culpable vos no tenias opcion!!  
Si tuvieran en cuenta nuestros reclamos 
las cosas serian muy distinas  Lo sabias? Lo sabes?  
Miel no llores...Pelea!!   
Anticonceptivso para no abortar, aborto legal para no morir.
```

## [Fun People - Leave me alone](https://www.youtube.com/watch?v=Vb-6mHm5vsY)

```
Dejame solo,
lo comprendo todo.
Oh!, mi novia muri,
asi que por favor dejame solo.
La suma era demasiado para ambos
y decidiste hacerlo sola mujer!, no!
```

## [Fun People - Never Knows (Es Obvio)](https://www.youtube.com/watch?v=uMWFvS13BqI)

```
Me encantaba jugar siempre a ser mujer,
a pesar de lo que insististe hacerme un varón!
Mujer de noche, hombre de día,
es algo que vos, mientras dormías,
vos jamás pudiste controlarme.
Never knows (es obvio).
Yo prefería jugar siempre a ser varón,
a pesar de lo que insististe hacerme niña,
mujer de día ¿hombre a escondidas?.
¿Es lo que soy?. Sin género.
Esta es mi historia, así es mi vida.
```

## [Fun people - Esos Dias ](https://www.youtube.com/watch?v=guWmFwOk89c)

```
Los sintomas empiezan creo asi...
woman in these days se te inchan la panza y las tits, woman... 
y te duelen los ovarios tambien, woman...
en esos dias estas sensible, tan sensible!!!
I try, I try, I try to make feel, feel better I feel, 
I feel desesperado e impotente, I try, I try, I try. 
Con mi cariño y mis abrazos, crees que podre
hacerte sentir mejor? en esos dias woman in these days. 
y finalmente el dia llego woman... estas incomoda y molesta, 
woman... queres estar conmigo pero no, 
woman in these days en estos dias estas sensible, 
tan sensible a pesar de todo 
ambos lo esperamos feliz, siempre, je.
```

## [Boom Boom Kid - Jenny](https://www.youtube.com/watch?v=ss2WEfV_IJI)

```
jenny!!!
tu nooo tienes nada que explicarte amor!
jenny, deja fluir lo que tienes dentro vale mucho ozir
jenny, mi loco amor
que importa ellos, tu no te dejes caer!
(porque) lo que es sentir, no saben
lo que es amar... tu sabes bien, y lo que es verdad amor...
que lo que ellos viven, es puro cuento
```

## [Boom Boom Kid - Canción De Cuna A La Menospaucia](https://www.youtube.com/watch?v=SIQrLG3vT08)

```
I'm feel so fine
Feel so fine with me...
But I feel the world
I feel the world fall down to me

I'm feel so fine
I'm Feel allright with me
But I feel so strange when
I feel suet all over me

But I'm happy to meet you "trouble"
I'm happy to meet you "trouble"

I'm feel so fine
Feel so fine with me...
But I feel the world
I feel the world fall down to me

I'm feel so fine
I'm Feel allright with me
'Cause now I more
More free the I used to be

But I'm happy to meet you "trouble"
I'm happy to meet you "trouble"

Now I'm feel so fine
Now I'm feel so so fine with me
Now I'm feel tan bien
Now I'm feel so oh oh oh oh...

```

## [Boom Boom Kid - She runaway](https://www.youtube.com/watch?v=-pPya092Fxg)

```
She runaway,away from nothing,
scaping of his nothing town
nothing town is here.
all they here,until your boyfriend
told your are not older
you got seventeen.
she runawa,she was like fighter
she gonna be mother
a real big one.
she feel run run run
oh run run
oh run run .
```

## [Fun people - FMS (Fuck Male Supremacy)](https://www.youtube.com/watch?v=DDlZ8nLyoxI)

```
You don't understand?
You don't understand?
You don't understand?
You don't understand?

(Esto siempre fue así y va a quedar así!)

When i look in the past gigs we've got it now (again!)
Woman in the back , man in the front (again!)
Everybody goes along with it (again!)
Woman in the back , man in the front!
Fuck Male Supremacy!!!

When i look in the past gigs we've got it now (again!)
Woman in the back , man in the front (again!)
Everybody goes along with it (again!)
Woman in the back , man in the front!
Fuck Male Supremacy!!!
```

## [Boom Boom kid - Es Duro Ser Una Chica En Cualquier Lugar](https://www.youtube.com/watch?v=G1SSTKKdnQI)

```
Miles de ventanas,
miles de edificios,
pero aqui es dificil encontrar un individuo
cientos de ventanas
cientos de suenios
y en la periferia solo (hay) olor a factoria
y es en ambos casos la misma situacion
aqui nadie te regala una alegria!
se que es duro ser una chica de la gran ciudad,
duro ser una chica en la periferia
se que es duro ser una chica de la gran ciudad,
duro es ser una chica en la periferia
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica en la periferia
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica en la periferia
cientos de personas en un tren por la maniana
decenas de personas por la noche en colectivo
volver a casa
dificil situacion
en cualquier lugar si tu look es femenino
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica en la periferia
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica en la periferia
se que es duro ser una chica de la gran ciudad,
se que es duro ser una chica en la periferia
en cualqueir lugar
en la gran ciudad y en la periferia
en la gran ciudad y en la periferia
cuando cuando
ira a cambiar?
cuando?
```

## [Shaila - Eva ](https://www.youtube.com/watch?v=-qgDXWDeOvM)

```
La dieta en la costilla patriarcal
de culpas y anorexia cultural
le lee el cuento a Cenicienta del pecado original
la viste de princesa y la ayuda a vomitar

La manzana que escupía Adán desde el Talmud
que Sansón le dio a Jehová como peluquero
mientras Eva no iba al cielo

La farolera vuelve a tropezar
y traga arroz con leche porque abortar
es coacción y norma del sexismo
y la eugenesia señorial que mata en la pobreza de un hospital

Y hoy objetivan cuerpos por maquillar con esa cruz
que es violencia familiar, violación y entierro
del pecado del cordero
No hay bien ni mal sin verdad...

Betsabé, Dalila, Magdalena y más detrás
de santas, putas y el axioma al preenjuiciar
y Eva atrás, siempre atrás
-con la culpa del destierro que es tu carga y tu encierro-
paria de esta misoginia estructural
```

## [Rodia - Heteronorma](https://www.youtube.com/watch?v=Fc9Fn7im5eQ)

```
Aguafuertes de una discusión,
que en nombre de un valor cínico, “moral"
pretende hallarte tan cobarde, calmo y virginal

Y esta vez (otra vez)
se enmascara lo que es normalización en fe y creyentes
Ves, otra vez, se derrama la matriz de la exclusión,
La misma cruz de ayer, pecado, culpa, o ¿qué?

Penitencia y punición
con la fecundidad bajo inspección…
Y es tan cobarde, tan superficial
¿Siluetas de quién? Tú cuerpo es tuyo y libre!
Es eso y nada más.

Y esta vez (otra vez)
se enmascara lo que es normalización en fe y creyentes
Ves, ¿ahora ves? liberarse es construir revolución
la cruz de hoy y ayer, abrí tu mente:
Eros sin regulación,
despertares llenos de una vida por armar
con gestos sin ordenación,
avatares plenos sin amarres ni deidad
si es más natural amar sin cuestionar,
juzgar si es válido amarse…

Y ahora quién, ¿otra vez?
Se prepara la matriz de la exclusión
La cruz de hoy y ayer
otra vez Y ¿ahora quién? otra vez, y ¿ahora quién?
otra vez, y otra vez, y ¿ahora quién? Y ¿ahora quién?
```
