# 996.ICU

- Es el *Segundo repo más popular de Github*, [Repo 996.ICU](https://github.com/996icu/996.ICU)
- Web promocional [996.icu](https://996.icu/#/es_MX)
- [Sistema de trabajo 996](https://en.wikipedia.org/wiki/996_working_hour_system)

## ¿De que se trata esto?

> Siguiendo con la idea de cuidarnos más y conocer además la situación de otres en la misma que nosotros, y en línea de ayudarnos a reflexionar sobre nuestro trabajo es que parece interesante este tema.

- *996* se refiere a una forma de trabajo de 9hs a 21hs, 6 días a la semana que está ganando más popularidad al menos en China.
- *ICU* se refiere a la terapia intensiva de los hospitales

## Dilemas

Este tipo de cuestiones también se piensan en función de como modela la libertad y si habría o no que tomarlo en cuenta por ejemplo en la libertad de las personas sobre el software. ¿Los derechos laborales dentro de las licencias de software?, otro ejemplo de esto es ¿Licencias que restringe usos particulares del software?

Este proyecto creo la licencia *Anti-996 License*, su proposito es que las compañias que no cumplen con determinadas leyes laborales puedan utilizar  el software que tenga esta licencia, además que puedan forzar a estas mismas a modificar su forma de trabajo.

> [Texto de la licencia](https://github.com/996icu/996.ICU/blob/master/LICENSE)

En contraposición a estos argumentos con respecto a la licencias, la FSF la considera no libre, y acá están los argumentos que esgrimen sobre [las restricciones a la libertad sobre el uso de los programas para cualquier propósito](https://www.gnu.org/philosophy/programs-must-not-limit-freedom-to-run.html)

