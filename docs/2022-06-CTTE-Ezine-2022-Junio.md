---
title: Junio 2022
---

# Cambá CTTE Ezine Junio 2022

<div style="text-align: center;">

![Diente](@img/dientefinal.png)

</div>

```
Andar en bici
con la calle hecha hielo
la niebla, y todo blanco;
el frío en las calles
de tierra, lo hermoso
y puro de la vida /
el frío, un éxtasis frío /
y el fuerpo caliente
de pedalear la bici
como si fuera eso
abrir mis alas /
y el cuerpo caliente
de la vida misma
que me enciende
y me invita al placer,
en cada matriz
en cada cambio /
en cambio,
en cada segundo
que elijo andar la bici,
o parar la bici
en medio de
la calle congelada /
para escribir
sobre el hielo
sobre el éxtasis frío /
y las manos congelándose
de frío y de amor
por tener
tantas ganas
de escribir.
```

Poema "El éxtasis es frío" de [Sofia Slobo Parisi](https://sofiasloboparisi.com/) en libro [Poesía Menstrual](https://tiendanatural.empretienda.com.ar/libros-y-publicaciones/poesia-menstrual).


## Herramientas de software

- [Ranger](https://ranger.github.io/) - Manejador de archivos desde la terminal con teclas de VI
- [Metrics](https://github.com/lowlighter/metrics) - Generador de infografias con soporte a muchos plugins para mostrar estadisticas de tu cuenta de Github.
- [Eva](https://eva.js.org/) - Motor de juegos frontend para crear juegos interactivos
- [Grav](https://getgrav.org/) - CMS moderno en archivos estático
- [daesalOs](https://github.com/DustinBrett/daedalOS) - Ambiente de escritorio en el navegador

## Informes/Encuestas/Lanzamientos/Capacitaciones

- Tutoriales sobre dapps y contratos inteligentes: [Ethernaut](https://ethernaut.openzeppelin.com/) y  [cryptozombies](https://cryptozombies.io/)
- [Tutorial para programar con Javascript](https://javascript.info/)
- [Ejercicios para acostumbrarse a Erlang](https://github.com/lambdaclass/erlings)
- [¿Qué hace el algoritmo de hash sha256?](https://sha256algorithm.com/)
- Proyectos de software libre: [Militarización](https://beny23.github.io/posts/on_weaponisation_of_open_source/) y [Sabotaje](https://snyk.io/blog/peacenotwar-malicious-npm-node-ipc-package-vulnerability/)

## Noticias, Enlaces y Textos

### Sociales/filosóficas

-  [Relevamiento de Claudio Alvarez Teran sobre el libro "INFOCRACIA. La digitalización y la crisis de la democracia", de Byung-Chul Han.](https://www.youtube.com/watch?v=vgsf2ZUVf4A)
- Lecturas cruzadas de Íñigo Errejón y Amador Fernández-Savater en conversacion sobre sus libros "Con todo" y "La fuerza de los débiles". [Video](https://www.youtube.com/watch?v=I_Fh68oonaY)
- HISTORIA DE LA SEXUALIDAD DE MICHEL FOUCAULT | Darío Sztajnszrajber es #DemasiadoHumano -Ep.07 T7 - [Video](https://www.youtube.com/watch?v=hkQBtJnK2-0) y [Resumen](CTTE-texto-foucault.md)
- [Resumen sobre libro "No cosas - Byun-chul-han"](CTTE-texto-nocosas.md)
- [Immoral Code - Sobre para a los robots que matan](https://www.youtube.com/watch?v=xUU8YHa_Cjg)
- [Qué es la economía popular? Experiencias, voces y debates](CTTE-texto-economia-popular-libro.md)


### Tecnología

- [Herramientas para Formato de imagenes avif](https://css-tricks.com/useful-tools-for-creating-avif-images/)
- [Scraping con Chome DevTools heap snapshots](https://www.adriancooney.ie/blog/web-scraping-via-javascript-heap-snapshots) - [Repo](https://github.com/adriancooney/puppeteer-heap-snapshot)
- [Comparación de performance ionic vs react native, segun ionic](https://ionicframework.com/blog/ionic-vs-react-native-performance-comparison/)
- [Creación de shield para Arduino con kickad](https://electronlinux.wordpress.com/2022/05/03/flisol-shield-arduino/)
