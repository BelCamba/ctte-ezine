# [El hombre rebelde](https://es.wikipedia.org/wiki/El_hombre_rebelde) - [Albert Camus](https://es.wikipedia.org/wiki/Albert_Camus)

> Estos son extractos de un libro que es un tratado casi filosófico sobre la rebeldía. Ya tiene casi 70 años de escrito y sigue siendo importante para las lecturas actuales sobre la revolución y la posibildad de emancipació. Si les interesa el tema, se puede profundizar leyendo [el libro completo](https://www.derechopenalenlared.com/libros/Albert_Camus_el_hombre_rebelde.pdf)

**¿Qué es un hombre rebelde?**
Un hombre que dice que no. Pero si se niega, no renuncia: es además un hombre que deci que sí desde su primer movimiento.

## El rechazo de la salvación

Toda la ciencia del mundo no vale las lágrimas de los niños. [..] No existe la verdad. Dice que si existe la verdad, soló puede ser inaceptables. ¿Por qué? Porque es injusta. La lucha de la justicia contra la verdad [..] ya nunca tendrá tregua.


## El único

Resumir

## Nietzsche y el nihilismo

"Negamos a Dios, negamos la responsabilidad de Dios; solamente así liberaremos el mundo".

¿Se puede vivir sin creer en nada? Su respuesta es sí

Si el nihilismo es la impotencia para creer, su sintoma más grave no se encuentra en el ateísmo, sino en la impotencia para creer lo que es, para ver lo que se hace, para vivir lo que se ofrece. Esta enfermedad está en la base de todo idealismo.

Si nada es cierto, si el mundo carece de regla, nada está prohibido; para prohibir una acción se necesita, en efecto, un valor y una finalidad. Al mismo tiempo, nada esta autorizado; se necesitan también un valor y una finalidad para elegir otra acción.

No hay libertad sino en un mundo en lo que es posible se halla definido al mismo tiempo que lo que no lo es.

Allí donde nadie puede decir ya qué es negro y qué es blanco, la luz se extingue y la libertad se convierte en una prisión voluntaria.

Ser libre es, justamente, abolir los fines. La inocencia del devenir, desde el momento que se la admite, simnliza el máximo de libertad. El espíritu libre ama lo que es necesario.

## Nihilismo e Historia

La inserrección humana, en sus formas elevadas y trágicas, no es ni puede ser sino una larga protesta contra la muerte.
Existen, al parecer, los rebeldes que quieren morir y los que quieren hacer morir.
La libertad absoluta se convierte por fin en una prisión de deberes absolutos, una ascesis colectiva, una historia hay que terminar.


## La profecia revolucionaria y el fracaso

 En una sociedad sin angustia es fácil ignorar la muerte. Sin embargo, y ésta es la verdadera condena de nuestra sociedad, la angustia de la muerte es un lujo que afecta mucho más al ocioso que al trabajador, asfixiado por su propia tarea.

Al exigir para el trabajador la verdadera riqueza, que no es la del dinero, sino la del ocio o de la creación, ha reclamado, a pesar de las apariencias, la calidad del hombre.

Un fin que necesita medios injustos no es un fin justo.

Económicamente, el capitalismo es opresor por el fenómeno de la acumulación. Precisemos que la productividad no es dañina sino cuando tomada como un fin, no como un medio que podría ser liberador.


## El asesinato nihilista

Si la injusticia es mala para el rebelde, no lo es porque contradiga una idea eterna de la justicia que no sabemos dónde situar, sino porque perpetúa la muda hostilidad que separa al opresor del oprimido.

La libertad más extrema, la de matar, no es compatible con las razones de la rebelión. La rebelión no es modo alguno una reclamación de libertad total.

Reclama para todos la libertad que reclama para sí mismo; y prohibe a todos la que rechaza.

Si hay rebelión es porque la mentira, la injusticia y la violencia constituyen en parte la condición del rebelde.

## El asesinato histórico

Si, en efecto, ignorar la historia equivale a negar lo real, es también alejarse de lo real considerar la historia como un todo que se basta a si mismo.

En consecuencia, hay que vivir de acuerdo con la eficacia inmediata, y callarse o mentir. La violencia sistemática o el silencio impuesto, el cálculo o la mentira concertada se convierten en reglas inevitables. Un pensamiento puramente histórico es, por lo tanto, nihilista: acepta totalmente el mal de la historia y se opone en esto a la rebelión.

La historia como un todo, no podría existir sino para los ojos de un observador exterior a ella mismo y al mundo. Es imposible obrarde acuerdo con planes que abarquen la totalidad de la historia universal.

## Mesura y desmesura

La revolución sin más límites que la eficacia histórica significa la servidumbre sin límites.

El camión conducido a lo largo de los días y las noches por su conductor no humilla a éste, que lo conoce enteramente y lo utiliza con amor y eficacia. La verdadera e inhumana desmesura está en la división del trabajo.

El valor más moral puesto de manifiesto por la rebelión, finalmente, no está por encima de la vida y la historia que lo que la vida y la historia están por encima de él. En verdad, sólo adquiere realidad en la historia cuando un hombre da su vida por él, o se la consagra.

## Más allá del nihilismo

Pero la sociedad y la política sólo se encargan de arreglar los asuntos de todos para que cada uno disponga de tiempo y libertad para realizar esa búsqueda comṕun. La historia no puede ser erigida, por tanto, en objeto de culto.

En su mayor esfuerzo, el hombre no puede sino proponerse la disminisión aritmética del dolor del mundo. Pero la injusticia y el sufrimiento subsistirán y, por mucho que se los límite, no dejarán de escandalizar.

Su honor consiste en no calcular nada y distribuir todo enla vida presente a sus hermanos vivientes. La verdadera generosidad con el porvenir consiste en dar todo lo presente.
