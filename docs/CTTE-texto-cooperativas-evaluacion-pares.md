# Evaluación de pares en Cooperativas de trabajo

> Traducción al español de texto visto en [Cooperantics](http://www.cooperantics.coop/2019/04/08/peer-appraisal-in-worker-co-ops/)
> Escrito por [Kate Whittle](http://go-op.coop/people/bios/), con algunas cosas de Nathan Brown y Britta Werner ([Unicorn Co-operative Grocery](https://www.unicorn-grocery.coop/)) Abril 2019

## Texto

*¿Como le digo a mi compañere que trabaja como la mierda?” (Pista: No lo haces)*

Muchas lunas atrás, en una conferencia de cooperativas, alguien me pregunto: ¿Cómo le digo a mi compañere que trabaja como la mierda?
Pensé que era una buena pregunta, pero no tenia la menor idea de como contestarla, a excepción quizás de que, como también actualmente creo, no hay que decirle a tu compañere de coopeartiva de trabajo que trabaja como la mierda.

La úlitma cosa que queremos es echar a alguien. Pero necesitamos una forma de proveer ayuda a les asocieades y formas de que todos alcancen la calidad, tiempos y compromisos de nuestra misión y objetivos.

Las evaluaciones proveen a les asociades ayuda y a la vez una estructura para mantener la responsabilidad. Cualquier tipo de negocio con empleados (o voluntarios) necesita poder llevar a cabo de manera regular evaluaciones. Lo que nos interesa es como se hace, porque en una cooperativa de trabajo tenes una organización achatada y más demócratica. Además podes encontrarte que todos los empleados son directores, y con diferentes tipos de estructura (reuniones de gestión o un comite de gestión, que tienen poderes delegados o que son representativos de diferentes equipos o departamentos). También hay una cantidad cada vez mayor de cooperativas de trabajo que adoptan herramientas y estructuras sociocráticas. Por eso no buscamos una solución que resuelva los problemas para todas.

En una empresa privada para un empleador vale la pena realizar evaluación de empleados para que tengan la confianza de que están trabajando bien, y además, el empleador sabe las personas que necesitan entrenamientos o necesitan un nuevo software o equipamiento o cuando se necesita reclutar más gente o hacer que los empleados actuales trabajen más.

En un espacio de trabajo horizontal manejando democraticamente, es igualmente importante tener esta información, y hay una variedad de formas de como hacerlo. No se necesita una estructura jerárquica con un gerente responsable de mantener el ejercicio de evaluaciones con sus empleados, hay otros enfoques y eso es lo que nos proponemos explorar en este texto.

#### ¿Por qué hacerlas?

- Dar seguridad a las personas - Saben que están haciendo bien las cosas, en el lugar adecuado y en el tiempo justo. Saben cuando están haciendo un buen trabajo. Hay oportunidad para el elogio.
- Ayuda que las personas se sientan valorizadas
- Se puede identificar cuando se están por debajo de lo esperado o luchando para lograrlo
- El equipo sabe cuando las personas necesitan capacitación o necesitan una nueva app o software.
- Se informa a la cooperativa que hay una necesidad de ir a buscar más gente para trabajar o decir a los actuales asociades que hay que trabajar más tiempo. O que algún rol de trabajo necesita ser modificado o divido en más personas.
- Permite alinear las expectativas con la realidad
- Da un plan de carrera
- Provee información importante para cada individuo - Puede ser la base para el desarrollo de un plan personal

#### ¿Qué pasa si no las haces?

- Costos para el negocio: *hiring & hiring* (copyright B. Cannell); incrementa la rotación de personal; incrementa el costo de inducción a nuevas personas
- Costo para la moralidad de las personas: "Si alguien puede zafar de algo, por que yo no puedo?"
- Generación de frustración si no hay una devolución de los demás
- Poco feedback incentiva negativamente el crecimiento y el desarrollo de sus habilidades
- La personas se abstiene a tomar acciones, lo que lleva a quejas formales
- Una perdida de compromiso causa a largo plaza insatisfacción
- Costo para la cultura cooperativa: Les nueves socies van a ver las contradicciones entre lo que se dice y lo que se hace, y puede debilitar la misión de las cooperativa


### Modelos y ejemplos

### Ejemplo de modelo de evaluación de Co-operantics

#### Evaluaciones centradas en las personas

1) Mira tu rol. Identifica los puntos claves de él y las habilidades involucradas. Trata de llegar a la escencia de lo que tu trabajo realmente es.

2) Hacer una revisión reflexiva preliminar usando puntajes.

3) Para cada aspecto de tu rol, puntúa tu competencia y confianza usando un puntaje del 1 al 5 o bajo/medio/alto

4) Luego hacete preguntas abiertas tales como:
  - ¿Te sentis sub utilizado/con tiempo libre o sobre utilizado/sobre cargado de trabajo?
  - ¿Encontras partes de tu trabajo en donde encontras dificultad? ¿Por qué?
  - ¿Encontras partes de tu trabajo que especialmente disfrutas? ¿Por qué?
  - ¿Qué cosas te gustaría estar haciendo más y cuales menos?
  - ¿Identificas en que necesitas más capacitación?
  - ¿Cuales son los niveles generales de felicidad como persona trabajadora?

5) Una vez que se realizo la revisión personal, hay que trabajar con un par que su propósito sea desafiar y ayude a clarificar tus respuestas. Esta puede ser la misma personas para todes o diferentes para cada una.

6) Identificar mejoras personales, requerimientos de capacitación y metas (puede ser usado para crear un plan de desarrollo personal).

7) Cuando cada persona en la cooperativa a pasado por este proceso tenes una planilla completa con puntajes que informan el desarrollo individual de la perona, el plan de capacitación para el año entrante, cambios en la forma que se asignan los roles, etc.

*Si está última parte la lleva a cabo una personas con ese rol o lo hace un sub comite depende de la cultura y la forma en que te organizas.*

#### Evaluación de competencias cooperativa

1) La cooperativa separa la competencia para hacer el trabajo de la competencia para ser asociade. Se escribe una lista estandar de competencias para cada asociade y cada cual necesita completar para ser un asociade activo de la cooperativa. ( A veces se llama a esto Descripción de trabajo de asociade ).

2) Se hace una reunión de seguimiento a les asociades y aspirantes (individual o sub comité lo lleva a cabo). Cuando las competencias no puede demostrarse, debería resultar en planes de capacitación individuales y cooperativos, en línea con el quinto principio de capacitación, educación e información.

Este enfoque puede ser usado para medir el progreso a través del proceso de aspirantes a socies.

**Nota:** Una evaluación de membresia/cooperativa debería ser usada con otro tipo de proceso de evaluaciones para las competencias de trabajo. Ser una gran persona democrática no asegura el éxito del negocio sin atributos o habilidades comerciales.


#### Sistema de acompañamiento

- Cada persona tiene une acompañante, que su rol es ayudarlos, como si fuera un tradicional personal de recursos humanos.
- De manera similar al enfoque anterior, la persona hace una revisión en una planilla. La persona que mentorea discute esta evaluación
- Se presenta la evaluación y se hacen recomendaciones a la cooperativa o comite de gestión o al sub comite sin la presencia de la persona.
- Las acciones surgidas se desarrollan y acuerdan con la cooperativa y la persona involucrada para medir progreso a futuro.

*Este proceso pone más responsabilidad de decisión en la cooperativa que en la persona*

#### KPIs/Objetivos personales

- A Cada persona se le asignan indicadores de rendimiento relacionados con el rol de trabajo y cualquier otra responsabilidad que pueda contraer para la cooperativa.
- Por ejemplo cada 3 meses se hace una reunión para evaluar el rendimiento contra los KPIs y discutir los temas relacionados.

*Los sistemas basados en KPI funcionan bien en los negocios que pueden predecir el rendimiento y el resultado del trabajo. Hay un peligro en usar estos sistemas y convertir las reuniones en focalizar el los objetivos y no en los temas que están latentes que impactan en la relaciones interpersonales o en la carencia de habilidades hasta que empiezan a impactar en los objetivos. En ese momento ya es demasiado tarde y el daño ya fue causado.*

#### Revisión de equipo

El equipo regularmente usa tiempo en cuadros o otra ayuda visual para identificar problemas y aciertos en cuanto al negocio, y alientan a cada persona a identificar sus fuerzas, debilidades y como contribuyen con el rendimiento de la cooperativa.

Está aproximación, que aparentemente es democrática y cohesiva, en realidad tiene un riesgo grande de perder de vista los problemas que los individuos quieren resolver. Hay un riesgo de desarrollar *groupthink* y que recaiga más en la persona correcta identificar que el emperador esta de hecho no usando ropa.

*Groupthink* ocurre cuando un grupo de personas dice lo que se espera que digan, y lo que la mayoría parece apoyar. Quizás porque no quieren molestar a otros, o cortar la inspiración ( se siente que es más importante estar de acuerdo con tus pares que lo que pensas). Quiere decir que las voces individuales no son escuchadas, los problemas quedan ocultos, los desafios críticos se pierden m las decisiones quedan reducidas al mínimo común denominador y el acercamiento del grupo o rendimiento frecuentemente es mediocre o por debajo del estandar.

### Enfoque enfocado en resultados

#### Proceso Evaluación personal

**¿Qué?**

- para mejorar nuestro trabajo
- par mejorar nuestras relaciones de trabajo
- para hacer lo que decimos
- para crear un espacio abierto para discusiones políticas
- para crear un ambiente feliz
- para aprender como apreciar a los otro y al colectivo
- para crear tiempo para la critica constructiva
- para mejorar la transparencia y las comunicaciones

**¿Cómo?**

- Sistema de acompañamiento
- Todo se sienten seguros y ayudados
- Abierto, franco, transparente, seguro
- Sistema de Feedback para todo el grupo
- Ejercicio de escucha
- Basado en la descripción del trabajo o rol

#### Jornada de revisión Anual

- Los objetivos de la cooperativa se salen desde el plan de negocios anual que se construye en la reuniones de miembros y revisado mensualmente y más profundamente cada 3 meses.
- Los roles se acuerdan y revisan anualmente en función del plan de negocios
- Las descripciones de tareas de cada rol deben ser incluidas y explicitadas de la manera más simple posible exponiendo en las funciones que se realizan.
- Combinando a los asociades (member/owner/director) y su rol (manager/worker/employee) construimos la descripción general de descripciones de trabajo.
- El objetivo de lo anterior no es dar un devolución tecnocrática sobre si la persona alcanzo o falló en como se desenvolvio, pero si dar una devolución holistica en relación a su como se desenvolvio en función de los objetivos estratégicos de la cooperativa.


#### Escucha de grupo (Medio día mensualmente)

- Las reuniones se llamarán 'escucha de grupo' en vez ‘sesiones de devolución’, con el objetivo de ser a la vez motivador de comentarios y focalizar la escucha en lo que se está diciendo más que en discutir si acordas o no acordas con lo que se dice.
- Dos personas serán las que reciban la devolución del grupo por cada reunión. Se decidirá anticipadamente quienes serán y lo harán durante 6 meses), se inlcuira a todas las personas en este rol. Esto continua por lo que cada uno esperará y recibirá una devolución cada 6 meses (con los ajustes necesarios por vacaciones, ausencias, nuevos asociades, etc). Cada uno usará un ‘Stop – Start – Continue’ ofreciendo comentarios constructivos.
- Todes, incluyendo los que reciben los comentarios, deben escuchar atentamente y sin interrumpir, mientras hacen una escucha respetuosa lo que cada un expresa
- Los que reciben la devolución tiene la opción de ofrecer una respuesta al final de la reunión.
- Además el grupo debe ofrecer una respuesta al comienzo dela reunión del próximo mes.


#### Sistema de acompañamiento (1 hora mensualmente)

- A Cada persona se le da un acompañante de manera aleatoria y será así durante 6 meses.
- Cada pareja se reúne 1 hora por mes
- Cada uno habla por turnos para decir y focalizar en:
  - La última Escucha de grupo, más allá de si es o no del grupo al que le hicieron la devolución.
  - Charlas par focalizar sobre cualquier lección aprendida, pensamientos, reflexiones en como impacta las practicas de trabajo personales, etc. - No deberian incluir comentarios acerca de si la devolución fue justa o injusta.
  - Preocupaciones generales o comentarios sobre el compañero.
  - En ambos casos cuando se escucha no se debe dar consejos a no ser que explicitamente se pida, tampoco debería contradecirse los sentimientos del otre (ej: ‘No deberías estresarte por eso’). En realidad el foco debe ser puesto en la escucha y ofrece preguntas para guiar o profundizar lo explayado (ej; ‘Qué es entonces exactamente lo que te enojo sobre eso?’; ‘Cual podría ser una forma constructiva de tratarlo / o ayudarte a resolverlo / encontrar ayuda del colectivo?’) y ponerlo en perspectiva de proceso colectivo.

### Proceso de evaluación/revisión del personal (basado en el sistema de acompañamiento)

#### Guía para la persona evaluada

- Cuando completas el formulario de auto evaluación pensa sobre tu rol y tu lista de tareas, y lo que lograste durante los últimos años.
- Si sentís que deberías sumar cosas a tu lista, por usar este proceso para proponer que sumarias (con anterioridad a la reunión con tu acompañante)
- Para cada una de las 10 categorías anteriores comenta Cuan bien pensas estas trabajando y trata de explicar las respuestas del año anterior.
- Luego de discutir tus pensamientos con tu acompañante, este formulación de evaluación debería compartirse con el resto de la cooperativa para otros comentarios. Tu compañero te traera los comentarios a vos.
- Luego de discutir la devolución del resto de la cooperativa con tu acompañante, resumí lo puntos centrales de la discusión y los lineamientos de acción acordados.
- El progreso sobre estas lineas de acción deben ser revisados por tu acompañante cada 3 meses.

#### Guía para le acompañante

- El rol del acompañante es escuchar hacer preguntas, clarificar para dar cuenta a la cooperativa y a la persona evaluada.
- Durante la auto evaluación, asegurarte de entender todos los comentarios. Clarifica los comentarios vagos y trata de delinear las acción relevantes donde sea relevante. (les acompañantes deben dar la devolución a la cooperativa y comprender bien cual es la devolución de vuelta)
- Ver la lista de las tareas más recientes y discutí los potenciales cambio o actualizaciones. Estas será evaluadas y aprobadas en la devolución desde la cooperativa.
- Una vez que se complete la auto evaluación, se complete los formularios de revisión deben ser chequeados y aprobados por le evaluado y enviado a todos les socies de la cooperativa.

#### Encabezados/Títulos

- Habilidades y áreas de trabajo
- Acciones futuras como las decidió el trabajador
- Mirada de los cooperativistas como ellos lo hicieron
- Comentarios del resto de la cooperativa (acuerdos/ desacuerdo)
- Acciones a futuro como las decidieron el trabajador y su acompañante

#### Categorías

- Habilidad para trabajar cooperativamente (incluyendo la voluntad de ayudar en las tareas del día/día que hacen otras personas)
- Contribuye activamente en las reuniones y discusiones en grupo (incluyendo los sub grupos)
- Habilidad para comunicarse efectivamente (incluyendo dar y recibir devoluciones)
- Habilidad para manejar la carga de trabajo y llegar a las metas que nos proponemos (deadlines)
- Calidad del trabajo
- Habilidad de desempeñarte en tu rol de la mejor manera posible (revisar lista de tareas)
- Busca y acepta responsabilidad
- Busca y se compromete en el desarrollo personal, las capacitaciones y aprende de sus errores.
- Tus futuras aspiraciones dentro de la cooperativa
- Necesidad de capacitación
- Futuras aspiraciones para la cooperativa (ej: nuevos productos o servicios, campañas, actividades, desarrollos, investigación, etc)
- ¿Algún otro comentario?


*Esta cooperativa últimamente ha hecho el proceso de revisión del personal y la  gran mayoría de socies piensa que ha funcionado de manera efectiva en función de las auto evaluaciones, discusión con los acompañantes y la devolución de la cooperativa. De todas formas había menos acuerdo en función de las futuras acciones decididas*

### Evaluación de socies (con la ayuda del equipo del personas y su análisis)

La evaluación de socies se hace cada 18 meses. Todas las devoluciones de las evaluaciones (aspirante o socies deben tener completas al menos el 75% de todas las evaluaciones). Si no se completan repetidamente el equipo informara a la asamblea.

#### Formulario de Evaluación

Pensas que X cumple su rol:

- Satisfactoriamente
- No Satisfactoriamente
- Yo no trabajo con X

Lee cuidadosamente la descripción del trabajo/rol y comenta en los puntos de evaluación en que el evaluado hace su trabajo bien y por que:

- Pensas que el evaluado puede mejorar con alguna capacitación o desarrollo personal?
- Si trabajas con X en un equipo, podes especificamente comentar sobre su trabajo (velocidad, efectivo, responsabilidad de equipo, área de mejora
)
- Cualquier otro comentario


Previamente las personas envian esta devolución de manera anónima y hacen su auto evaluación antes de una reunión con Recursos humanos y gente del equipo de capacitación basada en el siguiente cuestionario:

- Equipos: Cual es tu rol, como contribuis a los equipos?
- Sentis que necesitas más/menos trabajo?
  - Qué particularmente disfrutas de hacer?
  - Qué algo que particularmente no disfrutas?
- Alguna habilidad tuya que deberiamos estar usando?
- Algunas capacitación de la cual puedas beneficiarte?
- Preguntas para el equipo de capacitación
  - capacitaciones recibidas
  - capacitaciones que necesitas
  - Visitas
  - Compartir habilidades
  - Presupuesto para capacitaciones individual

*Esta cooperativa es en proceso de evaluación. Recursos humanos esta diseñado el cuestionario para ver que personas quieren salirse del sistema de evaluación*

## Algunas conclusiones y como continuar


Entonces, como le decis a tu compañero que trabaja para la mierda. Claramente no se lo decis. Pero les trabajadores de cooperativa necesitamos un procedimiento para medir los logros y metas. No para sancionar si no se llega a las metas, pero si para identificar si no acordamos bien que hacer o si alcanzamos los estandares esperados, si para evaluar si:

- metas/estandares son alcanzables
- los roles, descripciones de trabajo necesita re diseñarse
- Necesitas más trabajadorxs
- Los socies necesita capacitaciones o re entrenamiento
- Necesitamos adicional software de control financiero, operacional, marketing, etc
- Necesitas hacer una capacitación de inducción
- Necesitas revisar el plan de negocios o hasta su misión

Ojala esta evaluación te haya dado idea de los diferentes formatos que funcionan en otras cooperativas, y que no hay un método que sirva para todes. Esta forma de diseñar las evaluaciones depende de la cultura de la cooperativa, el sector, la estructura organizacional, su edad y su tamaño entre otras cosas.

Igualmente para cualquier formato que uses, tene en cuenta que este no es un ejercicio de política. Es una forma de conocer las devoluciones sobre rendimiento de les socies. Es una forma de seguir el camino ayudando a las cooperativas, respondiendo a los nuevos desafios y alcanzar su misión.

Además no importa el sistema, procedimiento, reglas, o proceso que uses si no tenes un cultura fuerte cooperativa, compartir agenda, etc. Como dijo *Siôn Whellens*: **'La cultura se come a la gobernanza/gobierno de desayuno'**


Entonces, ¿Cómo seguimos? Algunos último pensamientos. Antes de implementar un proceso de evaluación de partes:

- Trabaja en lo que crean correcto en tu cooperativa, identifiquen donde están los puntos complicados y sean claros en lo que están haciendo, cuales son los problemas que tratan de resolver y como miden el progreso.
- Empeza de a poco, si hay un equipo para preparar o empezar a hacer un piloto con la estrategia seleccionada
- Prueben y tengan un mira retrospectiva de lo que paso: ¿Qué funciono? ¿Qué no? ¿Qué podemos hacer mejor la próxima vez? Sigan retocando y ajustando hasta que lo hagan bien.
- Luego la próxima pregunta es ¿Cómo escalarlo a toda la cooperativa? ¿Podemos usar el equipo que lo adopto para incentivar a los otros?

Esperamos poder agregar a este trabajo y otros enfoques que cooperativas de trabajo que hayan intentado, y queremos escuchar de ustedes cualquier otro comentario que tengan.

