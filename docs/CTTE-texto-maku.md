# La creatividad

## Prólogo

La [payasa Argentina Maku Fanchulini (ex Jarrak)](https://www.facebook.com/makufanchulini/) nos presenta un video con cinco partes sobre **la creatividad** del dramaturgo y director Argentino [Mauricio Kartun](https://es.wikipedia.org/wiki/Mauricio_Kartun).

Este video fue grabado integramente durante las última [Convención argentina de payasxs, cirquerxs y artistas de calle](http://convencionargentina.com/wp/) hecha en octubre del 2018.
Acá un video general y una foto de la fiesta final **el tortazo** en donde cientos de personas en las calles de Monte Grande con platos en mano con espuma/crema para afeitar se lo refriegan en la cara unes a otres.

[Video](https://www.youtube.com/watch?v=hnCvsx42x4Q) y donde están @gabriel @josx luego del tortazo.
![Acá Foto](@img/tortazo.png)

## Videos

- [Parte 1](https://www.youtube.com/watch?v=OPPOO6iP6WE) 18'
- [Parte 2](https://www.youtube.com/watch?v=1pMb6hLwr9M) 12'
- [Parte 3](https://www.youtube.com/watch?v=C0fy-yDbVjQ) 21'
- [Parte 4](https://www.youtube.com/watch?v=z7-p2xRGaRw) 9'
- [Parte 5](https://www.youtube.com/watch?v=mHfQJ81cwY0) 27'

## Resumen

> Podes ser una de las primeras 50 personas que descubre estos videos

### Video parte 1

- Mecanismos de creación con valor analógicos, ¿Cómo aparecen las ideas?
- Primera idea: lo mágico, la inspiración
- En contraposición, los mecanismos de creación son convencionales
- Pone de ejemplo una obra de su autoría [Terrenal, pequeño misterio ácrata](http://www.alternativateatral.com/obra32723-terrenal)
- Estado profano (hablamos, lógico) vs Estado Sagrado (amamos, conmovemos, entusiasmo)
- Entusiasmado (En tu dios), estado de exaltación
- El humor trabaja con los mismos presupuestos que la poesia
- La creatividad es la capacidad de relacionar dos elementos antes no relacionados
- Dejamos de ser creativos cuando nos incorporamos a la red conceptual, fluir en sintonía, pero no te permite pensar.
- El Cerebro humano incorpora conocimiento, por primera vez de manera muy precaria, la velocidad del camino de la sinapsis.
- Parsimonia: Optimiza recursos y reemplaza, queda grabado
- Luego ya no pensas, fluis, sos de la cosa
- La red conceptual te hace hacer algo y no estas pensando

### Video parte 2

- La creactividad vive en la ciencia, el arte y el humor
- El humor esta mal visto, porque el humano no sabe manejar la risa (no se sabe para que sirve)
- La risa es una orgía privada, la creación de un espacio sagrado personal momentaneo de identificación de creatividad.
- Sorpresa, ser presa del creador.
- El humor esta mal visto, porque no se puede hacer mientras producis (merma de la productividad)
- El humor es condena de domingo
- Pensar la vida es reirse, pasarla bien, porque la risa es celebración de la creatividad
- Mecanismos caretas de la infelicidad de la red conceptual
- Mecanismos por los que accedemos al estado de creatividad, el problema es la red conceptual
- La red conceptual tiene sistemas de analisis naturales lógicos
- Los 2 grandes mecanismos de la red conceptual son asocio y disocio
- En la busqueda de la elipsis (estallo la red conceptual)

### Video parte 3

- Los creadores no asociamos, ni disasociamos, sino **bisociamos**
- Relaciones entre elementos no relaciodos, ejemplos de capusotto
- Procediminto de bisociación
- Cuenta ejemplo de bisociación al que jugaba con su padre
- Crear en tiempo real a partir de la respuesta del otro
- Cadena de asociación y disociaciones de cada elemento, y luego se relaciona a partir de esas asociación y/o disociaciones
- Ponerle el cuerpo a lo que no tiene lógica
- Interacción con el público ejerciendo la bisociación

### Video parte 4

- La creación es sorpresa, asombrarse del ingenio
- Campos: Poesía -> metáfora, ciencia -> descubrimiento, crean humor (pero no es serio)
- La única posibilidad ante el sistema es demostrarle que no puede vivir sin eso
- A la industria, producción y al sistema es aquello que obliga a trabajar, obliga a invertir, obliga a producir
- Lo nuestro es gratis
- Rutina, viene de ruta que se repitío muchas veces
- Ruta , viene de rota (crear nuevos caminos), acto de via rupta
- El gran desafio es la creación de lo nuevo (romper algo viejo)
- Ataquemos al cerebro por donde lo tiene camino

### Video parte 5

- No se puede leer a Nietzsche sin reirse
- Tratar de seguir para llegar al punto de la epifanía, ahora entiendo algo
- ¿Como manejo un desbloqueo?
  - Aceptar lo sagrado y mínimizar lo profano
  - Aceptar hipotesis del juego (energía de la interlocución)
  - Presencias imaginarias virtuales (escribo para marx en la tercera fila)
  - Determina una energía y un código
  - Otra cosa que puede ayudar **es el cruce**
- Estamos perdiendo tiempo libre, que es con lo que nosotros trabajamos
  - Con lo virtual nuestro tiempo a sido colonizado
  - La única posibilidad es tiempo liberado, hay que expropiarlo a lo virtual
  - Salgo a caminar para recuperar un tiempo hodológico (natural, con la tierra, etc)
  - Crear espacios sin invación, el templo del tiempo sagrado
- El non plus ultra es el cuerpo, inteligencia corporal
  - Crea por talento mimética sobre una ausencia que crea una presencia
- ¿Comó ser un artista mayor?
  - Las ganas de romperle el culo al espectaculo. Hago lo mejor que puedo hacer.
  - Creatividad y originalidad
