Conferencia CRJ
===

### Temas a debatir

- Soberanía tecnológica
- Economía de plataforma
- Trabajo y tecnología
- Sociedad, algoritmos..
- Herramientas de seguridad

---
En esta sección invitamos a pensar sobre el horizonte tecnológico que estamos proyectando, para aplicarle un juicio crítico desde nuestras organizaciones y construir en red una visón tecnopolítica sobre la innovación tecnológica.

---

## Soberanía tecnológica


*Quiénes tienen poder de desición sobre el desarrollo, uso, acceso y distribución de las tecnologías?*

La inversión de recursos en el desarrollo de conocimientos orientados directamente a la producción o desarrollo de nuevos procesos y productos, convierte a la tecnología actual que consumimos, en un bien comercial. Su adquisición, transmisión y transferencia deja de ser un proceso informal del procomún, convirtiéndose en uno formal, sometido a las leyes e intereses del mercado, las patentes y los registros de propiedad intelectual. Todo ello se desarrolla mayoritariamente en grandes empresas, corporaciones, estados y gobiernos; y sus frutos son una mecanización excesiva, que promueve el desplazamiento humano obligatorio, desbasta los recursos e incrementa el absoluto desapoderamiento y conocimiento social sobre las tecnologías por quien las está usando.

La relación con la tecnología es paradójica. Te permite hacer más cosas (autonomía), pero dependes de ella (dependencia).
Dependemos de quienes la desarrollan y distribuyen, de sus planes de negocio o de sus contribuciones al valor social. Y cambiamos con ella. ¿No está cambiado Whatsapp o Telegram la cultura relacional? ¿No está cambiando Wikipedia la cultura enciclopédica? Y también la cambiamos a ella.

La situación de dependencia y desigualdad en el desarrollo se observa cuando la fuente principal de tecnología de un país se ubica en el exterior, y cuando no se dispone de una capacidad local para generar y adaptar tecnologías propias.

La introducción de una tecnología inadecuada, que no se comprende, en una comunidad o su adopción por un individuo, genera una dependencia tecnológica viciosa y una evolución económica incompatible con las necesidades sociales, convirtiendo esta dependencia en una causa, síntoma y consecuencia de la falta de autonomía.

La soberanía tecnológica se hace, sobre todo, en comunidades. Todas las tecnologías se desarrollan en comunidades, que pueden ser, más o menos, autónomas o pueden estar, más o menos, controladas por las corporaciones. En la lucha por la soberanía, la cosa va de comunidades.
La premisa de una comunidad que aspira a ser soberana es que el conocimiento debe ser compartido y los desarrollos individuales deben ser devueltos al común. El conocimiento crece con la cooperación. La inteligencia es colectiva y privatizar el conocimiento es matar la comunidad. Así, la comunidad es garante de la libertad, es decir, de la soberanía.

Para continuar leyendo te recomendamos el dossier de Soberanía Tecnológica [acá](https://sobtec.gitbooks.io/sobtec2/es/).

## ¿Cómo funciona internet?

¿Quién usa Internet? ¿Cómo se usa? ¿Cómo nos afecta?

Internet es una red de redes de computadoras interconectadas. Si bien estamos aconstumbradxs a conectarnos desde nuestros dispositivos a través de redes Wi-Fi, la realidad es que el soporte de internet son cables de fibra óptica submarinos, que interconectan todos los continentes.

*Internet son los cables: [Mapa de Cables](https://www.submarinecablemap.com/)*

## Gobernanza de Internet

*Quiénes gobiernan Internet?*

Nadie y todo el mundo.

Al contrario que la red telefónica, que durante años en muchos países, la dirigía una sola compañía; Internet global consiste en millares de redes interconectadas dirigidas por proveedores de servicios, compañías individuales, universidades, gobiernos y otras personas.

[Artíulo de Internet Society sobre cómo funciona Internet](https://www.internetsociety.org/es/about-the-internet/how-it-works/)

## Privacidad

Según la Declaración Universal de los Derechos Humanos:

> Nadie será objeto de injerencias arbitrarias en su vida privada, su familia, su domicilio o su correspondencia, ni de ataques a su honra o a su reputación. Toda persona tiene derecho a la protección de la ley contra tales injerencias o ataques.

Sin embargo, las revelaciones de Edward Snowden (ex contratista de la CIA), expusieron el proyecto PRISM, de la National Security Agency de USA, orientado a monitoriar el tráfico de internet por dos vías:
 - Interviniendo el tráfico de la red y recolectando datos.
 - Acordando con las grandes empresas proveedoras de servicios (Google, Facebook, Amazon, Yahoo, etc) el acceso a los datos de sus usuarios.

Esto pone en relevancia el hecho de que agencias gubernamentales no solo interceptan y recopilan nuestras comunicaciones (laborales, financieras, personales) sino que además las almacenan y analizan.

Para más información, recomendamos este artículo de Rafael Bonifaz, especialista en seguridad informática: https://rafael.bonifaz.ec/blog/2013/06/la-vigilancia-en-internet/

## Datos

### Plataformas privadas centralizadas: nuestros datos, riesgos

La infraestructura de comunicación online que hoy tenemos al alcance más fácilmente son las plataformas privadas centralizadas, como Twitter, Facebook o Instagram.

A cambio, no solo entregamos nuestros datos personales a estas compañías, lo que ya compromete nuestra seguridad, sino que también quedamos casi completamente bajo sus reglas para gestionar nuestra expresión en línea.

Lo cierto es que estamos bajo las reglas, el control y la tutela de una empresa privada de la que somos apenas clientes y que no nos da una participación real en el diseño de sus principios, códigos de conducta, reglas y funcionalidades.

Un ejemplo típico son los criterios utilizados para eliminar imágenes consideradas "poco apropiadas" en Facebook, o cerrar canales en YouTube por supuestas infracciones de copyright.

> La lógica capitalista... produce una tecnología cada vez más privatizada, más rentable y controlada por las corporaciones. Pero ¿a quién le pagas por tu servicio de internet, por alojar tu sitio web, tener una casilla de correo electrónico y usar las redes sociales? Si no pagas con dinero, estás pagando con datos personales. No tenemos ningún control sobre el diseño del software propietario y tampoco sabemos cómo implementan las políticas de usuarios/as las empresas privadas. ¿Quién se queda con nuestros datos y genera ganancias a partir de nuestro trabajo?

Los algoritmos nunca son «neutros» e imparciales, y se centran en realizar la misión que les ha sido asignada, con frecuencia por occidentales de género masculino procedentes de las clases altas acunadas por el capitalismo.

Así, el código es un objeto político.

https://www.mozilla.org/es-ES/internet-health/privacy-security/

# El caso de Cambridge Analityca

El escándalo de Cambridge Analityca comienza con la recopilación de millones de datos de usuarios de Facebook a través de una aplicación que te proponía conocer "tu vida digital" por medio de un test, que
a través de varias pregunta delineaba como era tu perfil digital. Además de brindarte la respuestsa de cómo es tu personaidad, la aplicación recopilaba información no sólo sobre sus usuarios sino que también lo hacía sobe los contactos de estos.

Luego, esos datos fueron vendidos a Cambridge Analityca, una compañía privada que combinaba la minería de datos y el análisis de datos con la comunicación estratégica para el proceso electoral.

A través de técnicas de mejora de datos y segmentación de audiencia que proporcionan análisis psicográficos, CA recopiló datos sobre votantes utilizando fuentes como datos demográficos, comportamiento del consumidor, actividad en Internet y otras fuentes públicas y privadas, interfiriendo de esta manera en procesos electorales en USA (Trump), el Brexit de UK, además de  Nigeria, Kenia, la República Checa, India y Argentina.



## Conclusión de este apartado

Aumentar las prácticas seguras en la red también depende de desarrollar una  visión  holística  de  la  seguridad.  Hay  que  pensar  en  la  seguridad
como un proceso multidimensional que va desde defender tu cuerpo donde solo mandas tú, defender tu derecho a la expresión, a la cooperación, al
anonimato, a la privacidad, a la autoría... hasta defender tu derecho al aprendizaje de herramientas y aplicaciones que te protejan, lo que también
requiere saber qué alternativas existen y que implica usarlas, apoyarlas, defenderlas.

Las nuevas tecnologías de la información y la comunicación (TIC) están cambiando la forma en que nos comunicamos y en que nos relacionamos, la
forma en que creamos redes y en que se organiza la sociedad.

> la revolución tecnológica está produciendo cambios en la distribución del poder. El poder no desaparece, pero sus posiciones sufren cambios, se descoloca y, en algunos puntos, se disloca. Estos cambios se condensan en la metáfora de la red.
-- Marga Padilla, *El kit de la lucha en Internet*

## Disparador

Es necesario crear recursos, mecanismos y estrategias de seguridad digital respondiendo a los contextos donde toman forma nuestras identidades electrónicas.

## Recursos

- Risk - Documental sobre Julian Assange y Wikileaks (en Inglés): https://www.youtube.com/watch?v=u68nLqjm9TM

- Citizen Four - Documental sobre las filtraciones de Edward Snowden: https://powvldeo.net/rn1fykqi74wp

- El Gran Hackeo (The great hack) - Documental sobre la incidencia de Facebook y Cambridge Analityca en las elecciones - Netflix
---

## Economia de plataforma

Mientras el mercado es regido por las corporaciones que continúan marcando agenda en la economía regional, los principales indicadores sobre las condiciones de vida continúan deteriorándose.

Durante las últimas décadas se experimentaron cambios significativos en las formas de generación y extracción de valor, como así en las formas de organización del trabajo.

El conocimiento y la información comenzaron a ser elementos cada vez más importantes en la producción, y una gama de bienes emergentes han dado lugar a nuevas prácticas económicas.

Mientras su desarrollo tiene lugar, también avanza la tendencia hacia su privatización.

Códigos, información, símbolos, imágenes, ideas, conocimientos, subjetividades y relaciones sociales. Los elementos cognitivos toman lugar en la escena productiva, y la privatización de este mundo inmaterial trae como consecuencia la ampliación de los espacios mercatilizados.

En este contexto, circula un discurso hegemónico en relación al acceso al mundo del trabajo, que tiene que ver con la imposición de una cultura laboral basada en el emprendedurismo individual, cuya lógica desarticula toda capacidad de organización colectiva.

Las nuevas configuraciones en el mercado de trabajo se expresan en la instalación de una economía basada en lógicas de producción capitalistas mediadas por plataformas tecnológicas.

Uber, Rappi, Glovo, como principales representantes de un fenómeno que altera las relaciones laborales tradicionales, se autodefinen como simples intermediarios entre un usuario que desea contratar un servicio y un trabajador independiente que lo provee.

Sin embargo, esto que disfrazan de Economía Colaborativa, esconde una lógica de mercado totalmente desregulada, sin políticas públicas que la contenga y exijan la garantía de los derechos laborales.

Flexibilidad en los horarios, rápido acceso al empleo y autonomía, son algunas de las falacias que utilizan para legitimar prácticas de explotación laboral.

Detrás de cada uno de los argumentos con los que han logrado posicionarse como protagonistas en esta coyuntura, podemos encontrar estrategias económicas que buscan maximizar las ganancias de dichas empresas a costa de la calidad de la vida de quienes trabajan bajo estas modalidades.

Quienes se inscriben en ellas, deben poner a disposición no sólo su fuerza de trabajo, sino también los medios de producción: en principio se trata del transporte utilizado para desarrollar la actividad(como la bicleta o el auto) y el teléfono celular para manejar la aplicación, basada en los algoritmos del capital tendientes a trackear los tiempos, los recorridos, y cada uno de los datos que pueden obtenerse en cada jornada laboral.

Esta situación avanza en un sistema donde, como expresa Rita Segato, "los actos y prácticas que enseñan, habitúan y programan a los sujetos a transmutar lo vivo y su vitalidad en cosas" están presentes en cada esfera social.

No existe amparo ante los accidentes, el desempleo está al alcance de un click, y el peso de la vejez y la enfermedad cae sobre los hombros de los trabajadores.

En este marco socioeconómico, el sector de la juventud es uno de los mas afectados, en tanto es el primero que se ve interpelado por estas transformaciones en el mercado de trabajo.

Frente a esto, entendemos que si nuestro impulso de movimiento está ligado al deseo de co-crear una sociedad más sostenible, colectiva y comunitaria, entonces son los medios, los recursos y las relaciones sociales los que deben ser hackeados, y con esto, nos referimos a una intervención que subvierta el enfoque sobre el que están basados, y que sea la sostenibilidad de la vida la que pase a estar en el centro de cualquier desarrollo tecnolóǵico.

La tecnología y el trabajo informático apela a nuestra crítica consciente sobre lo que estamos construyendo y para quién lo hacemos. “Al construir un mundo preocupado sólo por obtener los resultados más eficientes - y no por procesos que hacen posible esos resultados- habrá menos posibilidades que seamos conscientes de la profundidad de la pasión, la dignidad y el respeto humanos” (Morozov, 378).

## Recursos

- Plataformas: una serie  documental (revista Anfibia) - https://www.youtube.com/playlist?list=PLnk2ynglFvD_jTmT8zc5LYOMUuR6e7_S4

- Capitalismo cognitivo y plataformas, una mirada
desde la economía social - https://www.idelcoop.org.ar/sites/www.idelcoop.org.ar/files/revista/articulos/pdf/pg_11-19.pdf
- Cooperativas de Platafomra, Trebor Sholz - http://dimmons.net/wp-content/uploads/2016/05/maq_Trebor-Scholz_COOP_PreF.pdf

- Platform Design Toolkit - De los modelos de Negocios al Diseño de Plataformas  - https://platformdesigntoolkit.com/wp-content/docs/Platform-Design-Toolkit-Whitepaper-ESP.pdf

---


#### Sobre la creación de contenidos libres

Crear recursos protegidos por licencias abiertas para que nuestro conocimiento quede a disposición de otras personas que pueden combinarlo, traducirlo y adaptarlo a diferentes contextos.


Como todas las otras, la soberanía tecnológica se hace, sobre todo, en comunidades.


---

### Herramientas libres

#### Individuales
- [Testear nuestro navegador](https://panopticlick.eff.org//)
- [Plugin para navegadores para bloquear publicidad](https://www.eff.org/privacybadger).


#### Colectivas

[Lime Survey](https://www.limesurvey.org/en): desarrollar cuestionarios online fácil de usar. Permite también hacer tratamientos estadísticos de los resultados de manera semi-automática.

---

### Acciones

- Emprender proceso para usar software libre siempre, donde sea posible.
- Invertir en una infraestructura libre, abierta y feminista que priorice la creatividad, la comunidad, la colaboración y el conocimiento compartido. Poder hostear un codiMD, un Limesurvey, etc.


---
Video:

- Presentación: nosotros, Cambá y FACTTIC
- Invitación a la video conferencia
- Vamos a hablar de tecnología, trabajo y juventud.
- Nuestra postura es desde trabajadorxs de la industria tecnológica.
- Entendemos el desarrollo de la tecnología como una tarea interdisciplinaria, donde no solo lxs programadorxs deben tomar las decisiones
- Convocar a todxs a opinar e


---

- adjuntar dossies soberania tecnologia  (padilla)
- adjuntar el de trebol


---

# Las tres pregunta

- una puede ir por el lado de como estas herramientas y redes sociales nos construyen un "sentido común"

- otra quizas: *Quiénes tienen poder de desición sobre el desarrollo, uso, acceso y distribución de las tecnologías?*

- ¿Cómo, y sobre todo para quién, funciona Internet ?

-

- En qué medida podemos tener autonomía como colectivo o movimiento mientras usamos tecnologias provativas?
![](https://drive.google.com/file/d/1DBPfkVLHTXtvqFFDIoI4_-5XwjfoGTId/view?usp=sharing)









---


### Economia de plataforma

1. Plataformas irrumpen acompañando una nueva narrativa -> servicios personalizados, rápidos, humanxs, relevantes al usuarix.

2. Se trata de negocios que crean "redes de pares".

3. Distintas capas componen las plataformas:

a- comercio| transacciones | relaciones
b- conectividad
c- hardware | diseño | conocimiento | bienes raíces

4. Qué plataformas conocemos? Caracterizar breve cada una (mensajeria, comida, alojamiento, citas, etc).

6. Cómo se genera y quién se queda con el valor que es producido a través de las plataformas?

### Datos y privacidad

1. Internet son los cables

2. Gobernanza. Internet ->  redes interconectadas dirigidas por proveedores de servicios, compañías individuales, universidades, gobiernos y otras personas.

3. Privacidad. Derecho humano.Vigilancia masiva: Snowden y wikileaks.
4. huella digital -> uso de aplicaciones de comunicacion masivas -> datos.

5. nuevo valor -> datos como mercancia -> publicidad -> ideologias -> consumo ->

6. moldeadores de sentidos -> subjetividad expuesta -> publicidades -> fake news -> cambrige analytica

7. ¿uso crítico? -> terminos y condiciones

8. soberania tecnologica - quien desarrolla y para que las tecnologias que usamos en nuestros organizaciones?




a) De las aplicaciones que usas diariamente, de cuántas leíste los términos y condiciones?

b)¿Cómo vive la juventud el impacto de las tecnologías en el mundo del trabajo? Qué ventajas y desventajas encontrás?

c) ¿Qué tanto crees que respetan los valores del cooperativismo las tecnologías que usamos?
